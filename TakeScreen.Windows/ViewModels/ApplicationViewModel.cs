﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TakeScreen.Windows.Controllers;
using TakeScreen.Windows.Views;

namespace TakeScreen.Windows.ViewModels
{
    public class ApplicationViewModel : Core.ViewModels.ApplicationViewModel<ApiController,ApplicationController>
    {
        public override ApiController ApiController
        {
            get
            {
                return ApiController.Current;
            }
        }

        private OptionsView _optionsView;

        public override ApplicationController ApplicationController
        {
            get
            {
                return ApplicationController.Current;
            }
        }
        public override void DisplayOptions()
        {

            //TODO: use weak reference
           if(_optionsView == null)
            {
                _optionsView = new OptionsView();
                _optionsView.Closed += OptionsWindow_Closed;
            }
           if(_optionsView.Visible)
            {
                _optionsView.Activate();
                return;
            }
            AutoUpdateController.Current.Locker.Reset();
            _optionsView.ShowDialog();
        }

        private void OptionsWindow_Closed(object sender, EventArgs e)
        {
            _optionsView.Closed -= OptionsWindow_Closed;
            _optionsView = null;
            AutoUpdateController.Current.Locker.Set();
        }

        public override void StartCapture()
        {
            CaptureController.Current.Start();
        }

    }
}
