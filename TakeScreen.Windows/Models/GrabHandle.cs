﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TakeScreen.Windows.Models
{
    public enum GrabHandle
    {
        None,
        TopLeft,
        TopMid,
        TopRight,
        MidLeft,
        MidRight,
        BottomLeft,
        BottomMid,
        BottomRight,
        Middle
    }
}
