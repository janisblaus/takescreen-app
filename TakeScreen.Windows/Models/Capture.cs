﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TakeScreen.Core.Models;
using TakeScreen.Windows.Controllers;
using TakeScreen.Windows.Drawings;
using TakeScreen.Windows.Extensions;
using TakeScreen.Windows.Views;

namespace TakeScreen.Windows.Models
{
  public  class Capture:Core.Models.Capture
    {
        private Bitmap _screenshot;
        private Color _penColor;
        private Color _markerColor;
        private ObservableCollection< Drawing> _drawings;
        //private Rect _labelBounds;
        //private string _labelText;
        public Capture() : base()
        {
            //Selection = new Selection();
            PropertyChanged += Capture_PropertyChanged;
            _penColor = Core.Properties.Settings.Default.PenColor;
            _markerColor = Core.Properties.Settings.Default.MarkerColor;
            _drawings = new ObservableCollection<Drawing>();
            _drawings.CollectionChanged += Drawings_CollectionChanged;
        }

        private void Drawings_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            CanUndo = Drawings.Count > 0;
        }

        private void Capture_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
               case "ScreenshotControlVisible":
                    RaisePropertyChanged("ScreenshotControlVisibility");
                    break;
                case "VideoControlVisible":
                    RaisePropertyChanged("VideoControlVisibility");
                    break;
                case "Mode":
                    RaisePropertyChanged("CaptureControlVisibility");
                    RaisePropertyChanged("CanvasVisibility");
                    break;
                case "ToolType":
                    RaisePropertyChanged("CaptureControlVisibility");
                    RaisePropertyChanged("CanvasVisibility");
                    RaisePropertyChanged("CurrentToolColor");
                    break;
                case "CanUndo":
                    //System.Windows.Input.CommandManager.InvalidateRequerySuggested();
                    break;
            }
        }

        //public new Selection Selection
        //{
        //    get { return base.Selection as Selection; }
        //    set
        //    {
        //        base.Selection = value;
        //    }
        //}

        public Bitmap Screenshot
        {
            get
            {
                return _screenshot;
            }
            set
            {
                if (_screenshot != value)
                {
                    _screenshot = value;
                    RaisePropertyChanged("Screenshot");
                }
            }
        }

        //public System.Windows.Visibility VideoControlVisibility
        //{
        //    get
        //    {
        //        if (VideoControlVisible)
        //        {
        //            return System.Windows.Visibility.Visible;
        //        }
        //        return System.Windows.Visibility.Collapsed;
        //    }
        //}

        //public System.Windows.Visibility ScreenshotControlVisibility
        //{
        //    get
        //    {
        //        if (ScreenshotControlVisible)
        //        {
        //            return System.Windows.Visibility.Visible;
        //        }
        //        return System.Windows.Visibility.Collapsed;
        //    }
        //}

        //public System.Windows.Visibility CaptureControlVisibility
        //{
        //    get
        //    {
        //        if (Mode == Core.Models.CaptureMode.Video)
        //        {
        //            return System.Windows.Visibility.Visible;
        //        }
        //        if (ToolType == Core.Models.ToolType.None)
        //        {
        //            return System.Windows.Visibility.Hidden;
        //        }
        //        return System.Windows.Visibility.Visible;
        //    }
        //}

        //public System.Windows.Visibility CanvasVisibility
        //{
        //    get
        //    {
        //        if (Mode == Core.Models.CaptureMode.Video)
        //        {
        //            return System.Windows.Visibility.Hidden;
        //        }
        //        if(ToolType == Core.Models.ToolType.None)
        //        {
        //            return System.Windows.Visibility.Hidden;
        //        }
        //        return System.Windows.Visibility.Visible;
        //    }
        //}

        public Color PenColor
        {
            get
            {
                return _penColor;
            }
            set
            {
                if(_penColor != value)
                {
                    _penColor = value;
                    RaisePropertyChanged("PenColor");
                    if (ToolType != Core.Models.ToolType.Marker)
                    {
                        RaisePropertyChanged("CurrentToolColor");
                    }

                    Task.Run(() =>
                    {

                        try
                        {
                            Core.Properties.Settings.Default.PenColor = _penColor;
                            Core.Properties.Settings.Default.Save();
                        }
                        catch (Exception ex)
                        {
                            ApplicationController.Current.CaptureException(ex);
                        }

                    });
                }
            }
        }

        public Color MarkerColor
        {
            get
            {
                return _markerColor;
            }
            set
            {
                if(_markerColor != value)
                {
                    _markerColor = value;
                    RaisePropertyChanged("MarkerColor");
                    if (ToolType == Core.Models.ToolType.Marker)
                    {
                        RaisePropertyChanged("CurrentToolColor");
                    }

                    Task.Run(() =>
                    {

                        try
                        {
                            Core.Properties.Settings.Default.MarkerColor = _markerColor;
                            Core.Properties.Settings.Default.Save();
                        }
                        catch (Exception ex)
                        {
                            ApplicationController.Current.CaptureException(ex);
                        }

                    });
                }
            }
        }

        public override ToolType ToolType
        {
            get
            {
                return base.ToolType;
            }
            set
            {
                var oldValue = base.ToolType;
                if (oldValue != value)
                {
                    base.ToolType = value;
                    RaisePropertyChanged("CurrentToolColor");
                }
            }
        }

        public ObservableCollection<Drawing> Drawings
        {
            get
            {
                return _drawings;
            }
        }

        public Bitmap GetImage()
        {
            if (Screenshot == null)
            {
                return null;
            }

            var imageRect = new Rectangle(System.Drawing.Point.Empty, Screenshot.Size);
            var cRect = Rectangle.Intersect(imageRect, Selection.Rectangle);


            using (Bitmap clone = Screenshot.Clone() as Bitmap)
            {
                using (Graphics g = Graphics.FromImage(clone))
                {
                    foreach (var drawing in Drawings)
                    {
                        drawing.OnPaint(g);
                    }
                }

                return clone.Clone(Selection.Rectangle, System.Drawing.Imaging.PixelFormat.DontCare);
            }

        }

        public Color CurrentToolColor
        {
            get
            {
                if (ToolType == Core.Models.ToolType.Marker)
                {
                    return MarkerColor;
                }

                return PenColor;
            }
            set
            {
                if (ToolType == Core.Models.ToolType.Marker)
                {
                    MarkerColor = value;
                }
                else
                {
                    PenColor = value;
                }
            }
        }

        //public Rect LabelBounds {
        //    get
        //    {
        //        return _labelBounds;
        //    }
        //     set
        //    {
        //        if(_labelBounds != value)
        //        {
        //            _labelBounds = value;
        //            RaisePropertyChanged("LabelBounds");
        //        }
        //    }
        //}
        //public string LabelText {
        //    get
        //    {
        //        return _labelText;
        //    }
        //     set
        //    {
        //        if(_labelText != value)
        //        {
        //            _labelText = value;
        //            RaisePropertyChanged("LabelText");
        //        }
        //    }
        //}

 
    }
}
