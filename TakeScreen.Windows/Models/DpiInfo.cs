﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TakeScreen.Windows.Models
{
    public struct DpiInfo
    {
        private readonly double _dpiScaleX;

        private readonly double _dpiScaleY;

        /// <summary>Gets the DPI scale on the X axis.</summary>
        /// <returns>The DPI scale for the X axis.</returns>
        public double DpiScaleX
        {
            get
            {
                return this._dpiScaleX;
            }
        }

        /// <summary>Gets the DPI scale on the Yaxis.</summary>
        /// <returns>The DPI scale for the Y axis.</returns>
        public double DpiScaleY
        {
            get
            {
                return this._dpiScaleY;
            }
        }

        public double PixelsPerDip
        {
            get
            {
                return this._dpiScaleY;
            }
        }

        /// <summary>Gets the DPI along X axis.</summary>
        /// <returns>The DPI along the X axis.</returns>
        public double PixelsPerInchX
        {
            get
            {
                return 96.0 * this._dpiScaleX;
            }
        }

        /// <summary>Gets the DPI along Y axis.</summary>
        /// <returns>The DPI along the Y axis.</returns>
        public double PixelsPerInchY
        {
            get
            {
                return 96.0 * this._dpiScaleY;
            }
        }

        public DpiInfo(double dpiScaleX, double dpiScaleY)
        {
            this._dpiScaleX = dpiScaleX;
            this._dpiScaleY = dpiScaleY;
        }
    }
}

