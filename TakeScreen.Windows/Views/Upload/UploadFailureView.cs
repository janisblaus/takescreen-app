﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TakeScreen.Windows.Views.Upload
{
    public partial class UploadFailureView : UserControl
    {
        public UploadFailureView()
        {
            InitializeComponent();
            retryButton.Text = Core.Properties.Resources.Retry;
            copyButton.Text = Core.Properties.Resources.Copy;
            saveButton.Text = Core.Properties.Resources.Save;
        }
    }
}
