﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TakeScreen.Windows.Views.Upload
{
    public partial class UploadSuccessView : UserControl
    {
        public UploadSuccessView()
        {
            InitializeComponent();
            this.openButton.Text = Core.Properties.Resources.Open;
            this.copyButton.Text = Core.Properties.Resources.Copy;
        }
    }
}
