﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using TakeScreen.Windows.Controllers;
using TakeScreen.Windows.Extensions;
using TakeScreen.Windows.Models;

namespace TakeScreen.Windows.Views.Upload
{
    public partial class UploadView : DefaultView
    {

        public UploadView()
        {
            InitializeComponent();
            Text = Core.Properties.Resources.Upload;

            successView.openButton.Click += new System.EventHandler(this.OpenButton_Click);
            successView.copyButton.Click += new System.EventHandler(this.CopyButton_Click);

            failureView.retryButton.Click += RetryButton_Click;
            failureView.copyButton.Click += new System.EventHandler(this.CopyButton_Click);
            failureView.saveButton.Click += SaveButton_Click;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            bool saveImage = BitmapSource != null;
            SaveFileDialog sfd = saveImage ? CaptureController.Current.SaveFileDialog : CaptureController.Current.VideoSaveFileDialog;
            sfd.InitialDirectory = TakeScreen.Core.Properties.Settings.Default.LastSaveDirectory;

            try
            {
                sfd.FileName = CaptureController.Current.GetNextFileName(sfd.InitialDirectory
                    , saveImage ? "Screenshot" : "Video"
                    , saveImage ? "png" : "mp4");
            }
            catch (Exception ex)
            {
                ApplicationController.Current.CaptureException(ex);
            }
            var result = sfd.ShowDialog();
            if (result == DialogResult.OK)
            {

                try
                {

                    var filePath = sfd.FileName;

                    if (saveImage)
                    {
                        var extension = System.IO.Path.GetExtension(filePath).ToLower();
                        System.Windows.Media.Imaging.BitmapEncoder encoder = null;
                        switch (extension)
                        {
                            case ".png":
                                encoder = new System.Windows.Media.Imaging.PngBitmapEncoder();
                                break;
                            case ".jpeg":
                                encoder = new System.Windows.Media.Imaging.JpegBitmapEncoder();
                                break;
                        }

                        if (encoder == null)
                        {
                            return;
                        }

                        using (var fs = System.IO.File.Create(filePath))
                        {
                            encoder.Frames.Add(System.Windows.Media.Imaging.BitmapFrame.Create(this.BitmapSource));
                            encoder.Save(fs);
                        }
                    }
                    else
                    {
                        System.IO.File.Copy(Core.Properties.Settings.Default.VideoFilePath, filePath, true);
                    }


                    TakeScreen.Core.Properties.Settings.Default.LastSaveDirectory = System.IO.Path.GetDirectoryName(filePath);
                    TakeScreen.Core.Properties.Settings.Default.Save();
                    this.Close();
                }

                catch (Exception ex)
                {
                    ApplicationController.Current.CaptureException(ex);
#if DEBUG

#else
                                Program.MainView.DisplayNotification(null, ex.Message, System.Windows.Forms.ToolTipIcon.Error);
#endif
                }
            }
        }

        private void RetryButton_Click(object sender, EventArgs e)
        {
            Upload();
        }


        public void Upload()
        {

            Console.WriteLine("{0}: Starting upload ...", DateTime.Now);


            failureView.errorLabel.Text = null;
            successView.urlTextBox.Text = null;

            failureView.Visible = false;
            successView.Visible = false;
            progressView.Visible = true;

            byte[] data = null;
            string filePath = Core.Properties.Settings.Default.VideoFilePath;
            if (BitmapSource != null)
            {
                string imageFormat = Core.Properties.Settings.Default.CurrentImageFormat;
                switch (imageFormat)
                {
                    case "png":
                        data = BitmapSource.PngBytes();
                        break;
                    case "jpeg":
                        data = BitmapSource.JpegBytes();
                        break;
                }
            }

            Task.Run(async () =>
            {
                try
                {
                    string url;
                    if (data == null)
                    {
                        url = await ApiController.Current.UploadVideoAsync(filePath, null);
                    }
                    else
                    {
                        url = await ApiController.Current.UploadImageAsync(data, null);
                    }
                    if (this.CanInvoke())
                    {
                        this.BeginInvoke(new Action(() =>
                        {

                            successView.urlTextBox.Text = url;
                            progressView.Visible = false;
                            failureView.Visible = false;
                            successView.Visible = true;
                        }));
                    }
                    Console.WriteLine("{0}: Upload done.", DateTime.Now);

                }
                catch (Exception ex)
                {
                    ApplicationController.Current.CaptureException(ex);
                    if (this.CanInvoke())
                    {
                        this.BeginInvoke(new Action(() =>
                        {
                            failureView.errorLabel.Text = ex.Message;
                            toolTip.SetToolTip(failureView.errorLabel, ex.Message);
                            progressView.Visible = false;
                            successView.Visible = false;
                            failureView.Visible = true;
                        }));
                    }
                }
            });

        }


        private void OpenButton_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start(this.successView.urlTextBox.Text);
            }
            catch (Exception ex)
            {
                ApplicationController.Current.CaptureException(ex);
            }
            finally
            {
                this.Close();
            }
        }

        private void CopyButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(successView.urlTextBox.Text))
                {
                    if (BitmapSource == null)
                    {
                        System.Collections.Specialized.StringCollection fileDropList = new System.Collections.Specialized.StringCollection();
                        fileDropList.Add(Core.Properties.Settings.Default.VideoFilePath);
                        Clipboard.SetFileDropList(fileDropList);
                        Program.MainView.DisplayNotification(null, TakeScreen.Core.Properties.Resources.CopyVideoSuccessMessage, System.Windows.Forms.ToolTipIcon.Info);
                    }
                    else
                    {
                        System.Windows.Clipboard.SetImage(BitmapSource);
                        Program.MainView.DisplayNotification(null, TakeScreen.Core.Properties.Resources.CopySuccessMessage, System.Windows.Forms.ToolTipIcon.Info);
                    }
                }
                else
                {
                    Clipboard.SetText(successView.urlTextBox.Text);
                }
                Close();
            }
            catch (Exception ex)
            {
                ApplicationController.Current.CaptureException(ex);
#if DEBUG

#else
                                Program.MainView.DisplayNotification(null, ex.Message, System.Windows.Forms.ToolTipIcon.Error);
#endif
            }
        }
    }
}
