﻿using System.Windows.Forms;

namespace TakeScreen.Windows.Views.Upload
{
    partial class UploadView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.progressView = new TakeScreen.Windows.Views.Upload.UploadProgressView();
            this.successView = new TakeScreen.Windows.Views.Upload.UploadSuccessView();
            this.failureView = new TakeScreen.Windows.Views.Upload.UploadFailureView();
            this.SuspendLayout();
            // 
            // progressView
            // 
            this.progressView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressView.Location = new System.Drawing.Point(0, 0);
            this.progressView.Name = "progressView";
            this.progressView.Size = new System.Drawing.Size(344, 72);
            this.progressView.TabIndex = 0;
            // 
            // successView
            // 
            this.successView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.successView.Location = new System.Drawing.Point(0, 0);
            this.successView.Name = "successView";
            this.successView.Size = new System.Drawing.Size(344, 72);
            this.successView.TabIndex = 1;
            this.successView.Visible = false;
            // 
            // failureView
            // 
            this.failureView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.failureView.Location = new System.Drawing.Point(0, 0);
            this.failureView.Name = "failureView";
            this.failureView.Size = new System.Drawing.Size(344, 72);
            this.failureView.TabIndex = 2;
            this.failureView.Visible = false;
            // 
            // UploadView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(344, 71);
            this.Controls.Add(this.progressView);
            this.Controls.Add(this.successView);
            this.Controls.Add(this.failureView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UploadView";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Upload";
            this.TopMost = true;
            this.ResumeLayout(false);

        }

        #endregion
    
        private System.Windows.Forms.ToolTip toolTip;
        private UploadProgressView progressView;
        private UploadSuccessView successView;
        private UploadFailureView failureView;
    }
}