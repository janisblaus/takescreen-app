﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TakeScreen.Windows.Views
{
    class ColorButtonView:ButtonView
    {
        private Color _color;

        public Color Color
        {
            get
            {
                return _color;
            }
            set
            {
                if(_color != value)
                {
                    _color = value;
                    var oldImage = this.Image;
                    Bitmap bmp = new Bitmap(4, 4);
                    using(Graphics g = Graphics.FromImage(bmp))
                    {
                        g.Clear(_color);
                    }
                    Image = bmp;
                    if (oldImage != null)
                    {
                        oldImage.Dispose();
                    }
                    _visualNeedsUpdate = true;
                }
            }
        }
    }
}
