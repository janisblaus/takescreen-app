﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TakeScreen.Core.ViewModels;
using TakeScreen.Windows.Controllers;
using TakeScreen.Windows.Extensions;
using TakeScreen.Windows.ViewModels;

namespace TakeScreen.Windows.Views
{
    public class ApplicationView : Core.Views.ApplicationView<ApplicationViewModel, ApiController,ApplicationController>
    {
        private NotifyIcon _notifyIcon;
        public ApplicationView():base()
        {
            _notifyIcon = new NotifyIcon();
           
            _notifyIcon.Icon = Model.NotifyIcon.Icon;
            _notifyIcon.MouseClick += _notifyIcon_MouseClick;
        
            _notifyIcon.ContextMenuStrip = new ContextMenuStrip();
            foreach (var item in Model.NotifyIcon.Menu.Items)
            {
                CreateToolStripMenuItem(item, Model.NotifyIcon.Menu.Items.IndexOf(item));
            }

           
            Model.NotifyIcon.Menu.Items.CollectionChanged += Items_CollectionChanged;
        }

        private  void CreateToolStripMenuItem(MenuItemViewModel item, int index)
        {
            var menuStrip = _notifyIcon.ContextMenuStrip;
            if (item is Core.ViewModels.TextMenuItemViewModel)
            {
                var tItem = item as Core.ViewModels.TextMenuItemViewModel;
                var mItem = new ToolStripMenuItem(tItem.Text);
                mItem.Enabled = item.Enabled;
                mItem.Name = item.Key;
                mItem.Click += new EventHandler((object sender, EventArgs e) =>
                {
                    tItem.Action?.Invoke();
                });
                tItem.EnabledChanged += new EventHandler((object sender, EventArgs e) =>
                {
                    if (menuStrip.CanInvoke())
                    {
                        if (menuStrip.InvokeRequired)
                        {
                            menuStrip.BeginInvoke(new Action(() =>
                            {
                                mItem.Enabled = tItem.Enabled;
                            }));
                        }
                        else
                        {
                            mItem.Enabled = tItem.Enabled;
                        }
                    }
                });
                tItem.TextChanged += new EventHandler((object sender, EventArgs e) =>
                {
                    if (menuStrip.CanInvoke())
                    {
                        if (menuStrip.InvokeRequired)
                        {
                            menuStrip.BeginInvoke(new Action(() =>
                            {
                                mItem.Text = tItem.Text;
                            }));
                        }
                        else
                        {
                            mItem.Text = tItem.Text;
                        }
                    }
                });
                menuStrip.Items.Insert(index,mItem);
            }
            else if (item is Core.ViewModels.SeparatorMenuItemViewModel)
            {
                menuStrip.Items.Insert(index,new ToolStripSeparator() { Name = item.Key });
            }
        }

        private void Items_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {

            var action = new Action(() =>
            {
                switch (e.Action)
                {
                    case NotifyCollectionChangedAction.Add:
                        foreach (var item in e.NewItems)
                        {
                            var mItem = item as MenuItemViewModel;
                            var i = Model.NotifyIcon.Menu.Items.IndexOf(mItem);
                            CreateToolStripMenuItem(mItem, i);
                        }
                        break;
                    case NotifyCollectionChangedAction.Remove:
                        foreach (var item in e.OldItems)
                        {
                            var mItem = item as MenuItemViewModel;
                            _notifyIcon.ContextMenuStrip.Items.RemoveByKey(mItem.Key);
                        }
                        break;
                }
            });

            //if (_notifyIcon.ContextMenuStrip.CanInvoke())
            //{
                if (_notifyIcon.ContextMenuStrip.InvokeRequired)
                {
                    _notifyIcon.ContextMenuStrip.BeginInvoke(action);
                }
                else
                {
                    action();
                }
            //}
        }

        private void _notifyIcon_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Model.NotifyIcon.Action?.Invoke();
            }
        }

        public override void Show()
        {
            _notifyIcon.Visible = true;
        }

        public void DisplayNotification(string title, string message, ToolTipIcon icon)
        {

            _notifyIcon.ShowBalloonTip(10000, title, message, icon);
        }

        public override void Hide()
        {
            _notifyIcon.Visible = false;
        }

        public NotifyIcon NotifyIcon
        {
            get
            {
                return _notifyIcon;
            }
        }
    }
}
