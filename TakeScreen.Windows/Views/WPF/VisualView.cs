﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using TakeScreen.Windows.Extensions;

namespace TakeScreen.Windows.Views
{
    /// <summary>
    /// Renders a <see cref="System.Windows.Media.Visual"/>.
    /// </summary>
    public partial class VisualView:UIView
    {
        internal Visual _visual;
        internal bool _visualNeedsUpdate;

        public VisualView() : base()
        {
            _visual = CreateVisual();
            
        }

        internal virtual Visual CreateVisual()
        {
            return null;
        }

        internal virtual void UpdateVisual()
        {

        }

        internal virtual void UpdateVisualImage(Graphics g)
        {
            if(_visual == null)
            {
                return;
            }

            RenderTargetBitmap image = new RenderTargetBitmap(
     (int)(RenderingBounds.Width * g.DpiX / 96.0),
     (int)(RenderingBounds.Height * g.DpiY / 96.0),
     g.DpiX,
     g.DpiY,
     PixelFormats.Pbgra32);
            image.Render(_visual);
   
            var oldImage = _visualBitmap;
            _visualBitmap = image.PngImage();
            if (oldImage != null)
            {
                oldImage.Dispose();
            }
        }

        public override void OnPaint(Graphics g)
        {
            base.OnPaint(g);
            if (!Visible)
            {
                return;
            }
            if (_visualNeedsUpdate)
            {
                UpdateVisual();
                UpdateVisualImage(g);
                _visualNeedsUpdate = false;
            }
            if(_visualBitmap != null)
            {
                g.DrawImage(_visualBitmap, RenderingBounds);
            }
        }
    }
}
