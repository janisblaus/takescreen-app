﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;

namespace TakeScreen.Windows.Views
{
   public class LabelView:VisualView
    {

        public LabelView(Label label):base()
        {
            Label = label;
        }

        public Label Label
        {
            get
            {
                return _visual as Label;
            }
            set
            {
                var oldValue = Label;
                if (_visual != value)
                {
                    _visual = value;
                    _visualNeedsUpdate = true;
                }
               
            }
        }

        internal override Visual CreateVisual()
        {
            return Label;
        }

        internal override void UpdateVisual()
        {
            if(Label == null)
            {
                return;
            }
            Label.Measure(new System.Windows.Size(double.PositiveInfinity,double.PositiveInfinity));
            Label.Arrange(new System.Windows.Rect(new System.Windows.Point(), Label.DesiredSize));
            Size = new Size((int)Label.DesiredSize.Width, (int)Label.DesiredSize.Height);
        }

        internal override void UpdateVisualImage(Graphics g)
        {
            base.UpdateVisualImage(g);
            Size = _visualBitmap.Size;
        }
    }
}
