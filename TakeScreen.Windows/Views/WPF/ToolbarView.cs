﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using TakeScreen.Windows.Extensions;

namespace TakeScreen.Windows.Views
{
    public partial class ToolbarView
    {

        private int _shadowDepth = 3;

        internal override Visual CreateVisual()
        {
            var border = new Border();
            border.CornerRadius = new System.Windows.CornerRadius(3.0);
            border.Background = new SolidColorBrush()
            {
                Color = Colors.Black,
                Opacity = 0.95
            };
            border.Effect = new DropShadowEffect()
            {
                Opacity = 0.56,
                BlurRadius = 6.0,
                ShadowDepth = _shadowDepth
            };

            return border;
        }

        public override Rectangle Bounds
        {
            get { return base.Bounds; }
            set
            {
                var oldValue = base.Bounds;
                if (oldValue != value)
                {
                    base.Bounds = value;
                    _visualNeedsUpdate = true;
                }
            }
        }

        internal override void UpdateVisual()
        {
            base.UpdateVisual();
            var border = _visual as Border;
            if (border == null)
            {
                return;
            }
            var rect = Bounds.Rect();
            //var dpi = border.Dpi();
        
            //rect.X /= dpi.DpiScaleX;
            //rect.Y /= dpi.DpiScaleY;
            //rect.Width /= dpi.DpiScaleX;
            //rect.Height /= dpi.DpiScaleY;
            border.Measure(rect.Size);
            border.Arrange(new System.Windows.Rect(new System.Windows.Point(), rect.Size));

        }

        public override Rectangle RenderingBounds
        {
            get
            {
                var rBounds = Bounds;
                rBounds.Width += _shadowDepth;
                rBounds.Height += _shadowDepth;
                return rBounds;
            }
        }
    }
}
