﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Controls;
using System.Windows.Media.Effects;
using TakeScreen.Windows.Extensions;

namespace TakeScreen.Windows.Views
{
    public partial class ButtonView
    {
        private DropShadowEffect _checkedEffect;
        private DropShadowEffect _highlightedEffect;

        public ButtonView() : base()
        {
            _checkedEffect = new DropShadowEffect()
            {
                Color = Colors.White,
                Opacity = 1,
                ShadowDepth = 0,
                Direction = 0,
                BlurRadius=16
            };
            _highlightedEffect = new DropShadowEffect()
            {
                Color = Colors.White,
                Opacity = 0.66,
                ShadowDepth = 0,
                Direction = 0,
                BlurRadius = 16
            };
        }
        internal override Visual CreateVisual()
        {
            Border border = new Border();
            Image image = new Image();
            border.Child = image;

            return border;
        }

        internal override void UpdateVisual()
        {
            base.UpdateVisual();
            var border = _visual as Border;
            if (border == null)
            {
                return;
            }
            border.IsEnabled = Enabled;
            if (Checked)
            {
                border.Effect = _checkedEffect;
            }
            else if (Highlighted)
            {
                border.Effect = _highlightedEffect;
            }
            else
            {
                border.Effect = null;
            }
            border.Padding = new System.Windows.Thickness(Padding.Left, Padding.Top, Padding.Right, Padding.Bottom);
            var rect = Bounds.Rect();

            border.Measure(rect.Size);
            border.Arrange(new System.Windows.Rect(new System.Windows.Point(), rect.Size));
        }

    }
}
