﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TakeScreen.Windows.Controllers;

namespace TakeScreen.Windows.Views
{
  public  class VideoCaptureView:BaseCaptureView
    {

        public VideoCaptureView() : base()
        {

        }

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams createParams = base.CreateParams;
                createParams.ExStyle |= NativeMethods.WS_EX_LAYERED;
                return createParams;
            }
        }

        public void SetLayeredWindow()
        {

            using (Bitmap bitmap = new Bitmap(Width, Height, PixelFormat.Format32bppArgb))
            {
                using (Graphics g = Graphics.FromImage(bitmap))
                {
                    Draw(g);
                }
                //bitmap.Save(System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "Screen.png"));
                // Get device contexts
                IntPtr screenDc = NativeMethods.GetDC(IntPtr.Zero);
                IntPtr memDc = NativeMethods.CreateCompatibleDC(screenDc);
                IntPtr hBitmap = IntPtr.Zero;
                IntPtr hOldBitmap = IntPtr.Zero;

                try
                {
                    // Get handle to the new bitmap and select it into the current device context
                    hBitmap = bitmap.GetHbitmap(Color.FromArgb(0));
                    hOldBitmap = NativeMethods.SelectObject(memDc, hBitmap);

                    // Set parameters for layered window update
                    NativeMethods.Size newSize = new NativeMethods.Size(bitmap.Width, bitmap.Height);   // Size window to match bitmap
                    NativeMethods.Point sourceLocation = new NativeMethods.Point(0, 0);
                    NativeMethods.Point newLocation = new NativeMethods.Point(this.Left, this.Top);     // Same as this window
                    NativeMethods.BLENDFUNCTION blend = new NativeMethods.BLENDFUNCTION();
                    blend.BlendOp = NativeMethods.AC_SRC_OVER;                        // Only works with a 32bpp bitmap
                    blend.BlendFlags = 0;                                           // Always 0
                    blend.SourceConstantAlpha = 255;                                        // Set to 255 for per-pixel alpha values
                    blend.AlphaFormat = NativeMethods.AC_SRC_ALPHA;                       // Only works when the bitmap contains an alpha channel

                    // Update the window
                    NativeMethods.UpdateLayeredWindow(Handle, screenDc, ref newLocation, ref newSize,
                        memDc, ref sourceLocation, 0, ref blend, NativeMethods.ULW_ALPHA);
                    //var errcode=      System.Runtime.InteropServices.Marshal.GetLastWin32Error();
                }
                finally
                {
                    // Release device context
                    NativeMethods.ReleaseDC(IntPtr.Zero, screenDc);
                    if (hBitmap != IntPtr.Zero)
                    {
                        NativeMethods.SelectObject(memDc, hOldBitmap);
                        NativeMethods.DeleteObject(hBitmap);                                      // Remove bitmap resources
                    }
                    NativeMethods.DeleteDC(memDc);
                }
            }
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            base.OnMouseLeave(e);
            SetLayeredWindow();
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            SetLayeredWindow();
        }

        protected override void Menubar_ItemClicked(ToolbarView sender, ToolbarViewItemClickedEventArgs e)
        {
            switch (e.Item.Name)
            {
                case "StopButton":
                    Stop();
                    break;
            }
        }

        private void Stop()
        {
            CaptureController.Current.StopRecording();
        }

    }
}
