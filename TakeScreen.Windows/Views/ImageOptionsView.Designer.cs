﻿namespace TakeScreen.Windows.Views
{
    partial class ImageOptionsView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.defaultFormatLabel = new System.Windows.Forms.Label();
            this.jpegQualityLabel = new System.Windows.Forms.Label();
            this.captureMouseLabel = new System.Windows.Forms.Label();
            this.defaultFormatComboBox = new System.Windows.Forms.ComboBox();
            this.jpegQualityNumeric = new System.Windows.Forms.NumericUpDown();
            this.captureMouseCheckBox = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.jpegQualityNumeric)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.AutoScroll = true;
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.defaultFormatLabel, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.jpegQualityLabel, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.captureMouseLabel, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.defaultFormatComboBox, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.jpegQualityNumeric, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.captureMouseCheckBox, 1, 2);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 4;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(381, 213);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // defaultFormatLabel
            // 
            this.defaultFormatLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.defaultFormatLabel.AutoSize = true;
            this.defaultFormatLabel.Location = new System.Drawing.Point(3, 7);
            this.defaultFormatLabel.Name = "defaultFormatLabel";
            this.defaultFormatLabel.Size = new System.Drawing.Size(73, 13);
            this.defaultFormatLabel.TabIndex = 1;
            this.defaultFormatLabel.Text = "Default format";
            // 
            // jpegQualityLabel
            // 
            this.jpegQualityLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.jpegQualityLabel.AutoSize = true;
            this.jpegQualityLabel.Location = new System.Drawing.Point(3, 33);
            this.jpegQualityLabel.Name = "jpegQualityLabel";
            this.jpegQualityLabel.Size = new System.Drawing.Size(69, 13);
            this.jpegQualityLabel.TabIndex = 2;
            this.jpegQualityLabel.Text = "JPEG Quality";
            // 
            // captureMouseLabel
            // 
            this.captureMouseLabel.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.captureMouseLabel.AutoSize = true;
            this.captureMouseLabel.Location = new System.Drawing.Point(3, 56);
            this.captureMouseLabel.Name = "captureMouseLabel";
            this.captureMouseLabel.Size = new System.Drawing.Size(110, 13);
            this.captureMouseLabel.TabIndex = 3;
            this.captureMouseLabel.Text = "Capture mouse cursor";
            // 
            // defaultFormatComboBox
            // 
            this.defaultFormatComboBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.defaultFormatComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.defaultFormatComboBox.FormattingEnabled = true;
            this.defaultFormatComboBox.Location = new System.Drawing.Point(119, 3);
            this.defaultFormatComboBox.Name = "defaultFormatComboBox";
            this.defaultFormatComboBox.Size = new System.Drawing.Size(60, 21);
            this.defaultFormatComboBox.TabIndex = 4;
            // 
            // jpegQualityNumeric
            // 
            this.jpegQualityNumeric.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.jpegQualityNumeric.Location = new System.Drawing.Point(119, 30);
            this.jpegQualityNumeric.Name = "jpegQualityNumeric";
            this.jpegQualityNumeric.Size = new System.Drawing.Size(50, 20);
            this.jpegQualityNumeric.TabIndex = 6;
            this.jpegQualityNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.jpegQualityNumeric.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // captureMouseCheckBox
            // 
            this.captureMouseCheckBox.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.captureMouseCheckBox.Location = new System.Drawing.Point(119, 56);
            this.captureMouseCheckBox.Name = "captureMouseCheckBox";
            this.captureMouseCheckBox.Size = new System.Drawing.Size(50, 14);
            this.captureMouseCheckBox.TabIndex = 7;
            this.captureMouseCheckBox.UseVisualStyleBackColor = true;
            // 
            // ImageOptionsView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel3);
            this.Name = "ImageOptionsView";
            this.Size = new System.Drawing.Size(381, 213);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.jpegQualityNumeric)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        public System.Windows.Forms.ComboBox defaultFormatComboBox;
        public System.Windows.Forms.NumericUpDown jpegQualityNumeric;
        public System.Windows.Forms.CheckBox captureMouseCheckBox;
        public System.Windows.Forms.Label defaultFormatLabel;
        public System.Windows.Forms.Label jpegQualityLabel;
        public System.Windows.Forms.Label captureMouseLabel;
    }
}
