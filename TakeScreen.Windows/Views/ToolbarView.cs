﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TakeScreen.Windows.Views
{
    public partial class ToolbarView : VisualView
    {
        private ObservableCollection<ButtonView> _items;
        private bool _autoSize;
        private Orientation _orientation;
        private ButtonView _highlightedItem;
        private Dictionary<MouseButtons,ButtonView> _capturedItems;

        public event EventHandler HighlightedItemChanging;
        public event EventHandler HighlightedItemChanged;
        public event ToolbarViewItemClickedEventHandler ItemClicked;

        public ToolbarView() : base()
        {
            _items = new ObservableCollection<ButtonView>();
            _items.CollectionChanged += Items_CollectionChanged;
            _autoSize = true;
            LocationChanged += ToolbarView_LocationChanged;
            _capturedItems = new Dictionary<MouseButtons, ButtonView>();
        }

        private void ToolbarView_LocationChanged(object sender, EventArgs e)
        {
            //TODO: update only the locations
            PerformAutoSize();
        }

        private void Items_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case System.Collections.Specialized.NotifyCollectionChangedAction.Add:
                case System.Collections.Specialized.NotifyCollectionChangedAction.Remove:
                case System.Collections.Specialized.NotifyCollectionChangedAction.Replace:
                case System.Collections.Specialized.NotifyCollectionChangedAction.Reset:
                    PerformAutoSize();
                    break;
            }
        }

        private void PerformAutoSize()
        {
            if (!AutoSize)
            {
                return;
            }

            int width = 0;
            int height = 0;
            //x,y coordinates for items
            int x = this.Bounds.X + this.Padding.Left;
            int y = this.Bounds.Y + this.Padding.Top;

            switch (_orientation)
            {
                case Orientation.Horizontal:
                    width = Padding.Horizontal;
                    
                    foreach(var item in _items)
                    {
                        item.Left = x + item.Margin.Left;
                        width += item.Size.Width + item.Margin.Horizontal;
                        height = Math.Max(height, item.Size.Height + item.Margin.Vertical);
                        x += item.Bounds.Width + item.Margin.Horizontal;
                    }
                    height += Padding.Vertical;

                    Size = new Size(width, height);

                    foreach (var item in _items)
                    {
                        item.Top = Bounds.Top+ (height - item.Size.Height) / 2;
                    }

                    break;
                case Orientation.Vertical:
                    height = Padding.Vertical;

                    foreach (var item in _items)
                    {
                        item.Top = y + item.Margin.Top;
                        height += item.Size.Height + item.Margin.Vertical;
                        width = Math.Max(width, item.Size.Width + item.Margin.Horizontal);
                        y += item.Bounds.Height + item.Margin.Vertical;
                    }
                    width += Padding.Horizontal;

                    Size = new Size(width, height);

                    foreach (var item in _items)
                    {
                        item.Left = Bounds.Left+ (width - item.Size.Width) / 2;
                    }
                    break;
            }

        }

        public bool AutoSize
        {
            get
            {
                return _autoSize;
            }
            set
            {
                if(_autoSize != value)
                {
                    _autoSize = value;
                    PerformAutoSize();
                }
            }
        }

        public Orientation Orientation
        {
            get
            {
                return _orientation;
            }

            set
            {
                if(_orientation != value)
                {
                    _orientation = value;
                    PerformAutoSize();
                }
            }
        }

        public ObservableCollection<ButtonView> Items
        {
            get { return _items; }
        }

        public override void OnPaint(Graphics g)
        {
            base.OnPaint(g);
            if (!Visible)
            {
                return;
            }
            foreach(var item in _items)
            {
                item.OnPaint(g);
            }
        }

        public override void OnMouseMove(HandledMouseEventArgs e)
        {
            base.OnMouseMove(e);
            if (e.Handled)
            {
                return;
            }
            if (!Visible)
            {
                return;
            }
            if (!Enabled)
            {
                return;
            }
            if (e.Button == MouseButtons.None)
            {
                if (Bounds.Contains(e.Location))
                {
                    var items = (from i in _items
                                where i.Visible && i.Enabled
                                select i).Reverse();
                    foreach (var item in items)
                    {
                        if (item.Bounds.Contains(e.Location))
                        {
                            HighlightedItem = item;
                            return;
                        }
                    }
                    HighlightedItem = null;
                }
                else
                {
                    HighlightedItem = null;
                }

            }
        }

        public override void OnMouseDown(HandledMouseEventArgs e)
        {
            base.OnMouseDown(e);
            if (e.Handled)
            {
                return;
            }
            if (!Visible)
            {
                return;
            }
            if (!Enabled)
            {
                return;
            }

            if (Bounds.Contains(e.Location))
            {
                e.Handled = true;
                var items = (from i in _items
                             where i.Visible && i.Enabled
                             select i).Reverse();
                foreach (var item in items)
                {
                    if (item.Bounds.Contains(e.Location))
                    {
                        _capturedItems[e.Button] = item;
                       
                        return;
                    }
                }
                _capturedItems[e.Button] = null;
            }
            else
            {
                _capturedItems[e.Button] = null;
            }
        }

        public override void OnMouseUp(HandledMouseEventArgs e)
        {
            base.OnMouseUp(e);
            if (Bounds.Contains(e.Location))
            {
                var items = (from i in _items
                             select i).Reverse();
                foreach (var item in items)
                {
                    if (item.Bounds.Contains(e.Location))
                    {
                        if (_capturedItems[e.Button] == item)
                        {
                            ItemClicked?.Invoke(this
                                , new ToolbarViewItemClickedEventArgs(item));
                            e.Handled = true;
                        }
                        break;
                    }
                }
            }

            _capturedItems[e.Button] = null;
        }

        public ButtonView HighlightedItem
        {
            get
            {
                return _highlightedItem;
            }
            internal set
            {
                if (_highlightedItem != value)
                {
                    var oldValue = _highlightedItem;
                    HighlightedItemChanging?.Invoke(this, EventArgs.Empty);
                    _highlightedItem = value;
                    if (oldValue != null)
                    {
                        oldValue.Highlighted = false;
                    }
                    if (_highlightedItem != null)
                    {
                        _highlightedItem.Highlighted = true;
                    }
                    HighlightedItemChanged?.Invoke(this, EventArgs.Empty);
                }
            }
        }
    }

    public delegate void ToolbarViewItemClickedEventHandler(ToolbarView sender, ToolbarViewItemClickedEventArgs e);

    public class ToolbarViewItemClickedEventArgs : EventArgs
    {
        private ButtonView _item;

        public ToolbarViewItemClickedEventArgs(ButtonView item) : base()
        {
            _item = item;
        }

        public ButtonView Item
        {
            get
            {
                return _item;
            }
        }
    }
}
