﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using TakeScreen.Core.Models;
using TakeScreen.Windows.Controllers;

namespace TakeScreen.Windows.Views
{
    public partial class UpgradeProgressView : TakeScreen.Windows.Views.DefaultView
    {
        public UpgradeProgressView()
        {
            InitializeComponent();
            Text = string.Format("TakeScreen - {0}", Core.Properties.Resources.Update);
            logoPictureBox.Image = Core.Properties.Resources.Logo;
            titleLabel.Text = string.Format( Core.Properties.Resources.UpgradeProgressViewTitleFormat,"TakeScreen");
            messageLabel.Text = string.Format(Core.Properties.Resources.UpdateProgressMessageFormat, "TakeScreen");

        } 

    }
}
