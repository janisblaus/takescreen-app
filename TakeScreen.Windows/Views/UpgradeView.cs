﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using TakeScreen.Core.Models;
using TakeScreen.Windows.Controllers;

namespace TakeScreen.Windows.Views
{
    public partial class UpgradeView : TakeScreen.Windows.Views.DefaultView
    {
        public UpgradeView()
        {
            InitializeComponent();
            Text = string.Format("TakeScreen - {0}", Core.Properties.Resources.Update);
            logoPictureBox.Image = Core.Properties.Resources.Logo;
            titleLabel.Text = Core.Properties.Resources.UpgradeViewTitle;
            messageLabel.Text = string.Format(Core.Properties.Resources.UpdateMessageFormat, "TakeScreen");
            upgradeButton.Text = Core.Properties.Resources.Upgrade;
            notNowButton.Text = Core.Properties.Resources.NotNow;

            try
            {
                bool fIsElevated = Program.IsProcessElevated();
                this.upgradeButton.FlatStyle = FlatStyle.System;
                NativeMethods.SendMessage(upgradeButton.Handle,
                    NativeMethods.BCM_SETSHIELD, 0,
                    fIsElevated ? IntPtr.Zero : (IntPtr)1);
            }
            catch (Exception ex)
            {
                ApplicationController.Current.CaptureException(ex);
            }
        } 

        public AutoUpdateUpgradeResponse Response { get; set; }

        private void upgradeButton_Click(object sender, EventArgs e)
        {
            Response = AutoUpdateUpgradeResponse.Upgrade;
            Close();
        }

        private void notNowButton_Click(object sender, EventArgs e)
        {
            Response = AutoUpdateUpgradeResponse.NotNow;
            Close();
        }
    }
}
