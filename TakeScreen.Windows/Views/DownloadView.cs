﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using TakeScreen.Core.Models;

namespace TakeScreen.Windows.Views
{
    public partial class DownloadView : TakeScreen.Windows.Views.DefaultView
    {
        public DownloadView()
        {
            InitializeComponent();
            Text = string.Format("TakeScreen - {0}", Core.Properties.Resources.Update);
            logoPictureBox.Image = Core.Properties.Resources.Logo;
            titleLabel.Text = string.Format(Core.Properties.Resources.DownloadViewTitleFormat, "TakeScreen");
            messageLabel.Text =string.Format( Core.Properties.Resources.DownloadMessageFormat,"TakeScreen");
            downloadButton.Text = Core.Properties.Resources.Download;
            decideLaterButton.Text = Core.Properties.Resources.DecideLater;
            showLogButton.Text = Core.Properties.Resources.ShowLog;
        }

        public AutoUpdateDownloadResponse Response { get; set; }

        private void showLogButton_Click(object sender, EventArgs e)
        {
            Response = AutoUpdateDownloadResponse.ShowLog;
            Close();
        }

        private void downloadButton_Click(object sender, EventArgs e)
        {
            Response = AutoUpdateDownloadResponse.Download;
            Close();
        }

        private void decideLaterButton_Click(object sender, EventArgs e)
        {
            Response = AutoUpdateDownloadResponse.DecideLater;
            Close();
        }
    }
}
