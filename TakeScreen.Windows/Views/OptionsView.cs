﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TakeScreen.Windows.Controllers;

namespace TakeScreen.Windows.Views
{
    public partial class OptionsView : DefaultView
    {
        public OptionsView()
        {
            InitializeComponent();

            Text = Core.Properties.Resources.Options;
           

            tabPageGeneral.Text = Core.Properties.Resources.General;
            languageLabel.Text = Core.Properties.Resources.Language;
            languageComboBox.DataSource = Core.Properties.Settings.Default.AvailableCultures;
            languageComboBox.ValueMember = "Name";
            languageComboBox.DisplayMember = "DisplayName";
            languageComboBox.SelectedValue = Core.Properties.Settings.Default.CurrentCultureName;

            tabPageImage.Text = Core.Properties.Resources.Image;
            imageOptionsView.defaultFormatLabel.Text = Core.Properties.Resources.DefaultImageFormat;
            imageOptionsView.defaultFormatComboBox.DataSource = Core.Properties.Settings.Default.AvailableImageFormats;
            imageOptionsView.defaultFormatComboBox.SelectedItem = Core.Properties.Settings.Default.CurrentImageFormat;
            imageOptionsView.jpegQualityLabel.Text = Core.Properties.Resources.JpegQuality;
            imageOptionsView.jpegQualityNumeric.Value = Core.Properties.Settings.Default.JpegQuality;
            imageOptionsView.captureMouseLabel.Text = Core.Properties.Resources.ImageCaptureCursor;
            imageOptionsView.captureMouseCheckBox.Checked = Core.Properties.Settings.Default.ImageCaptureCursor;

            //Remove it for now
            tabControl.TabPages.Remove(tabPageGeneral);

            okButton.Text = Core.Properties.Resources.OK;
            cancelButton.Text = Core.Properties.Resources.Cancel;
        }


        private void okButton_Click(object sender, EventArgs e)
        {

            var settings = Core.Properties.Settings.Default;
            settings.CurrentCultureName = languageComboBox.SelectedValue as string;
            settings.CurrentImageFormat = imageOptionsView.defaultFormatComboBox.SelectedItem as string;
            settings.JpegQuality = (int)imageOptionsView.jpegQualityNumeric.Value;
            settings.ImageCaptureCursor = imageOptionsView.captureMouseCheckBox.Checked;
            ApplicationController.Current.UpdateCurrentCulture();
            settings.Save();
            Close();
        }



    }
}
