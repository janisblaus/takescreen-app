﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TakeScreen.Windows.Extensions;

namespace TakeScreen.Windows.Views
{
    public partial class ButtonView : VisualView
    {
        private Image _image;
        private bool _checked;
        private bool _highlighted;

        public Image Image
        {
            get
            {
                return _image;
            }
            set
            {
                if (_image != value)
                {
                    _image = value;
                    _visualNeedsUpdate = true;
                    var border = _visual as System.Windows.Controls.Border;
                    if(border == null)
                    {
                        return;
                    }
                    var vImage = border.Child as System.Windows.Controls.Image;
                    if(vImage == null)
                    {
                        return;
                    }
                    var bitmap = _image as Bitmap;
                    vImage.Source = bitmap?.BitmapSource();
                }
            }
        }

        public bool Checked
        {
            get { return _checked; }
            set
            {
                if (_checked != value)
                {
                    _checked = value;
                    _visualNeedsUpdate = true;
                }
            }
        }

        public bool Highlighted
        {
            get { return _highlighted; }


            set
            {
                if (_highlighted != value)
                {
                    _highlighted = value;
                    _visualNeedsUpdate = true;
                }
            }
        }

        public override Rectangle Bounds
        {
            get
            {
                return base.Bounds;
            }
            set
            {
                var oldValue = base.Bounds;
                if (oldValue != value)
                {
                    base.Bounds = value;
                    _visualNeedsUpdate = true;
                }
            }
        }

        public string Group { get; set; }

        public override void OnMouseMove(HandledMouseEventArgs e)
        {
            base.OnMouseMove(e);
            if(e.Button == MouseButtons.None)
            {

            }
        }
    }
}


