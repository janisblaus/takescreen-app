﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using TakeScreen.Core.Models;
using TakeScreen.Windows.Extensions;

namespace TakeScreen.Windows.Views
{
    public partial class DownloadProgressView : TakeScreen.Windows.Views.DefaultView
    {
        private WebFile _file;

        public DownloadProgressView()
        {
            InitializeComponent();
            pauseImageList.Images.Add("Pause", Core.Properties.Resources.Pause);
            pauseImageList.Images.Add("Resume", Core.Properties.Resources.Play);
            Text = string.Format("TakeScreen - {0}", Core.Properties.Resources.Update);
            logoPictureBox.Image = Core.Properties.Resources.Logo;
            titleLabel.Text = string.Format("{0} ...", Core.Properties.Resources.Downloading);
            instructionsLabel.Text = string.Format(Core.Properties.Resources.DownloadInstructionsFormat, "TakeScreen");
            pauseButton.ImageKey ="Pause";
            hideButton.Text = Core.Properties.Resources.Hide;
            cancelButton.Text = Core.Properties.Resources.Cancel;
        }

        public WebFile File { get
            {
                return _file;
            }
            set
            {
                if(_file != value)
                {
                    UnwireFileEvents();
                    _file = value;
                    UpdateTotal();
                    UpdateProgress();
                    WireFileEvents();
                }
            }
        }

        private void WireFileEvents()
        {
            if(_file != null)
            {
                _file.PropertyChanged += File_PropertyChanged;
            }
        }

        private void File_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "Downloaded":
                case "StartDate":
                    UpdateProgress();
                    break;
                case "Total":
                    UpdateTotal();
                    break;
                case "Paused":
                    HandlePaused();
                    break;
            }
        }

        private void HandlePaused()
        {
            Action action = new Action(() =>
            {
                if (File == null)
                {
                    titleLabel.Text = null;
                    instructionsLabel.Visible = false;
                }
                else
                {
                    bool paused = File.Paused;
                    titleLabel.Text = paused
                ? Core.Properties.Resources.Paused
            : string.Format("{0} ...", Core.Properties.Resources.Downloading);
                    pauseButton.ImageKey = paused ? "Resume" : "Pause";

                }
            });
            if (this.CanInvoke())
            {
                if (InvokeRequired)
                {

                    BeginInvoke(action);


                }
                else
                {
                    action();
                }
            }
        }

        private void UpdateTotal()
        {
            Action action = new Action(() =>
            {
                if (File == null)
                {
                    totalLabel.Text = null;
                }
                else
                {
                    progressBar.Maximum = File.Total;
                    totalLabel.Text = string.Format("{0} KB {1}", (File.Total / 1024).ToString("N0"), Core.Properties.Resources.Total);
                }
            });
            if (InvokeRequired)
            {
                if (this.CanInvoke())
                {
                    BeginInvoke(action);
                }
            
            else
            {
                action();
            }}
        }

        private void UpdateProgress()
        {
            Action action = new Action(() =>
            {
                if (File == null)
                {
                    progressBar.Value = 0;
                    progressLabel.Text = null;
                }
                else
                {
                    progressBar.Value = File.Downloaded;
                    if (File.Speed == null)
                    {
                        progressLabel.Text = null;
                    }
                    else
                    {
                        progressLabel.Text = string.Format(Core.Properties.Resources.DownloadProgressFormat
                                              , (File.DownloadedPercent).ToString("P0")
                                              , File.TimeRemaining.Value.ToString(@"hh\:mm\:ss")
                                              , string.Format("{0} KB/s", (File.Speed.Value / 1024).ToString("N0")));
                    }

                }
            });
            if (this.CanInvoke())
            {
                if (InvokeRequired)
                {

                    BeginInvoke(action);
                }
                else
                {
                    action();
                }
            }
        }

        private void UnwireFileEvents()
        {
           if(_file != null)
            {
                _file.PropertyChanged -= File_PropertyChanged;
            }
        }


        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            if (!e.Cancel)
            {
                UnwireFileEvents();
            }
        }
        public CancellationTokenSource CancellationTokenSource { get; internal set; }

        private void hideButton_Click(object sender, EventArgs e)
        {
            Hide();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Controllers.AutoUpdateController.Current.PauseDownloadEvent.Set();
            if (CancellationTokenSource == null)
            {
                return;
            }
            CancellationTokenSource.Cancel();
            Close();
        }

        private void pauseButton_Click(object sender, EventArgs e)
        {
            File.Paused = !File.Paused;
            if (File.Paused)
            {
                 Controllers.AutoUpdateController.Current.PauseDownloadEvent.Reset();
            }
            else
            {
                Controllers.AutoUpdateController.Current.PauseDownloadEvent.Set();
            }
            
        }
    }
}
