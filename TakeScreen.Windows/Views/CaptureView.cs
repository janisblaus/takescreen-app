﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TakeScreen.Windows.Models;
using TakeScreen.Windows.Controllers;
using TakeScreen.Windows.Extensions;
using TakeScreen.Windows.ViewModels;
using TakeScreen.Windows.Drawings;
using System.Windows.Interop;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using TakeScreen.Windows.Views.Upload;

namespace TakeScreen.Windows.Views
{
    public class CaptureView : BaseCaptureView
    {
        private const string TOOLS_GROUP = "Tools";

        //private Capture _model;
        //private Rectangle _selectionRectangle;
        private Point _mouseDownLocation;
        private Point _mouseDownPoint;
        private GrabHandle _currentGrabHandle;
        //private int _grabHandleLength;
        private SolidBrush _blendBrush;
        //private Pen _dashPen;
        private Pen _grabHandlePen;
        private Brush _grabHandleBrush;

        //private string _labelText;
        //private Font _labelFont;
        //private Brush _labelBackgroundBrush;
        //private StringFormat _labelStringFormat;
        //private Brush _labelForegroundBrush;
        //private RectangleF _labelRectangle;
        //private GraphicsPath _labelRectanglePath;

         private Drawing _activeDrawing;
        private Dictionary<int, Cursor> _cursors;
        private int _penSize;
        private int _markerSize;

        private ToolbarView _toolbar;
        //private ToolbarView _menubar;
        //private Size _buttonSize;
        //private Padding _buttonPadding;
        //private Padding _toolbarPadding;
        //private int _toolbarOffset ;
        //private UIView _previousMenubarHighlighted;
        private UIView _previousToolbarHighlighted;

        private PrintDocument _printDocument;
        //private ToolTip _tooltip;
        private LabelView _labelView;

       public CaptureView():base()
        {
            InitializeComponent();

           
            SetStyle(ControlStyles.AllPaintingInWmPaint
                | ControlStyles.UserPaint
                | ControlStyles.OptimizedDoubleBuffer
                , true);
            _cursors = new Dictionary<int, Cursor>();
            _penSize = 2;
            _markerSize = 20;
#if DEBUG
            TopMost = false;
#endif
            //_toolbarPadding = new Padding(0);
            //_toolbarOffset = 8;
            //DpiInfo dpi;
            //using(Graphics g= CreateGraphics())
            //{
            //    dpi = new DpiInfo((double)g.DpiX / 96.0, (double)g.DpiY / 96.0);
            //}

            //_buttonSize = new Size((int)(28.0*dpi.DpiScaleX),(int)( 28.0*dpi.DpiScaleY));
            //double allButtonPadding = 4.0;
            //_buttonPadding = new Padding((int)(allButtonPadding * dpi.DpiScaleX)
            //    , (int)(allButtonPadding * dpi.DpiScaleY)
            //    ,(int)(allButtonPadding * dpi.DpiScaleX)
            //     , (int)(allButtonPadding * dpi.DpiScaleY)
            //    );

            _toolbar = new ToolbarView();
            _toolbar.HighlightedItemChanging += Toolbar_HighlightedItemChanging;
            _toolbar.HighlightedItemChanged += Toolbar_HighlightedItemChanged;
            _toolbar.ItemClicked += Toolbar_ItemClicked;
            _toolbar.Visible = false;
            _toolbar.Padding = ToolbarPadding;
            _toolbar.Items.Add(new ButtonView()
            {
                Image = Core.Properties.Resources.SelectImage,
                Checked = true,
                Size= ButtonSize,
                Padding= ButtonPadding,
                Group=TOOLS_GROUP,
                Name="SelectButton",
                Tag= Core.Models.ToolType.None
            });
            _toolbar.Items.Add(new ButtonView()
            {
                Image = Core.Properties.Resources.PenImage,
                Size = ButtonSize,
                Padding = ButtonPadding,
                Group = TOOLS_GROUP,
                Name="PenButton",
                Tag = Core.Models.ToolType.Pen
            });
            _toolbar.Items.Add(new ButtonView()
            {
                Image = Core.Properties.Resources.RectangleImage,
                Size = ButtonSize,
                Padding = ButtonPadding,
                Group = TOOLS_GROUP,
                Name="RectangleButton",
                Tag = Core.Models.ToolType.Rectangle
            });
            _toolbar.Items.Add(new ButtonView()
            {
                Image = Core.Properties.Resources.EllipseImage,
                Size = ButtonSize,
                Padding = ButtonPadding,
                Group = TOOLS_GROUP,
                Name="EllipseButton",
                Tag = Core.Models.ToolType.Ellipse
            });
            _toolbar.Items.Add(new ButtonView()
            {
                Image = Core.Properties.Resources.ArrowImage,
                Size = ButtonSize,
                Padding = ButtonPadding,
                Group = TOOLS_GROUP,
                Name="ArrowButton",
                Tag = Core.Models.ToolType.Arrow
            });
            _toolbar.Items.Add(new ButtonView()
            {
                Image = Core.Properties.Resources.LineImage,
                Size = ButtonSize,
                Padding = ButtonPadding,
                Group = TOOLS_GROUP,
                Name="LineButton",
                Tag = Core.Models.ToolType.Line
            });
            _toolbar.Items.Add(new ButtonView()
            {
                Image = Core.Properties.Resources.TextImage,
                Size = ButtonSize,
                Padding = ButtonPadding,
                Group = TOOLS_GROUP,
                Name="TextButton",
                Tag = Core.Models.ToolType.Text
            });
            _toolbar.Items.Add(new ButtonView()
            {
                Image = Core.Properties.Resources.MarkerImage,
                Size = ButtonSize,
                Padding = ButtonPadding,
                Group = TOOLS_GROUP,
                Name="MarkerButton",
                Tag = Core.Models.ToolType.Marker
            });
            _toolbar.Items.Add(new ColorButtonView()
            {
                Size = ButtonSize,
                Padding = ButtonPadding,
                Name="ColorButton",
                Color=Model==null?Core.Properties.Settings.Default.PenColor:Model.CurrentToolColor
            });
            _toolbar.Items.Add(new ButtonView()
            {
                Image = Core.Properties.Resources.CloseImage,
                Size = ButtonSize,
                Padding = ButtonPadding,
                Name="CloseButton"
            });

            //_menubar = new ToolbarView();
            //_menubar.Visible = false;
            //_menubar.Padding = _toolbarPadding;
            //_menubar.HighlightedItemChanging += Menubar_HighlightedItemChanging;
            //_menubar.HighlightedItemChanged += Menubar_HighlightedItemChanged;
            //_menubar.ItemClicked += Menubar_ItemClicked;
            //_menubar.Orientation = Orientation.Vertical;
         
            _printDocument = new PrintDocument();
            _printDocument.DocumentName = "Screenshot";
            _printDocument.PrintPage += PrintDocument_PrintPage;
            _printDocument.EndPrint += PrintDocument_EndPrint;

            //_tooltip = new ToolTip();
            //_tooltip.ShowAlways = true;

            System.Windows.Controls.Label lbl = new System.Windows.Controls.Label();
            lbl.Content = TakeScreen.Core.Properties.Resources.SelectAreaCaption;
            lbl.Background = System.Windows.Media.Brushes.White;
            lbl.Padding = new System.Windows.Thickness(3);
            lbl.VerticalContentAlignment = System.Windows.VerticalAlignment.Center;
            lbl.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center;
            System.Windows.Media.Effects.DropShadowEffect effect = new System.Windows.Media.Effects.DropShadowEffect();
            effect.Color = System.Windows.Media.Colors.Black;
            effect.Opacity = 0.5;
            effect.Direction = 320;
            effect.ShadowDepth = 6;
            lbl.Margin = new System.Windows.Thickness(0, 0, effect.ShadowDepth, effect.ShadowDepth);
            lbl.Effect = effect;
            _labelView = new LabelView(lbl);
            _labelView.Visible = false;
            
        }



        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // CaptureView
            // 
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "CaptureView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Selection Layer";
            this.ResumeLayout(false);

        }

        public void CommitCurrentTool()
        {

            if(_activeDrawing != null)
            {
                _activeDrawing.Commit();
                var tDrawing = _activeDrawing as TextDrawing;
                if (tDrawing != null)
                {
                    if (!string.IsNullOrWhiteSpace(tDrawing.Text))
                    {
                        Model.Drawings.Add(tDrawing);

                    }
                    Invalidate(tDrawing.GetInvalidationBounds());
                }
                else
                {
                    if (_activeDrawing.Bounds.Width > 0 || _activeDrawing.Bounds.Height > 0)
                    {
                        Model.Drawings.Add(_activeDrawing);
                    }
                }
                _activeDrawing = null;
            }
        }

        protected override void ConfigureMenuBar()
        {
            MenuBar.Items.Add(new ButtonView()
            {
                Image = Core.Properties.Resources.UploadImage,
                Size = ButtonSize,
                Padding = ButtonPadding,
                Name = "UploadButton",
                Enabled = Model == null ? false : Model.CanUpload
            });
            MenuBar.Items.Add(new ButtonView()
            {
                Image = Core.Properties.Resources.RecordImage,
                Size = ButtonSize,
                Padding = ButtonPadding,
                Name = "RecordButton"

            });
            MenuBar.Items.Add(new ButtonView()
            {
                Image = Core.Properties.Resources.SaveImage,
                Size = ButtonSize,
                Padding = ButtonPadding,
                Name = "SaveButton",
                Enabled = Model == null ? false : Model.CanSave
            });
            MenuBar.Items.Add(new ButtonView()
            {
                Image = Core.Properties.Resources.CopyImage,
                Size = ButtonSize,
                Padding = ButtonPadding,
                Name = "CopyButton",
                Enabled = Model == null ? false : Model.CanCopy
            });
            MenuBar.Items.Add(new ButtonView()
            {
                Image = Core.Properties.Resources.PrintImage,
                Size = ButtonSize,
                Padding = ButtonPadding,
                Name = "PrintButton",
                Enabled = Model == null ? false : Model.CanPrint
            });
            MenuBar.Items.Add(new ButtonView()
            {
                Image = Core.Properties.Resources.UndoImage,
                Size = ButtonSize,
                Padding = ButtonPadding,
                Name = "UndoButton",
                Enabled = Model == null ? false : Model.CanUndo
            });
        }

        //protected override CreateParams CreateParams
        //{
        //    get
        //    {
        //        CreateParams createParams = base.CreateParams;
        //        if(Model == null)
        //        {
        //            return createParams;
        //        }
        //        switch (Model.Mode)
        //        {
        //            case Core.Models.CaptureMode.Image:
        //                return createParams;
        //        }
        //        createParams.ExStyle |= NativeMethods.WS_EX_LAYERED;
        //        return createParams;
        //    }
        //}

        protected override void Menubar_ItemClicked(ToolbarView sender, ToolbarViewItemClickedEventArgs e)
        {
            switch (e.Item.Name)
            {
                default:
                    base.Menubar_ItemClicked(sender, e);
                    break;
                case "UploadButton":
                    Upload();
                    break;
                case "RecordButton":
                    Record();
                    break;
                case "SaveButton":
                    Save();
                    break;
                case "CopyButton":
                    Copy();
                    break;
                case "PrintButton":
                    Print();
                    break;
                case "UndoButton":
                    Undo();
                    break;
            }
        }

        private void Copy()
        {
            try
            {
                CommitCurrentTool();
                var bmp = Model.GetImage();
                Clipboard.SetImage(bmp);
                CaptureController.Current.Close();
                Program.MainView.DisplayNotification(null, TakeScreen.Core.Properties.Resources.CopySuccessMessage, System.Windows.Forms.ToolTipIcon.Info);
                AutoUpdateController.Current.Locker.Set();
            }
            catch (Exception ex)
            {

                ApplicationController.Current.CaptureException(ex);
#if DEBUG

#else
                                Program.MainView.DisplayNotification(null, ex.Message, System.Windows.Forms.ToolTipIcon.Error);
#endif
            }
        }
        private void Upload()
        {
            CommitCurrentTool();
            var image = Model.GetImage().BitmapSource();

            UploadView w = new UploadView();
            w.FormClosed += UploadView_FormClosed;
            w.BitmapSource = image;

            w.Left = SystemInformation.WorkingArea.Right - w.Width;
            w.Top = SystemInformation.WorkingArea.Bottom - w.Height;
            w.Show();
            w.Upload();
            CaptureController.Current.Close();
        }

        private void UploadView_FormClosed(object sender, FormClosedEventArgs e)
        {
            UploadView w = sender as UploadView;
            if (w != null)
            {
                w.FormClosed -= UploadView_FormClosed;
            }
            AutoUpdateController.Current.Locker.Set();
        }

        private void Record()
        {

            CaptureController.Current.StartRecording();
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {

        }

        private void Undo()
        {
            if (Model == null)
            {
                return;
            }
            var last = Model.Drawings.LastOrDefault();
            if (last != null)
            {
                Model.Drawings.Remove(last);
            }
        }

        private void Save()
        {
            CommitCurrentTool();
            Model.ToolboxIsOpen = false;
            Model.MenuboxIsOpen = false;
            SaveFileDialog sfd = CaptureController.Current.SaveFileDialog;
            sfd.InitialDirectory = TakeScreen.Core.Properties.Settings.Default.LastSaveDirectory;

            try
            {
                sfd.FileName = CaptureController.Current.GetNextFileName(sfd.InitialDirectory, "Screenshot", TakeScreen.Core.Properties.Settings.Default.CurrentImageFormat);
            }
            catch (Exception ex)
            {
                ApplicationController.Current.CaptureException(ex);
            }
            var result = sfd.ShowDialog();
            if (result== DialogResult.OK)
            {

                try
                {

                    var filePath = sfd.FileName;
                    var extension = System.IO.Path.GetExtension(filePath).ToLower();
                    System.Windows.Media.Imaging.BitmapEncoder encoder = null;
                    switch (extension)
                    {
                        case ".png":
                            encoder = new System.Windows.Media.Imaging.PngBitmapEncoder();
                            break;
                        case ".jpeg":
                            var jpegEncoder = new System.Windows.Media.Imaging.JpegBitmapEncoder();
                            jpegEncoder.QualityLevel = TakeScreen.Core.Properties.Settings.Default.JpegQuality;
                            encoder = jpegEncoder;

                            break;
                    }
                    using (var fs = System.IO.File.Create(filePath))
                    {
                        encoder.Frames.Add(System.Windows.Media.Imaging.BitmapFrame.Create(Model.GetImage().BitmapSource()));
                        encoder.Save(fs);
                    }
                    TakeScreen.Core.Properties.Settings.Default.LastSaveDirectory = System.IO.Path.GetDirectoryName(filePath);
                    TakeScreen.Core.Properties.Settings.Default.Save();
                    CaptureController.Current.Close();
                    AutoUpdateController.Current.Locker.Set();
                }

                catch (Exception ex)
                {
                    ApplicationController.Current.CaptureException(ex);
#if DEBUG

#else
                    Program.MainView.DisplayNotification(null, ex.Message, System.Windows.Forms.ToolTipIcon.Error);
#endif
                    Model.UpdateToolboxProperties();
                    Model.UpdateMenuboxProperties();
                }

            }
            else
            {
                Model.UpdateToolboxProperties();
                Model.UpdateMenuboxProperties();
            }
        }

        private void Print()
        {
            CommitCurrentTool();
            Model.ToolboxIsOpen = false;
            Model.MenuboxIsOpen = false;

            using (PrintDialog pd = new PrintDialog())
            {
                pd.Document = _printDocument;
                var result = pd.ShowDialog(this);
                if (result == DialogResult.OK)
                {
                    try
                    {
                        _printDocument.Print();

                    }
                    catch (Exception ex)
                    {
                        ApplicationController.Current.CaptureException(ex);
#if DEBUG

#else
                        Program.MainView.DisplayNotification(null, ex.Message, System.Windows.Forms.ToolTipIcon.Error);
#endif
                        Model.UpdateToolboxProperties();
                        Model.UpdateMenuboxProperties();
                    }
                }
                else
                {
                    Model.UpdateToolboxProperties();
                    Model.UpdateMenuboxProperties();
                }
            }
        }

        private void PrintDocument_PrintPage(object sender, PrintPageEventArgs e)
        {
            var image = Model.GetImage();
            Size imageSize = image.Size;
            Rectangle bounds = e.MarginBounds;
            Size boundsSize = bounds.Size;
            double xRatio = (double)imageSize.Width / (double)boundsSize.Width;
            double yRatio = (double)imageSize.Height / (double)boundsSize.Height;
            Size outputSize = imageSize;
            if (xRatio > yRatio)//preserve width
            {
                if (imageSize.Width > boundsSize.Width)
                {
                    outputSize.Width = boundsSize.Width;
                    outputSize.Height = (int)(imageSize.Height / xRatio);
                }
            }
            else
            {
                if (imageSize.Height > boundsSize.Height)
                {
                    outputSize.Height = boundsSize.Height;
                    outputSize.Width = (int)(imageSize.Width / yRatio);
                }
            }
            e.Graphics.CompositingQuality = CompositingQuality.HighQuality;
            e.Graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
            e.Graphics.DrawImage(image, new Rectangle(bounds.Location, outputSize));
            e.HasMorePages = false;
        }

        private void PrintDocument_EndPrint(object sender, PrintEventArgs e)
        {
            CaptureController.Current.Close();
            AutoUpdateController.Current.Locker.Set();
        }

        private void Toolbar_ItemClicked(ToolbarView sender, ToolbarViewItemClickedEventArgs e)
        {
            if (e.Item.Group == TOOLS_GROUP)
            {
                var previouslySelected = (from i in sender.Items
                                          where i.Group == TOOLS_GROUP
                                          && i.Checked
                                          select i).FirstOrDefault();
                if (previouslySelected != e.Item)
                {
                    Rectangle bounds;
                    if(previouslySelected!= null)
                    {
                        previouslySelected.Checked = false;
                         bounds = previouslySelected.RenderingBounds;
                        bounds.Inflate(1, 1);
                        Invalidate(bounds);
                    }
                    e.Item.Checked = true;
                     bounds = e.Item.RenderingBounds;
                    bounds.Inflate(1, 1);
                    Invalidate(bounds);
                }
            }

            var toolType = e.Item.Tag as Core.Models.ToolType?;
            if (toolType.HasValue)
            {
                Model.ToolType = toolType.Value;
                return;
            }

            switch (e.Item.Name)
            {
                case "ColorButton":
                    using(ColorDialog dlg=new ColorDialog())
                    {
                        dlg.AnyColor = true;
                        dlg.SolidColorOnly = false;
                        dlg.Color = Model.CurrentToolColor;
                        if(dlg.ShowDialog(this)== DialogResult.OK)
                        {
                            Model.CurrentToolColor = dlg.Color;

                        }
                    }
                    break;
                case "CloseButton":
                    CommitCurrentTool();
                    CaptureController.Current.Close();
                    AutoUpdateController.Current.Locker.Set();
                    break;
            }
        }

        //private void Menubar_HighlightedItemChanged(object sender, EventArgs e)
        //{
        //    Rectangle bounds;
        //    if(_previousMenubarHighlighted!= null)
        //    {
        //        bounds = _previousMenubarHighlighted.RenderingBounds;
        //        bounds.Inflate(1, 1);
        //        Invalidate(bounds);
        //    }
        //    if (_menubar.HighlightedItem == null)
        //    {
        //        _tooltip.Hide(this);
        //    }
        //    else
        //    {
        //        bounds = _menubar.HighlightedItem.RenderingBounds;
        //        bounds.Inflate(1, 1);
        //        Invalidate(bounds);

        //        var toolTipText = GetToolTipText(_menubar.HighlightedItem);
        //        if (string.IsNullOrWhiteSpace(toolTipText))
        //        {
        //            _tooltip.Hide(this);
        //        }
        //        else
        //        {
        //            var point = _menubar.HighlightedItem.Bounds.BottomRight();
        //            _tooltip.Show(toolTipText, this, point);
        //        }

        //    }

        //}

        //private void Menubar_HighlightedItemChanging(object sender, EventArgs e)
        //{
        //    _previousMenubarHighlighted = _menubar.HighlightedItem;
        //}

        private void Toolbar_HighlightedItemChanged(object sender, EventArgs e)
        {
            Rectangle bounds;
            if (_previousToolbarHighlighted != null)
            {
                bounds = _previousToolbarHighlighted.RenderingBounds;
                bounds.Inflate(1, 1);
                Invalidate(bounds);
            }

            if (_toolbar.HighlightedItem == null)
            {
                ToolTip.Hide(this);
            }
            else
            {
                bounds = _toolbar.HighlightedItem.RenderingBounds;
                bounds.Inflate(1, 1);
                Invalidate(bounds);

                var toolTipText = GetToolTipText(_toolbar.HighlightedItem);
                if (string.IsNullOrWhiteSpace(toolTipText))
                {
                    ToolTip.Hide(this);
                }
                else
                {
                    var point = _toolbar.HighlightedItem.Bounds.BottomRight();
                    ToolTip.Show(toolTipText, this, point);
                }
               

            }
        }

        //private string GetToolTipText(ButtonView item)
        //{
        //    switch (item.Name)
        //    {
        //        case "PenButton":
        //            return Core.Properties.Resources.PenCaption;
        //        case "RectangleButton":
        //            return Core.Properties.Resources.RectCaption;
        //        case "EllipseButton":
        //            return Core.Properties.Resources.EllipseCaption;
        //        case "ArrowButton":
        //            return Core.Properties.Resources.ArrowCaption;
        //        case "LineButton":
        //            return Core.Properties.Resources.LineCaption;
        //        case "TextButton":
        //            return Core.Properties.Resources.TextCaption;
        //        case "MarkerButton":
        //            return Core.Properties.Resources.MarkerCaption;
        //        case "CloseButton":
        //            return Core.Properties.Resources.Close;
        //        case "UploadButton":
        //            return Core.Properties.Resources.UploadToTakesc;
        //        case "RecordButton":
        //            return Core.Properties.Resources.Record;
        //        case "StopButton":
        //            return Core.Properties.Resources.Stop;
        //        case "SaveButton":
        //            return Core.Properties.Resources.Save;
        //        case "CopyButton":
        //            return Core.Properties.Resources.Copy;
        //        case "PrintButton":
        //            return Core.Properties.Resources.Print;
        //        case "UndoButton":
        //            return Core.Properties.Resources.Undo;
        //    }

        //    return null;
        //}

        private void Toolbar_HighlightedItemChanging(object sender, EventArgs e)
        {
            _previousToolbarHighlighted = _toolbar.HighlightedItem;
        }

        //public Capture Model
        //{
        //    get
        //    {
        //        return _model;
        //    }
        //    set
        //    {
        //        if (_model != value)
        //        {
        //            UnwireModelEvents();

        //            _model = value;
        //            WireModelEvents();
        //            if(_model.Mode== Core.Models.CaptureMode.Video)
        //            {
        //                RecreateHandle();
        //            }
        //        }
        //    }
        //}

        protected override void UnwireModelEvents()
        {
            base.UnwireModelEvents();
            var model = Model;
            if (model != null)
            {
                model.PropertyChanged -= Model_PropertyChanged;
                model.Drawings.CollectionChanged -= Drawings_CollectionChanged;
            }
        }

        protected override void WireModelEvents()
        {
            base.WireModelEvents();
            var model = Model;
            if (model != null)
            {
                model.PropertyChanged += Model_PropertyChanged;
                model.Drawings.CollectionChanged += Drawings_CollectionChanged;
            }
        }

        private void Drawings_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case System.Collections.Specialized.NotifyCollectionChangedAction.Remove:
                    foreach (var item in e.OldItems)
                    {
                        var d = item as Drawing;
                        if (d != null)
                        {
                            var rect = d.GetInvalidationBounds();
                            Invalidate(rect);
                        }
                    }
                    break;
            }
        }


        protected override void Model_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                default:
                    base.Model_PropertyChanged(sender, e);
                    break;
                case "PenColor":
                    if (_activeDrawing is TextDrawing)
                    {
                        var tDrawing = _activeDrawing as TextDrawing;
                        tDrawing.ForeColor = Model.PenColor;

                    }
                    break;
                case "ToolboxIsOpen":
                    _toolbar.Visible = Model.ToolboxIsOpen;
                    var tBounds = _toolbar.RenderingBounds;
                    tBounds.Inflate(1, 1);
                    Invalidate(tBounds);
                    break;

                case "CanUndo":
                    var undoButton = (from b in MenuBar.Items
                                      where b.Name == "UndoButton"
                                      select b).FirstOrDefault();
                    if (undoButton == null)
                    {
                        break;
                    }
                    undoButton.Enabled = Model.CanUndo;
                    var undoBounds = undoButton.RenderingBounds;
                    undoBounds.Inflate(1, 1);
                    Invalidate(undoBounds);
                    break;
                case "CanCopy":
                    var copyButton = (from b in MenuBar.Items
                                      where b.Name == "CopyButton"
                                      select b).FirstOrDefault();
                    if (copyButton == null)
                    {
                        break;
                    }
                    copyButton.Enabled = Model.CanCopy;
                    var copyBounds = copyButton.RenderingBounds;
                    copyBounds.Inflate(1, 1);
                    Invalidate(copyBounds);
                    break;
                case "CanPrint":
                    var printButton = (from b in MenuBar.Items
                                       where b.Name == "PrintButton"
                                       select b).FirstOrDefault();
                    if (printButton == null)
                    {
                        break;
                    }
                    printButton.Enabled = Model.CanPrint;
                    var printBounds = printButton.RenderingBounds;
                    printBounds.Inflate(1, 1);
                    Invalidate(printBounds);
                    break;
                case "CanUpload":
                    var uploadButton = (from b in MenuBar.Items
                                        where b.Name == "UploadButton"
                                        select b).FirstOrDefault();
                    if (uploadButton == null)
                    {
                        break;
                    }
                    uploadButton.Enabled = Model.CanUpload;
                    var uploadBounds = uploadButton.RenderingBounds;
                    uploadBounds.Inflate(1, 1);
                    Invalidate(uploadBounds);
                    break;
                case "CanSave":
                    var saveButton = (from b in MenuBar.Items
                                      where b.Name == "SaveButton"
                                      select b).FirstOrDefault();
                    if (saveButton == null)
                    {
                        break;
                    }
                    saveButton.Enabled = Model.CanSave;
                    var saveBounds = saveButton.RenderingBounds;
                    saveBounds.Inflate(1, 1);
                    Invalidate(saveBounds);
                    break;
                case "CurrentToolColor":
                    var colorButton = (from b in _toolbar.Items
                                       where b.Name == "ColorButton"
                                       select b).FirstOrDefault() as ColorButtonView;
                    if (colorButton == null)
                    {
                        break;
                    }
                    colorButton.Color = Model.CurrentToolColor;
                    var colorBounds = colorButton.RenderingBounds;
                    colorBounds.Inflate(1, 1);
                    Invalidate(colorBounds);
                    break;
            }
        }


        protected override void UpdateToolbarViewLocations()
        {
            base.UpdateToolbarViewLocations();
            UpdateToolbarLocation();
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    CommitCurrentTool();
                    CaptureController.Current.Close();
                    AutoUpdateController.Current.Locker.Set();
                    return;
            }
        }

        protected override void OnKeyUp(KeyEventArgs e)
        {
            base.OnKeyUp(e);
            if (Model == null)
            {
                return;
            }
            switch (e.KeyCode)
            {
                case Keys.A:
                    if (e.Control)//Maximize selection
                    {

                        if (Model.CanMaximize)
                        {
                            SelectionRectangle = Bounds;
                        }
                    }
                    break;
                case Keys.C:
                case Keys.Insert:
                    if (e.Control)//Copy
                    {
                        if (Model.CanCopy)
                        {
                            Copy();
                        }
                    }
                    break;
                case Keys.P:
                    if (e.Control)//Print
                    {
                        if (Model.CanPrint)
                        {
                            Print();
                        }

                    }
                    break;
                case Keys.S:
                    if (e.Control)//Save
                    {
                        if (Model.CanSave)
                        {
                            Save();
                        }

                    }
                    break;
                case Keys.U:
                    if (e.Control)//Upload
                    {
                        if (Model.CanUpload)
                        {
                            Upload();
                        }

                    }
                    break;
                case Keys.Z:
                    if (e.Control)//Undo
                    {
                        if (Model.CanUndo)
                        {
                            Undo();
                        }

                    }
                    break;
            }
        }

        //#region Label
        //private Font LabelFont
        //{
        //    get
        //    {
        //        if (_labelFont == null)
        //        {

        //            float emSize = 10;
        //            //var dpi = Controllers.CaptureController.Current.MainWindow.Dpi();
        //            //emSize *= (float)dpi.DpiScaleX;

        //            _labelFont = new Font("Segoe UI", emSize, FontStyle.Bold);
        //        }
        //        return _labelFont;
        //    }
        //}

        //private Brush LabelBackgroundBrush
        //{
        //    get
        //    {
        //        if (_labelBackgroundBrush == null)
        //        {
        //            _labelBackgroundBrush = new SolidBrush(Color.Black);
        //        }
        //        return _labelBackgroundBrush;
        //    }
        //}

        //private Brush LabelForegroundBrush
        //{
        //    get
        //    {
        //        if (_labelForegroundBrush == null)
        //        {
        //            _labelForegroundBrush = new SolidBrush(Color.White);
        //        }
        //        return _labelForegroundBrush;
        //    }
        //}

        //private StringFormat LabelStringFormat
        //{
        //    get
        //    {
        //        if (_labelStringFormat == null)
        //        {
        //            _labelStringFormat = StringFormat.GenericTypographic.Clone() as StringFormat;
        //            _labelStringFormat.Alignment = StringAlignment.Center;
        //            _labelStringFormat.LineAlignment = StringAlignment.Center;
        //        }
        //        return _labelStringFormat;
        //    }
        //}

        //private string LabelText
        //{
        //    get
        //    {
        //        return _labelText;
        //    }
        //}

        //public void UpdateLabelRectangle()
        //{
        //    SizeF dpiSize = new SizeF(1, 1);

        //    //var dpi = Controllers.CaptureController.Current.MainWindow.Dpi();
        //    //dpiSize.Width *= (float)dpi.DpiScaleX;
        //    //dpiSize.Height *= (float)dpi.DpiScaleY;



        //    var g = CreateGraphics();
        //    var labelSize = g.MeasureString(LabelText, LabelFont, PointF.Empty, LabelStringFormat);

        //    g.Dispose();
        //    var padding = new Padding(4);
        //    //padding.Left *= dpiSize.Width;
        //    //padding.Right *= dpiSize.Width;
        //    //padding.Top *= dpiSize.Height;
        //    //padding.Bottom *= dpiSize.Height;
        //    var margin = new Padding(6);
        //    //margin.Left *= dpiSize.Width;
        //    //margin.Right *= dpiSize.Width;
        //    //margin.Top *= dpiSize.Height;
        //    //margin.Bottom *= dpiSize.Height;
        //    var selRectangle = SelectionRectangle;
        //    _labelRectangle = new RectangleF(selRectangle.Left
        //        , selRectangle.Top - (margin.Bottom + padding.Bottom + padding.Top + labelSize.Height)
        //        , labelSize.Width + padding.Left + padding.Right
        //        , labelSize.Height + padding.Top + padding.Bottom);

        //    if (_labelRectanglePath != null)
        //    {
        //        _labelRectanglePath.Dispose();
        //        _labelRectanglePath = null;
        //    }


        //    //var lBounds = new System.Windows.Rect(_labelRectangle.X
        //    //    , _labelRectangle.Y
        //    //    , _labelRectangle.Width
        //    //    , _labelRectangle.Height);
        //    //lBounds.Scale(1 / dpiSize.Width, 1 / dpiSize.Height);
        //    //Model.LabelBounds = lBounds;
            
        //}

        //#endregion

        private SolidBrush BlendBrush
        {
            get
            {
                if (_blendBrush == null)
                {
                    _blendBrush = new SolidBrush(Color.FromArgb(150, 80, 80, 80));
                }
                return _blendBrush;
            }
        }

        //private Pen DashPen
        //{
        //    get
        //    {
        //        if (this._dashPen == null)
        //        {
        //            this._dashPen = new Pen(Color.Black);
        //            this._dashPen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
                   
        //        }
        //        return this._dashPen;
        //    }
        //}

        private Pen GrabHandlePen
        {
            get
            {
                if (this._grabHandlePen == null)
                {
                    this._grabHandlePen = new Pen(Color.White);
                }
                return this._grabHandlePen;
            }
        }

        private Brush GrabHandleBrush
        {
            get
            {
                if (_grabHandleBrush == null)
                {
                    _grabHandleBrush = new SolidBrush(Color.Black);
                }
                return _grabHandleBrush;
            }
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (_blendBrush != null))
            {
                _blendBrush.Dispose();
            }
            //if (disposing && (_dashPen != null))
            //{
            //    _dashPen.Dispose();
            //}
            if (disposing && (_grabHandlePen != null))
            {
                _grabHandlePen.Dispose();
            }
            if (disposing && (_grabHandleBrush != null))
            {
                _grabHandleBrush.Dispose();
            }
            //if (disposing && (_labelBackgroundBrush != null))
            //{
            //    _labelBackgroundBrush.Dispose();
            //}
            //if (disposing && (_labelForegroundBrush != null))
            //{
            //    _labelForegroundBrush.Dispose();
            //}
            //if (disposing && (_labelFont != null))
            //{
            //    _labelFont.Dispose();
            //}
            //if (disposing && (_labelStringFormat != null))
            //{
            //    _labelStringFormat.Dispose();
            //}
            //if (disposing && (_labelRectanglePath != null))
            //{
            //    _labelRectanglePath.Dispose();
            //}
            //if (disposing && (_tooltip != null))
            //{
            //    _tooltip.Dispose();
            //}
            if (disposing)
            {
                foreach(var cursor in _cursors.Values)
                {
                    cursor.Dispose();
                }
                _cursors.Clear();
            }
            base.Dispose(disposing);
        }

        //protected override void OnSizeChanged(EventArgs e)
        //{
        //    base.OnSizeChanged(e);
        //    Invalidate();
        //}

        private Rectangle GetGrabHandleRect(GrabHandle handle)
        {
            Rectangle r = new Rectangle();
            r.Width = GrabHandleLength;
            r.Height = GrabHandleLength;
            int w = SelectionRectangle.Width;
            int h = SelectionRectangle.Height;
            switch (handle)
            {
                case GrabHandle.TopLeft:
                    r.X = SelectionRectangle.X - GrabHandleLength;
                    r.Y = SelectionRectangle.Y - GrabHandleLength;
                    if (Model.Selection.RenderOutsideBounds)
                    {
                        r.X -= 1;
                        r.Y -= 1;
                    }
                    break;
                case GrabHandle.TopMid:
                    r.X = SelectionRectangle.X + (int)((w - GrabHandleLength) / 2.0d);
                    r.Y = SelectionRectangle.Y - GrabHandleLength;
                    if (Model.Selection.RenderOutsideBounds)
                    {
                        r.Y -= 1;
                    }
                    break;
                case GrabHandle.TopRight:
                    r.X = SelectionRectangle.X + w;
                    r.Y = SelectionRectangle.Y - GrabHandleLength;
                    if (Model.Selection.RenderOutsideBounds)
                    {
                        r.X += 1;
                        r.Y -= 1;
                    }
                    break;
                case GrabHandle.MidRight:
                    r.X = SelectionRectangle.X + w;
                    r.Y = SelectionRectangle.Y + (int)((h - GrabHandleLength) / 2.0d);
                    if (Model.Selection.RenderOutsideBounds)
                    {
                        r.X += 1;
                    }
                    break;
                case GrabHandle.BottomRight:
                    r.X = SelectionRectangle.X + w;
                    r.Y = SelectionRectangle.Y + h;
                    if (Model.Selection.RenderOutsideBounds)
                    {
                        r.X += 1;
                        r.Y += 1;
                    }
                    break;
                case GrabHandle.BottomMid:
                    r.X = SelectionRectangle.X + (int)((w - GrabHandleLength) / 2.0d);
                    r.Y = SelectionRectangle.Y + h;
                    if (Model.Selection.RenderOutsideBounds)
                    {
                        r.Y += 1;
                    }
                    break;
                case GrabHandle.BottomLeft:
                    r.X = SelectionRectangle.X - GrabHandleLength;
                    r.Y = SelectionRectangle.Y + h;
                    if (Model.Selection.RenderOutsideBounds)
                    {
                        r.X -= 1;
                        r.Y += 1;
                    }
                    break;
                case GrabHandle.MidLeft:
                    r.X = SelectionRectangle.X - GrabHandleLength;
                    r.Y = SelectionRectangle.Y + (int)((h - GrabHandleLength) / 2.0d);
                    if (Model.Selection.RenderOutsideBounds)
                    {
                        r.X -= 1;
                    }
                    break;
                case GrabHandle.Middle:
                    r = SelectionRectangle;
                    break;
            }

            return r;
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            //TODO: convert to HandledMouseEventArgs 
            if (Model == null)
            {
                return;
            }

            _labelView.Visible = false;
            Invalidate(_labelView.RenderingBounds);



            HandledMouseEventArgs mEvent = new HandledMouseEventArgs(e.Button, e.Clicks, e.X, e.Y, e.Delta);
            _toolbar.OnMouseDown(mEvent);
            if (mEvent.Handled)
            {
                return;
            }
            MenuBar.OnMouseDown(mEvent);
            if (mEvent.Handled)
            {
                return;
            }

            if (Model.Mode == Core.Models.CaptureMode.Video)
            {
                return;
            }

            if (e.Button != MouseButtons.Left)
            {
                Capture = false;
                var tDrawing = _activeDrawing as TextDrawing;
                if (tDrawing != null)
                {
                    if (tDrawing.Bounds.Contains(e.Location))
                    {

                        tDrawing.Window.TextBox.Focus();
                    }

                }
                return;
            }
            Action setSelecting = () =>
            {
                Model.Selection.Status = Core.Models.SelectionStatus.Selecting;
                _mouseDownPoint = e.Location;
                Cursor = Cursors.Cross;
            };
            Model.ToolboxIsOpen = false;
            Model.MenuboxIsOpen = false;
            if (Model.Selection.Status == Core.Models.SelectionStatus.Selected)
            {
                if (Model.ToolType != Core.Models.ToolType.None)
                {

                    if(Model.ToolType == Core.Models.ToolType.Text)
                    {
                        var tDrawing = _activeDrawing as TextDrawing;
                        if(tDrawing != null)
                        {
                            using(var r  = tDrawing.GetMovingRegion())
                            {
                                if (r.IsVisible(e.Location))
                                {
                                    tDrawing.OnMouseDown(e);
                                    return;
                                }
                                else if (tDrawing.Bounds.Contains(e.Location))
                                {
                                    Capture = false;
                                    tDrawing.Window.TextBox.Focus();
                                    return;
                                }
                            }
                        }

                    }
                    CommitCurrentTool();
                    _activeDrawing = GetNewDrawing();
                    _activeDrawing.OnMouseDown(e);

                    return;
                }

                if (e.Button.HasFlag(MouseButtons.Left)){
                    if (Model.Selection.GrabHandlesVisible)
                    {

                        Rectangle rect = SelectionRectangle;
                        _mouseDownLocation = rect.Location;

                        switch (_currentGrabHandle)
                        {
                            case GrabHandle.TopLeft:
                                _mouseDownPoint = rect.BottomRight();
                                break;
                            case GrabHandle.TopRight:
                                _mouseDownPoint = rect.BottomLeft();
                                break;
                            case GrabHandle.BottomRight:
                                _mouseDownPoint = rect.TopLeft();
                                break;
                            case GrabHandle.BottomLeft:
                                _mouseDownPoint = rect.TopRight();
                                break;
                            case GrabHandle.TopMid:
                                _mouseDownPoint = rect.BottomLeft();
                                break;
                            case GrabHandle.MidRight:
                                _mouseDownPoint = rect.TopLeft();
                                break;
                            case GrabHandle.BottomMid:
                                _mouseDownPoint = rect.TopLeft();
                                break;
                            case GrabHandle.MidLeft:
                                _mouseDownPoint = rect.TopRight();
                                break;
                            case GrabHandle.Middle:
                                _mouseDownPoint = e.Location;
                                break;
                            case GrabHandle.None:
                                setSelecting();
                                break;
                        }
                    }


                }
               

            }
            else
            {
                if (e.Button.HasFlag(MouseButtons.Left))
                {
                    setSelecting();
                }
            }
        }


        protected override void OnMouseLeave(EventArgs e)
        {
            _labelView.Visible = false;
            Invalidate(_labelView.RenderingBounds);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if(Model == null)
            {
                return;
            }

      

                _toolbar.OnMouseMove(new HandledMouseEventArgs(e.Button, e.Clicks, e.X, e.Y, e.Delta));
                     MenuBar.OnMouseMove(new HandledMouseEventArgs(e.Button, e.Clicks, e.X, e.Y, e.Delta));
           if(e.Button== MouseButtons.None)
            {
                if(_toolbar.Bounds.Contains(e.Location) || MenuBar.Bounds.Contains(e.Location))
                {
                    this.Cursor = Cursors.Default;
                }
                else
                {
                    this.UpdateCursor();
                }

            }


            if (Model.ToolType == Core.Models.ToolType.Text)
            {
                var tDrawing = _activeDrawing as TextDrawing;
                if (tDrawing != null)
                {
                    var oldDrawingBounds = tDrawing.GetInvalidationBounds();

                    tDrawing.OnMouseMove(e);


                    using (var r = tDrawing.GetMovingRegion())
                    {
                        if (r.IsVisible(e.Location))
                        {
                            Cursor = Cursors.SizeAll;
                            if (e.Button.HasFlag(MouseButtons.Left))
                            {
                                Invalidate(oldDrawingBounds);

                                var newDrawingBounds = _activeDrawing.GetInvalidationBounds();
                                Invalidate(newDrawingBounds);
                            }

                            return;
                        }
                        else
                        {
                            Cursor = Cursors.Default;
                        }
                    }


                }
            }



            if (e.Button.HasFlag(MouseButtons.Left))
            {
                if (_activeDrawing != null)
                {
                    var oldDrawingBounds = _activeDrawing.GetInvalidationBounds();
                    _activeDrawing.OnMouseMove(e);
                    Invalidate(oldDrawingBounds);
                    var newDrawingBounds = _activeDrawing.GetInvalidationBounds();
                    Invalidate(newDrawingBounds);
                    return;
                }
                var mouseLocation = e.Location;
                int left = 0;
                int top = 0;
                int right = 0;
                int bottom = 0;
                if (Model.Selection.Status == Core.Models.SelectionStatus.Selecting)
                {
                    left = Math.Min(_mouseDownPoint.X, mouseLocation.X);
                    top = Math.Min(_mouseDownPoint.Y, mouseLocation.Y);
                    right = Math.Max(_mouseDownPoint.X, mouseLocation.X);
                    bottom = Math.Max(_mouseDownPoint.Y, mouseLocation.Y);
                }
                else
                {
                    Action handleResize = delegate ()
                    {
                        left = Math.Min(_mouseDownPoint.X, mouseLocation.X);
                        top = Math.Min(_mouseDownPoint.Y, mouseLocation.Y);
                        right = Math.Max(_mouseDownPoint.X, mouseLocation.X);
                        bottom = Math.Max(_mouseDownPoint.Y, mouseLocation.Y);
                        if (mouseLocation.X < _mouseDownPoint.X)
                        {
                            if (mouseLocation.Y < _mouseDownPoint.Y)
                            {
                                Cursor = Cursors.SizeNWSE;

                            }
                            else if (mouseLocation.Y == _mouseDownPoint.Y)
                            {
                                Cursor = Cursors.SizeWE;
                            }
                            else
                            {
                                Cursor = Cursors.SizeNESW;
                            }
                        }
                        else if (mouseLocation.X == _mouseDownPoint.X)
                        {
                            Cursor = Cursors.SizeNS;
                        }
                        else
                        {
                            if (mouseLocation.Y < _mouseDownPoint.Y)
                            {
                                Cursor = Cursors.SizeNESW;
                            }
                            else if (mouseLocation.Y == _mouseDownPoint.Y)
                            {
                                Cursor = Cursors.SizeWE;
                            }
                            else
                            {
                                Cursor = Cursors.SizeNWSE;
                            }
                        }
                    };

                    switch (this._currentGrabHandle)
                    {
                        case GrabHandle.Middle://move
                            left = _mouseDownLocation.X + (mouseLocation.X - _mouseDownPoint.X);
                            top = _mouseDownLocation.Y + (mouseLocation.Y - _mouseDownPoint.Y);
                            right = left + SelectionRectangle.Width;
                            bottom = top + SelectionRectangle.Height;
                            break;
                        case GrabHandle.TopLeft://resize the entire shape
                        case GrabHandle.TopRight:
                        case GrabHandle.BottomRight:
                        case GrabHandle.BottomLeft:
                            handleResize();
                            break;
                        case GrabHandle.TopMid:
                        case GrabHandle.BottomMid:
                            left = SelectionRectangle.Left;
                            top = Math.Min(_mouseDownPoint.Y, mouseLocation.Y);
                            right = SelectionRectangle.Right;
                            bottom = Math.Max(_mouseDownPoint.Y, mouseLocation.Y);
                            Cursor = Cursors.SizeNS;
                            break;
                        case GrabHandle.MidLeft:
                        case GrabHandle.MidRight:
                            left = Math.Min(_mouseDownPoint.X, mouseLocation.X);
                            top = SelectionRectangle.Top;
                            right = Math.Max(_mouseDownPoint.X, mouseLocation.X);
                            bottom = SelectionRectangle.Bottom;
                            Cursor = Cursors.SizeWE;
                            break;
                        default:
                            return;
                    }

                }
                SelectionRectangle = Rectangle.FromLTRB(left, top, right, bottom);
            }
            else if (e.Button == MouseButtons.None)
            {
                if (Model.ToolType != Core.Models.ToolType.None)
                {
                    return;
                }

                if (Model.Selection.GrabHandlesVisible)
                {
                    var gHandles = Enum.GetValues(typeof(GrabHandle));
                    foreach (GrabHandle gh in gHandles)
                    {
                        Rectangle r = this.GetGrabHandleRect(gh);
                        if (r.Contains(e.Location))
                        {

                            switch (gh)
                            {
                                case GrabHandle.TopLeft:
                                case GrabHandle.BottomRight:
                                    this.Cursor = Cursors.SizeNWSE;
                                    break;
                                case GrabHandle.TopMid:
                                case GrabHandle.BottomMid:
                                    this.Cursor = Cursors.SizeNS;
                                    break;
                                case GrabHandle.TopRight:
                                case GrabHandle.BottomLeft:
                                    this.Cursor = Cursors.SizeNESW;
                                    break;
                                case GrabHandle.MidRight:
                                case GrabHandle.MidLeft:
                                    this.Cursor = Cursors.SizeWE;
                                    break;
                                case GrabHandle.Middle:
                                    this.Cursor = Cursors.SizeAll;
                                    break;
                                case GrabHandle.None:
                                    this.Cursor = Cursors.Arrow;
                                    var oldLabelViewBounds = _labelView.RenderingBounds;
                                    _labelView.Visible = Model.Selection.Status == Core.Models.SelectionStatus.None;
                                    _labelView.Location = new Point(e.Location.X, e.Location.Y + Cursor.Size.Height);
                                    Invalidate(oldLabelViewBounds);
                                    Invalidate(_labelView.RenderingBounds);
                                    break;
                            }
                            _currentGrabHandle = gh;
                            return;
                        }
                    }
                }

                _currentGrabHandle = GrabHandle.None;
                this.Cursor = Cursors.Arrow;
                var oldLabelBounds = _labelView.RenderingBounds;
                _labelView.Visible = Model.Selection.Status == Core.Models.SelectionStatus.None;
                _labelView.Location = new Point(e.Location.X, e.Location.Y + Cursor.Size.Height);
                Invalidate(oldLabelBounds);
                Invalidate(_labelView.RenderingBounds);
            }
        }


        protected override void OnMouseUp(MouseEventArgs e)
        {
            if (Model == null)
            {
                return;
            }



            HandledMouseEventArgs mEvent = new HandledMouseEventArgs(e.Button, e.Clicks, e.X, e.Y, e.Delta);
            _toolbar.OnMouseUp(mEvent);
            if (mEvent.Handled)
            {
                return;
            }
            MenuBar.OnMouseUp(mEvent);
            if (mEvent.Handled)
            {
                return;
            }

            if (Model.Mode == Core.Models.CaptureMode.Video)
            {
                return;
            }

            if (MouseButtons.HasFlag(MouseButtons.Left))//left button is still pressed
            {
                return;
            }

            if (_activeDrawing != null)
            {

                var tDrawing = _activeDrawing as TextDrawing;

                _activeDrawing.OnMouseUp(e);

                if (tDrawing != null)
                {

                    System.Windows.Forms.Integration.ElementHost.EnableModelessKeyboardInterop(tDrawing.Window);
                    WindowInteropHelper helper = new WindowInteropHelper(tDrawing.Window);
                    helper.Owner = Handle;
                    tDrawing.Window.TextBox.Focus();
                    var newBounds = tDrawing.GetInvalidationBounds();
                    Invalidate(newBounds);
                }
                else
                {

                    CommitCurrentTool();
                }

                Model.UpdateMenuboxProperties();
                Model.UpdateToolboxProperties();

                return;
            }

            var model = Model;
            model.Selection.Rectangle = SelectionRectangle;
            if (SelectionRectangle.Width > 0 && SelectionRectangle.Height > 0)
            {
                model.Selection.Status = Core.Models.SelectionStatus.Selected;
            }
            else
            {
                model.Selection.Status = Core.Models.SelectionStatus.None;
                Invalidate();
            }

            Model.UpdateToolboxProperties();
            Model.UpdateMenuboxProperties();
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            if (Model == null)
            {
                return;
            }
            switch (Model.ToolType)
            {
                case Core.Models.ToolType.None:
                case Core.Models.ToolType.Text:
                    return;
            }
            var s = Model.ToolType == Core.Models.ToolType.Marker ? _markerSize : _penSize;
            var maxS = Model.ToolType == Core.Models.ToolType.Marker ? 40 : 11;
            var minS = Model.ToolType == Core.Models.ToolType.Marker ? 10 : 2;
            if (e.Delta > 0)
            {
                if (Model.ToolType == Core.Models.ToolType.Marker)
                {
                    _markerSize = s < maxS ? s + 1 : s;
                }
                else
                {
                    _penSize = s < maxS ? s + 1 : s;
                }
            }
            else
            {
                if (Model.ToolType == Core.Models.ToolType.Marker)
                {
                    _markerSize = s > minS ? s - 1 : s;
                }
                else
                {
                    _penSize = s > minS ? s - 1 : s;
                }
            }


            if (_activeDrawing != null)
            {
               var oldBounds =  _activeDrawing.GetInvalidationBounds();
                _activeDrawing.BorderSize = GetCurrentPointerSize();
                Invalidate(oldBounds);
                Invalidate(_activeDrawing.GetInvalidationBounds());
            }

            UpdateCursor();
        }

        private Drawing GetNewDrawing()
        {
            if (Model == null)
            {
                return null;
            }
            switch (Model.ToolType)
            {
                case Core.Models.ToolType.Rectangle:
                    return new RectangleDrawing()
                    {
                        ForeColor = Model.PenColor
                        ,
                        BorderSize = GetCurrentPointerSize()
                    };
                case Core.Models.ToolType.Ellipse:
                    return new EllipseDrawing()
                    {
                        ForeColor = Model.PenColor
                       ,
                        BorderSize = GetCurrentPointerSize()
                    };
                case Core.Models.ToolType.Line:
                    return new LineDrawing()
                    {
                        ForeColor = Model.PenColor
                      ,
                        BorderSize = GetCurrentPointerSize()
                    };
                case Core.Models.ToolType.Arrow:
                    return new ArrowDrawing()
                    {
                        ForeColor = Model.PenColor
                    ,
                        BorderSize = GetCurrentPointerSize()
                    };
                case Core.Models.ToolType.Pen:
                    return new PenDrawing()
                    {
                        ForeColor = Model.PenColor
                   ,
                        BorderSize = GetCurrentPointerSize()
                    };
                case Core.Models.ToolType.Marker:
                    return new MarkerDrawing()
                    {
                        ForeColor = Model.MarkerColor
                 ,
                        BorderSize = GetCurrentPointerSize()
                    };
                case Core.Models.ToolType.Text:
                    return new TextDrawing()
                    {
                        ForeColor = Model.PenColor
                    };
            }

            return null;
        }

        //public Rectangle SelectionRectangle
        //{
        //    get
        //    {
        //        return _selectionRectangle;
        //    }
        //    set
        //    {
        //        if (_selectionRectangle != value)
        //        {
        //            var oldRect = GetInvalidationSelectionRectangle();
        //            var lRect = Rectangle.Round(_labelRectangle);
        //            lRect.Inflate(2, 2);
        //            _selectionRectangle = value;
        //            _labelText = string.Format("{0}x{1}", _selectionRectangle.Width, _selectionRectangle.Height);

        //            UpdateLabelRectangle();
        //            Invalidate(oldRect);
        //            Invalidate(GetInvalidationSelectionRectangle());
        //            Invalidate(lRect);
        //            lRect = Rectangle.Round(_labelRectangle);
        //            lRect.Inflate(2, 2);
        //            Invalidate(lRect);
        //            //Model.LabelText = _labelText;
        //            UpdateToolbarLocation();
        //            UpdateMenubarLocation();
        //        }
        //    }
        //}

        //private void UpdateMenubarLocation()
        //{
        //    var oldBounds = _menubar.RenderingBounds;
        //    oldBounds.Inflate(1, 1);
        //    _menubar.Location = GetMenuboxLocation();
        //    var bounds = _menubar.RenderingBounds;
        //    bounds.Inflate(1, 1);
        //    Invalidate(oldBounds);
        //    Invalidate(bounds);
        //}

        private void UpdateToolbarLocation()
        {
            var oldBounds = _toolbar.RenderingBounds;
            oldBounds.Inflate(1, 1);
            _toolbar.Location = GetToolboxLocation();
            var bounds = _toolbar.RenderingBounds;
            bounds.Inflate(1, 1);
            Invalidate(oldBounds);
            Invalidate(bounds);
        }

        private Point GetToolboxLocation()
        {
            var size = _toolbar.Size;
            var location = new Point(SelectionRectangle.Right - size.Width
                , SelectionRectangle.Bottom + ToolbarOffset);
            var itemRect = new Rectangle(location, size);
            //check y coord
            if (itemRect.Bottom > Size.Height)
            {
                location.Y = SelectionRectangle.Top - (ToolbarOffset + size.Height);
                itemRect.Location = location;
            }
            if (itemRect.Top < 0)
            {
                location.Y = SelectionRectangle.Bottom - size.Height;
                itemRect.Location = location;
            }
            //check x coord
            if (itemRect.Right > Size.Width)
            {
                location.X = Size.Width - size.Width;
                itemRect.Location = location;
            }
            if (itemRect.Left < 0)
            {
                location.X = 0;
            }
            return location;
        }

        //private Point GetMenuboxLocation()
        //{
        //    var size = _menubar.Size;
        //    var location = new Point(SelectionRectangle.Right + _toolbarOffset
        //        , SelectionRectangle.Bottom - size.Height);
        //    var itemRect = new Rectangle(location, size);
        //    //check x coord
        //    if (itemRect.Right > Size.Width)
        //    {
        //        location.X = SelectionRectangle.Left - (_toolbarOffset + size.Width);
        //        itemRect.Location = location;
        //    }
        //    if (itemRect.Left < 0)
        //    {
        //        location.X = SelectionRectangle.Right - size.Width;
        //        itemRect.Location = location;
        //    }
        //    //check y coord
        //    if (itemRect.Top < 0)
        //    {
        //        location.Y = 0;
        //        itemRect.Location = location;
        //    }
        //    if (itemRect.Bottom > Size.Height)
        //    {
        //        location.Y = Size.Height - size.Height;
        //    }
        //    return location;
        //}

        //private Rectangle GetInvalidationSelectionRectangle()
        //{
        //    var iRect = _selectionRectangle;
        //    if (Model.Selection.GrabHandlesVisible)
        //    {
        //        iRect.Inflate(_grabHandleLength, _grabHandleLength);
        //    }
        //    if (Model.Selection.RenderOutsideBounds)
        //    {
        //        iRect.Inflate(1, 1);
        //    }
        //    iRect.Inflate(2, 2);
        //    return iRect;
        //}

        protected override void OnPaint(PaintEventArgs e)
        {
            Draw(e.Graphics);
        }

        public override void Draw(Graphics g)
        {
            if (Model == null)
            {
                return;
            }

            switch (Model.Mode)
            {
                case Core.Models.CaptureMode.Image:
                    if (Model.Screenshot != null)
                    {
                        g.DrawImage(Model.Screenshot, new Rectangle(Point.Empty, Size));
                    }
                    break;
            }




            var selRectangle = SelectionRectangle;
            bool drawRectangle = Model.Selection.Status != Core.Models.SelectionStatus.None;
            if (Model.Selection.RenderOutsideBounds)
            {
                selRectangle.Inflate(1, 1);
            }


            var state = g.Save();
                    if (drawRectangle)
                    {
                        g.ExcludeClip(selRectangle);
                    }

                    g.FillRectangle(BlendBrush, new Rectangle(Point.Empty, ClientSize));
            
            g.Restore(state);

                    foreach (var dr in Model.Drawings)
                    {
                        dr.OnPaint(g);
                    }
                    if (_activeDrawing != null)
                    {
                        _activeDrawing.OnPaint(g);
                    }


            base.Draw(g);


            //state = g.Save();
            //if (drawRectangle)
            //{
            //    g.InterpolationMode = InterpolationMode.Low;
            //    g.CompositingQuality = CompositingQuality.HighSpeed;
            //    g.PixelOffsetMode = PixelOffsetMode.HighSpeed;
            //    g.SmoothingMode = SmoothingMode.HighSpeed;
            //    g.DrawRectangle(Pens.White, selRectangle);
            //    g.DrawRectangle(DashPen, selRectangle);
            //}

            //g.Restore(state);


            //if (!string.IsNullOrWhiteSpace(LabelText) && drawRectangle)
            //{
            //    state = g.Save();
            //    g.SmoothingMode = SmoothingMode.AntiAlias;
            //    g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
            //    g.FillPath(LabelBackgroundBrush, LabelRectanglePath);
            //    g.DrawString(LabelText, LabelFont, LabelForegroundBrush, _labelRectangle, LabelStringFormat);

            //    g.Restore(state);
            //}

            switch (Model.Mode)
            {
                case Core.Models.CaptureMode.Image:
                    if (drawRectangle && Model.Selection.GrabHandlesVisible)
                    {
                        var gHandles = Enum.GetValues(typeof(GrabHandle));
                        foreach (GrabHandle gh in gHandles)
                        {
                            switch (gh)
                            {
                                case GrabHandle.None:
                                case GrabHandle.Middle:
                                    continue;
                                default:
                                    var ghRect = GetGrabHandleRect(gh);
                                    g.FillRectangle(GrabHandleBrush, ghRect);
                                    g.DrawRectangle(GrabHandlePen, ghRect);
                                    break;
                            }
                        }
                    }
                    break;
            }

            state = g.Save();
            _toolbar.OnPaint(g);
            //_menubar.OnPaint(g);
            g.Restore(state);

            _labelView.OnPaint(g);
        }

        //public GraphicsPath LabelRectanglePath
        //{
        //    get
        //    {
        //        if (_labelRectanglePath == null)
        //        {
        //            float radius = 6;
        //            //var dpi = Controllers.CaptureController.Current.MainWindow.Dpi();
        //            //radius *= (float)dpi.DpiScaleX;

        //            _labelRectanglePath = Extensions.Drawing2DExtensions.RoundedRect(_labelRectangle, radius);
        //        }
        //        return _labelRectanglePath;
        //    }
        //}

        protected override void WndProc(ref Message m)
        {

            switch (m.Msg)
            {
                case 516:
                case 518:
                case 519:
                case 521:
                case 523:
                case 525:

                    Capture = false;
                    var tDrawing = _activeDrawing as TextDrawing;
                    tDrawing?.Window.TextBox?.Focus();
                    return;
            }
            base.WndProc(ref m);

        }

        #region Cursor  
        private void UpdateCursor()
        {
            if (Model == null)
            {
                return;
            }
            switch (Model.ToolType)
            {
                case Core.Models.ToolType.Arrow:
                case Core.Models.ToolType.Ellipse:
                case Core.Models.ToolType.Line:
                case Core.Models.ToolType.Pen:
                case Core.Models.ToolType.Rectangle:
                case Core.Models.ToolType.Marker:
                    this.Cursor = this.GetCurrentCursor();
                    break;
                case Core.Models.ToolType.Text:
                    this.Cursor = Cursors.Arrow;
                    break;
                case Core.Models.ToolType.None:
                    this.Cursor = Cursors.Arrow;
                    break;
            }
        }

        private Cursor GetCurrentCursor()
        {
            int s = this.GetCurrentPointerSize();
            Cursor c = null;
            if (this._cursors.ContainsKey(s))
            {
                c = this._cursors[s];

            }
            else
            {
                c = CursorExtensions.GetArrowCursorWithEllipse(this.GetPenEllipse());
                this._cursors[s] = c;
            }
            return c;
        }

        private int GetCurrentPointerSize()
        {
            int size = 0;
            switch (Model.ToolType)
            {
                case Core.Models.ToolType.Arrow:
                case Core.Models.ToolType.Ellipse:
                case Core.Models.ToolType.Line:
                case Core.Models.ToolType.Pen:
                case Core.Models.ToolType.Rectangle:
                    size = _penSize;
                    break;
                case Core.Models.ToolType.Marker:
                    size = _markerSize;
                    break;
            }

            if (size % 2 != 0)
            {
                size += 1;
            }

            int minLength = 2;
            if (size < minLength)
            {
                size = minLength;
            }

            return size;
        }

        private System.Windows.Shapes.Ellipse GetPenEllipse()
        {
            var penEllipse = new System.Windows.Shapes.Ellipse();
            penEllipse.Fill = null;
            penEllipse.Stroke = System.Windows.Media.Brushes.Black;
            penEllipse.StrokeThickness = 1.0d;
            penEllipse.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            penEllipse.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            int s = this.GetCurrentPointerSize();
            penEllipse.Width = s;
            penEllipse.Height = s;
            return penEllipse;
        }
        #endregion
    }
}
