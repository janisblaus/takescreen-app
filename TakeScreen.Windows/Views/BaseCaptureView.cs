﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TakeScreen.Windows.Controllers;
using TakeScreen.Windows.Extensions;
using TakeScreen.Windows.Models;

namespace TakeScreen.Windows.Views
{
    public partial class BaseCaptureView : Form
    {
        private Capture _model;
        private Rectangle _selectionRectangle;
        private int _grabHandleLength;

        private string _labelText;
        private Font _labelFont;
        private Brush _labelBackgroundBrush;
        private StringFormat _labelStringFormat;
        private Brush _labelForegroundBrush;
        private RectangleF _labelRectangle;
        private GraphicsPath _labelRectanglePath;

        private ToolbarView _menubar;
        private Size _buttonSize;
        private Padding _buttonPadding;
        private Padding _toolbarPadding;
        private int _toolbarOffset;
        private UIView _previousMenubarHighlighted;

        private ToolTip _tooltip;


        public BaseCaptureView()
        {
            InitializeComponent();
            _grabHandleLength = 6;
            _toolbarPadding = new Padding(0);
            _toolbarOffset = 8;

            DpiInfo dpi;
            using (Graphics g = CreateGraphics())
            {
                dpi = new DpiInfo((double)g.DpiX / 96.0, (double)g.DpiY / 96.0);
            }

            _buttonSize = new Size((int)(28.0 * dpi.DpiScaleX), (int)(28.0 * dpi.DpiScaleY));
            double allButtonPadding = 4.0;
            _buttonPadding = new Padding((int)(allButtonPadding * dpi.DpiScaleX)
                , (int)(allButtonPadding * dpi.DpiScaleY)
                , (int)(allButtonPadding * dpi.DpiScaleX)
                 , (int)(allButtonPadding * dpi.DpiScaleY)
                );

            _menubar = new ToolbarView();
            _menubar.Visible = false;
            _menubar.Padding = _toolbarPadding;
            _menubar.HighlightedItemChanging += Menubar_HighlightedItemChanging;
            _menubar.HighlightedItemChanged += Menubar_HighlightedItemChanged;
            _menubar.ItemClicked += Menubar_ItemClicked;
            _menubar.Orientation = Orientation.Vertical;
            ConfigureMenuBar();
       
       

            _tooltip = new ToolTip();
            _tooltip.ShowAlways = true;

        }
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // CaptureView
            // 
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "BaseCaptureView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            ShowInTaskbar = false;
            TopMost = true;
            this.Text = "Capture";
            this.ResumeLayout(false);

        }

        protected virtual void ConfigureMenuBar()
        {
            _menubar.Items.Add(new ButtonView()
            {
                Image = Core.Properties.Resources.StopImage,
                Size = _buttonSize,
                Padding = _buttonPadding,
                Name = "StopButton",
                Enabled = true
            });
        }

        public int GrabHandleLength
        {
            get
            {
                return _grabHandleLength;
            }
        }

        public Padding ToolbarPadding
        {
            get
            {
                return _toolbarPadding;
            }
        }

        public Size ButtonSize
        {
            get
            {
                return _buttonSize;
            }
        }

        public Padding ButtonPadding
        {
            get
            {
                return _buttonPadding;
            }
        }

        public ToolTip ToolTip
        {
            get
            {
                return _tooltip;
            }
        }

        public ToolbarView MenuBar
        {
            get
            {
                return _menubar;
            }
        }

        public int ToolbarOffset
        {
            get
            {
                return _toolbarOffset;
            }
        }


        protected virtual void Menubar_ItemClicked(ToolbarView sender, ToolbarViewItemClickedEventArgs e)
        {
        }



        private void Menubar_HighlightedItemChanged(object sender, EventArgs e)
        {
            Rectangle bounds;
            if (_previousMenubarHighlighted != null)
            {
                bounds = _previousMenubarHighlighted.RenderingBounds;
                bounds.Inflate(1, 1);
                Invalidate(bounds);
            }
            if (_menubar.HighlightedItem == null)
            {
                _tooltip.Hide(this);
            }
            else
            {
                bounds = _menubar.HighlightedItem.RenderingBounds;
                bounds.Inflate(1, 1);
                Invalidate(bounds);

                var toolTipText = GetToolTipText(_menubar.HighlightedItem);
                if (string.IsNullOrWhiteSpace(toolTipText))
                {
                    _tooltip.Hide(this);
                }
                else
                {
                    var point = _menubar.HighlightedItem.Bounds.BottomRight();
                    _tooltip.Show(toolTipText, this, point);
                }

            }

        }


        private void Menubar_HighlightedItemChanging(object sender, EventArgs e)
        {
            _previousMenubarHighlighted = _menubar.HighlightedItem;
        }

        public string GetToolTipText(ButtonView item)
        {
            switch (item.Name)
            {
                case "PenButton":
                    return Core.Properties.Resources.PenCaption;
                case "RectangleButton":
                    return Core.Properties.Resources.RectCaption;
                case "EllipseButton":
                    return Core.Properties.Resources.EllipseCaption;
                case "ArrowButton":
                    return Core.Properties.Resources.ArrowCaption;
                case "LineButton":
                    return Core.Properties.Resources.LineCaption;
                case "TextButton":
                    return Core.Properties.Resources.TextCaption;
                case "MarkerButton":
                    return Core.Properties.Resources.MarkerCaption;
                case "CloseButton":
                    return Core.Properties.Resources.Close;
                case "UploadButton":
                    return Core.Properties.Resources.UploadToTakesc;
                case "RecordButton":
                    return Core.Properties.Resources.Record;
                case "StopButton":
                    return Core.Properties.Resources.Stop;
                case "SaveButton":
                    return Core.Properties.Resources.Save;
                case "CopyButton":
                    return Core.Properties.Resources.Copy;
                case "PrintButton":
                    return Core.Properties.Resources.Print;
                case "UndoButton":
                    return Core.Properties.Resources.Undo;
            }

            return null;
        }

        public Capture Model
        {
            get
            {
                return _model;
            }
            set
            {
                if (_model != value)
                {
                    UnwireModelEvents();

                    _model = value;
                    WireModelEvents();
                }
            }
        }

        protected virtual void UnwireModelEvents()
        {
            var model = Model;
            if (model != null)
            {
                model.PropertyChanged -= Model_PropertyChanged;
            }
        }

        protected virtual void WireModelEvents()
        {
            var model = Model;
            if (model != null)
            {
                model.PropertyChanged += Model_PropertyChanged;

            }

        }

        protected virtual void Model_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {

                case "MenuboxIsOpen":
                    MenuBar.Visible = Model.MenuboxIsOpen;
                    var mBounds = MenuBar.RenderingBounds;
                    mBounds.Inflate(1, 1);
                    Invalidate(mBounds);
                    break;
            }
        }


        #region Label
        private Font LabelFont
        {
            get
            {
                if (_labelFont == null)
                {

                    float emSize = 10;
                    _labelFont = new Font("Segoe UI", emSize, FontStyle.Bold);
                }
                return _labelFont;
            }
        }

        private Brush LabelBackgroundBrush
        {
            get
            {
                if (_labelBackgroundBrush == null)
                {
                    _labelBackgroundBrush = new SolidBrush(Color.Black);
                }
                return _labelBackgroundBrush;
            }
        }

        private Brush LabelForegroundBrush
        {
            get
            {
                if (_labelForegroundBrush == null)
                {
                    _labelForegroundBrush = new SolidBrush(Color.White);
                }
                return _labelForegroundBrush;
            }
        }

        private StringFormat LabelStringFormat
        {
            get
            {
                if (_labelStringFormat == null)
                {
                    _labelStringFormat = StringFormat.GenericTypographic.Clone() as StringFormat;
                    _labelStringFormat.Alignment = StringAlignment.Center;
                    _labelStringFormat.LineAlignment = StringAlignment.Center;
                }
                return _labelStringFormat;
            }
        }

        private string LabelText
        {
            get
            {
                return _labelText;
            }
        }

        public void UpdateLabelRectangle()
        {
            SizeF dpiSize = new SizeF(1, 1);

            var g = CreateGraphics();
            var labelSize = g.MeasureString(LabelText, LabelFont, PointF.Empty, LabelStringFormat);

            g.Dispose();
            var padding = new Padding(4);
            var margin = new Padding(6);
            var selRectangle = SelectionRectangle;
            _labelRectangle = new RectangleF(selRectangle.Left
                , selRectangle.Top - (margin.Bottom + padding.Bottom + padding.Top + labelSize.Height)
                , labelSize.Width + padding.Left + padding.Right
                , labelSize.Height + padding.Top + padding.Bottom);

            if (_labelRectanglePath != null)
            {
                _labelRectanglePath.Dispose();
                _labelRectanglePath = null;
            }
        }


        public GraphicsPath LabelRectanglePath
        {
            get
            {
                if (_labelRectanglePath == null)
                {
                    float radius = 6;
                    _labelRectanglePath = Extensions.Drawing2DExtensions.RoundedRect(_labelRectangle, radius);
                }
                return _labelRectanglePath;
            }
        }

        #endregion

        public Rectangle SelectionRectangle
        {
            get
            {
                return _selectionRectangle;
            }
            set
            {
                if (_selectionRectangle != value)
                {
                    var oldRect = GetInvalidationSelectionRectangle();
                    var lRect = Rectangle.Round(_labelRectangle);
                    lRect.Inflate(2, 2);
                    _selectionRectangle = value;
                    _labelText = string.Format("{0}x{1}", _selectionRectangle.Width, _selectionRectangle.Height);

                    UpdateLabelRectangle();
                    Invalidate(oldRect);
                    Invalidate(GetInvalidationSelectionRectangle());
                    Invalidate(lRect);
                    lRect = Rectangle.Round(_labelRectangle);
                    lRect.Inflate(2, 2);
                    Invalidate(lRect);
                    UpdateToolbarViewLocations();
                }
            }
        }

        protected virtual void UpdateToolbarViewLocations()
        {
         
            UpdateMenubarLocation();
        }


        private void UpdateMenubarLocation()
        {
            var oldBounds = _menubar.RenderingBounds;
            oldBounds.Inflate(1, 1);
            _menubar.Location = GetMenuboxLocation();
            var bounds = _menubar.RenderingBounds;
            bounds.Inflate(1, 1);
            Invalidate(oldBounds);
            Invalidate(bounds);
        }

        private Point GetMenuboxLocation()
        {
            var size = _menubar.Size;
            var location = new Point(SelectionRectangle.Right + _toolbarOffset
                , SelectionRectangle.Bottom - size.Height);
            var itemRect = new Rectangle(location, size);
            //check x coord
            if (itemRect.Right > Size.Width)
            {
                location.X = SelectionRectangle.Left - (_toolbarOffset + size.Width);
                itemRect.Location = location;
            }
            if (itemRect.Left < 0)
            {
                location.X = SelectionRectangle.Right - size.Width;
                itemRect.Location = location;
            }
            //check y coord
            if (itemRect.Top < 0)
            {
                location.Y = 0;
                itemRect.Location = location;
            }
            if (itemRect.Bottom > Size.Height)
            {
                location.Y = Size.Height - size.Height;
            }
            return location;
        }

        private Rectangle GetInvalidationSelectionRectangle()
        {
            var iRect = _selectionRectangle;
            if (Model.Selection.GrabHandlesVisible)
            {
                iRect.Inflate(_grabHandleLength, _grabHandleLength);
            }
            if (Model.Selection.RenderOutsideBounds)
            {
                iRect.Inflate(1, 1);
            }
            iRect.Inflate(2, 2);
            return iRect;
        }

        public virtual void Draw(Graphics g)
        {
            if (Model == null)
            {
                return;
            }

            var selRectangle = SelectionRectangle;
            bool drawRectangle = Model.Selection.Status != Core.Models.SelectionStatus.None;
            if (Model.Selection.RenderOutsideBounds)
            {
                selRectangle.Inflate(1, 1);
            }




          var  state = g.Save();
            if (drawRectangle)
            {
                g.InterpolationMode = InterpolationMode.Low;
                g.CompositingQuality = CompositingQuality.HighSpeed;
                g.PixelOffsetMode = PixelOffsetMode.HighSpeed;
                g.SmoothingMode = SmoothingMode.HighSpeed;
                Rectangle inRect = selRectangle;
                inRect.Inflate(-1, -1);
                ControlPaint.DrawSelectionFrame(g, true,selRectangle,inRect,Color.White);
            }

            g.Restore(state);


            if (!string.IsNullOrWhiteSpace(LabelText) && drawRectangle)
            {
                state = g.Save();
                g.SmoothingMode = SmoothingMode.AntiAlias;
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.ClearTypeGridFit;
                g.FillPath(LabelBackgroundBrush, LabelRectanglePath);
                g.DrawString(LabelText, LabelFont, LabelForegroundBrush, _labelRectangle, LabelStringFormat);

                g.Restore(state);
            }

            state = g.Save();
            _menubar.OnPaint(g);
            g.Restore(state);

        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            //TODO: convert to HandledMouseEventArgs 
            HandledMouseEventArgs mEvent = new HandledMouseEventArgs(e.Button, e.Clicks, e.X, e.Y, e.Delta);
            _menubar.OnMouseDown(mEvent);
        }

        protected override void OnMouseLeave(EventArgs e)
        {
            _menubar.HighlightedItem = null;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            MenuBar.OnMouseMove(new HandledMouseEventArgs(e.Button, e.Clicks, e.X, e.Y, e.Delta));
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            HandledMouseEventArgs mEvent = new HandledMouseEventArgs(e.Button, e.Clicks, e.X, e.Y, e.Delta);
            MenuBar.OnMouseUp(mEvent);
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
  
            if (disposing && (_labelBackgroundBrush != null))
            {
                _labelBackgroundBrush.Dispose();
            }
            if (disposing && (_labelForegroundBrush != null))
            {
                _labelForegroundBrush.Dispose();
            }
            if (disposing && (_labelFont != null))
            {
                _labelFont.Dispose();
            }
            if (disposing && (_labelStringFormat != null))
            {
                _labelStringFormat.Dispose();
            }
            if (disposing && (_labelRectanglePath != null))
            {
                _labelRectanglePath.Dispose();
            }
            if (disposing && (_tooltip != null))
            {
                _tooltip.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}
