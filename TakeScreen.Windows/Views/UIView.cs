﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TakeScreen.Core.Views;

namespace TakeScreen.Windows.Views
{
   public class UIView:BaseUIView
    {

        public Padding Padding { get; set; }
        public Padding Margin { get; set; }
        public virtual void OnPaint(Graphics g)
        {

        }

        public virtual void OnMouseMove(HandledMouseEventArgs e)
        {

        }

        public virtual void OnMouseDown(HandledMouseEventArgs e)
        {

        }

        public virtual void OnMouseUp (HandledMouseEventArgs e)
        {

        }
    }
}
