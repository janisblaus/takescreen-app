﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TakeScreen.Windows.Drawings
{
   public class Drawing
    {
        internal Rectangle _bounds;
         internal Point _mouseDownPoint;
        
        public virtual Rectangle Bounds {
            get
            {
                return _bounds;
            }
        }

        internal virtual Pen GetBorderPen()
        {
            return new Pen(ForeColor, BorderSize) {
            Alignment = System.Drawing.Drawing2D.PenAlignment.Center};
        }

        public int BorderSize { get; set; }

        public virtual Color ForeColor { get; set; }

        public virtual void OnPaint(Graphics g)
        {

        }

        public virtual void OnMouseDown(MouseEventArgs e)
        {
            _mouseDownPoint = e.Location;
        }

        public virtual void OnMouseMove(MouseEventArgs e)
        {
            if (e.Button.HasFlag( MouseButtons.Left))
            {
                var left = Math.Min(_mouseDownPoint.X, e.Location.X);
                var top = Math.Min(_mouseDownPoint.Y, e.Location.Y);
                var right = Math.Max(_mouseDownPoint.X, e.Location.X);
                var bottom = Math.Max(_mouseDownPoint.Y, e.Location.Y);
                _bounds = Rectangle.FromLTRB(left, top, right, bottom);
            }
          
        }

        public virtual void OnMouseUp(MouseEventArgs e)
        {

        }

        public virtual Rectangle GetInvalidationBounds()
        {
            var b = _bounds;
            b.Inflate(BorderSize*2, BorderSize*2);
            b.Inflate(1, 1);
            return b;
        }

        public virtual void Commit()
        {

        }

    }
}
