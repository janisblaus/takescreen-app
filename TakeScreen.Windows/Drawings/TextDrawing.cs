﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TakeScreen.Windows.Extensions;
using TakeScreen.Windows.Drawings.WPF;

namespace TakeScreen.Windows.Drawings
{
    public partial class TextDrawing : Drawing
    {

        private string _text;
        private bool _isMoving;
        private Point _oldLocation;
      
        private bool _isEditing;
        private bool _wasEditing;
        private Bitmap _image;

        public string Text
        {
            get { return _text; }
        }

          public override Color ForeColor {
            get => base.ForeColor;

            set
            {
                base.ForeColor = value;
                if(_window != null)
                {
                    _window.TextBox.Foreground = new System.Windows.Media.SolidColorBrush(value.ToMediaColor());
                }
            }
        }

        public bool IsEditing
        {
            get
            {
                return _isEditing;
            }
            set
            {
                if (_isEditing != value)
                {
                    _isEditing = value;
                    if (_window != null)
                    {
                        if (_isEditing)
                        {
                            UpdateWindowLocation();
                            _window.Show();
                        }
                        else
                        {
                            _text = _window.TextBox.Text;
                            UpdateBoundsFromWindow();
                            UpdateImage();

                            _window.Hide();
                        }
                    }

                }
            }
        }

        private void UpdateBoundsFromWindow()
        {
            var x = _window.Left;
            var y = _window.Top;
            var w = _window.ActualWidth;
            var h = _window.ActualHeight;
            var dpi = _window.Dpi();
            x *= dpi.DpiScaleX;
            w *= dpi.DpiScaleX;
            y *= dpi.DpiScaleY;
            h *= dpi.DpiScaleY;

            _bounds.X = (int)x;
            _bounds.Y = (int)y;
            _bounds.Width = (int)w;
            _bounds.Height = (int)h;
        }


        public void UpdateSizeFromWindow()
        {
            var w = _window.ActualWidth;
            var h = _window.ActualHeight;
            var dpi = _window.Dpi();
            w *= dpi.DpiScaleX;
            h *= dpi.DpiScaleY;
            _bounds.Width = (int)w;
            _bounds.Height = (int)h;
        }


        public override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            using(var r = GetMovingRegion())
            {
                _isMoving = r.IsVisible(e.Location);
                _oldLocation =Point.Round( Bounds.Location);
                if (_isMoving)
                {
                    _wasEditing = IsEditing;
                    IsEditing = false;
                    //_textBox.CaptureMouse();
                }
            }
        }

        public override void OnMouseMove(MouseEventArgs e)
        {
            if (_isMoving)
            {
                _bounds.Location = new Point(_oldLocation.X + (e.Location.X - _mouseDownPoint.X)
                    , _oldLocation.Y + (e.Location.Y - _mouseDownPoint.Y));
                UpdateWindowLocation();
            }
        }


        public override void OnMouseUp(MouseEventArgs e)
        {
            
            base.OnMouseUp(e);
            if (e.Button == MouseButtons.Left)
            {
                if (_isMoving)
                {
                    _isMoving = false;
                    UpdateImage();
                    IsEditing = _wasEditing;
                }
                else
                {
                    if (IsEditing)
                    {
                        return;
                    }
                    _window = GetNewWindow();
                    IsEditing = true;
                    _bounds = new Rectangle(e.Location.X, e.Location.Y, (int)_window.ActualWidth, (int)_window.ActualHeight);
                    UpdateWindowLocation();
                    _window.SizeChanged += Window_SizeChanged;
                    _window.TextBox.Focus();
                }
            }
        }

        private void UpdateWindowLocation()
        {
            if (_window == null)
            {
                return;
            }

            double left = _bounds.Left;
            double top = _bounds.Top;
            var dpi = _window.Dpi();
            left /= dpi.DpiScaleX;
            top /= dpi.DpiScaleY;

            _window.Left = left;
            _window.Top = top;
        }

        public bool IsMoving
        {
            get
            {
                return _isMoving;
            }
        }

        private void Window_SizeChanged(object sender, System.Windows.SizeChangedEventArgs e)
        {
            var oldBounds = GetInvalidationBounds();
            UpdateSizeFromWindow();
            var frm = Controllers.CaptureController.Current.CaptureView;
            frm.Invalidate(oldBounds);
            frm.Invalidate(GetInvalidationBounds());
        }

        public override void Commit()
        {
            IsEditing = false;
            if (_window == null)
            {
                return;
            }

            UpdateBoundsFromWindow();
            _text = _window.TextBox.Text;

            _window.SizeChanged -= Window_SizeChanged;
            _window.Close();
            _window = null;
        }


        public override Rectangle GetInvalidationBounds()
        {
            var b= base.GetInvalidationBounds();
            b.Inflate(2, 2);
            return b;
        }
        public override void OnPaint(Graphics g)
        {
            //if (IsEditing)
            //{
            //    return;
            //}

            //if (_image == null)
            //{
            //    return;
            //}

            var state = g.Save();
            if (!IsEditing && _image != null)
            {
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.DrawImage(_image, Bounds);
            }
            g.Restore(state);
            if (_isMoving || IsEditing)
            {

                using (Pen p = new Pen(Color.Black))
                {
                    p.DashStyle = System.Drawing.Drawing2D.DashStyle.Custom;
                    p.DashPattern = new float[] { 4, 4 };
                    p.Alignment = System.Drawing.Drawing2D.PenAlignment.Inset;
                    state = g.Save();
                    g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighSpeed;
                    g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighSpeed;
                    g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighSpeed;
                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.Low;
                    Rectangle b = Bounds;
                    b.Inflate(1, 1);
                    g.DrawRectangle(Pens.White,b);
                    g.DrawRectangle(p, b);
                    g.Restore(state);
                }
        }

    }

        public Region GetMovingRegion()
        {
            var rect = Bounds;
            rect.Inflate(5, 5);
            Region r = new Region();
            r.MakeEmpty();
            r.Union(rect);
            rect.Inflate(-4, -4);
            r.Exclude(rect);
            return r;
        }
    }
}
