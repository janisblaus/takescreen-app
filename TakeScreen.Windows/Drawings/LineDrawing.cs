﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TakeScreen.Windows.Drawings
{
    public class LineDrawing : Drawing
    {
        internal Point? _endPoint;

        public override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            _endPoint = e.Location;
        }

        public override void OnPaint(Graphics g)
        {
            if(_endPoint == null)
            {
                return;
            }
            using (Pen p = GetBorderPen())
            {
                 var state = g.Save();
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.DrawLine(p, _mouseDownPoint, _endPoint.Value);
                g.Restore(state);
            }
        }
    }
}
