﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using TakeScreen.Windows.Drawings.WPF;
using TakeScreen.Windows.Extensions;

namespace TakeScreen.Windows.Drawings
{
    public partial class TextDrawing
    {
        private DrawingWindow _window;


        public DrawingWindow Window
        {
            get
            {
               return _window;
            }
        }

        private DrawingWindow GetNewWindow()
        {
            DrawingWindow w = new DrawingWindow();
           
            w.TextBox.Foreground = new System.Windows.Media.SolidColorBrush(ForeColor.ToMediaColor());
            return w;
        }

        public void UpdateImage()
        {
            var dpi = _window.Dpi();
            var dpiX = dpi.PixelsPerInchX;
            var dpiY = dpi.PixelsPerInchY;
            //var dpiX = 96;
            //var dpiY = 96;
            System.Windows.Media.Imaging.RenderTargetBitmap image = new System.Windows.Media.Imaging.RenderTargetBitmap(
    _bounds.Width,
    _bounds.Height,
    dpiX,
    dpiY,
    System.Windows.Media.PixelFormats.Pbgra32);

            var canvas = new System.Windows.Controls.Canvas();
            var textBox = new System.Windows.Controls.TextBox();
            var txtBox = _window.TextBox;
            textBox.Text = txtBox.Text;
            textBox.Foreground = txtBox.Foreground;
            textBox.BorderThickness = txtBox.BorderThickness;
            textBox.Background = txtBox.Background;
            textBox.AcceptsReturn = txtBox.AcceptsReturn;
            textBox.MinHeight = txtBox.MinHeight;
            textBox.MinWidth = txtBox.MinWidth;
            textBox.Padding = txtBox.Padding;
            textBox.SnapsToDevicePixels = txtBox.SnapsToDevicePixels;
            System.Windows.Media.RenderOptions.SetEdgeMode(textBox
                , System.Windows.Media.RenderOptions.GetEdgeMode(txtBox));
            var cropRect = new System.Windows.Int32Rect(_bounds.X, _bounds.Y, _bounds.Width, _bounds.Height);
            canvas.Children.Add(textBox);
            canvas.Measure(textBox.RenderSize);
            canvas.Arrange(new Rect(textBox.RenderSize));
            image.Render(canvas);

            var oldImage = _image;
            _image = image.PngImage();
            //#if DEBUG
            //            _image.Save(System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "test.png"));
            //#endif
            if (oldImage != null)
            {
                oldImage.Dispose();
            }
        }
    }

   

}
