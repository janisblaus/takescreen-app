﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace TakeScreen.Windows.Drawings.WPF
{
    public class DrawingTextBox : TextBox
    {
        public DrawingTextBox() : base()
        {
            MinHeight = 50;
            MinWidth = 20;
            Width = MinWidth;
            Height = MinHeight;
            Background = Brushes.Transparent;
            Padding = new Thickness(0);
            BorderThickness = new Thickness(0);
            AcceptsReturn = true;
        }

        protected override Size ArrangeOverride(Size arrangeBounds)
        {
            var size = base.ArrangeOverride(arrangeBounds);
            size.Width = (int)size.Width;
            size.Height = (int)size.Height;

            return size;
        }

        protected override Size MeasureOverride(Size constraint)
        {
            var size = base.MeasureOverride(constraint);
            size.Width = (int)size.Width;
            size.Height = (int)size.Height;

            return size;

        }
    }
}
