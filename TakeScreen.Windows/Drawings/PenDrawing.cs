﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TakeScreen.Windows.Drawings
{
    public class PenDrawing:Drawing
    {
        private List<Point> _pathPoints;

        public PenDrawing() : base()
        {
            _pathPoints = new List<Point>();
        }

        internal override Pen GetBorderPen()
        {
            var pen =  base.GetBorderPen();
            pen.DashCap = DashCap.Round;
            pen.StartCap = LineCap.Round;
            pen.EndCap = LineCap.Round;
            pen.LineJoin = LineJoin.Round;
            return pen;
        }

        public override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            _pathPoints.Add(_mouseDownPoint);
        }
        public override void OnMouseMove(MouseEventArgs e)
        {
            if (e.Button.HasFlag(MouseButtons.Left))
            {
                var boundsCreated = _pathPoints.Count > 1;
                int left;
                int top;
                int right;
                int bottom;
                if (boundsCreated)
                {
                    left = Math.Min(_bounds.Left, e.Location.X);
                    top = Math.Min(_bounds.Top, e.Location.Y);
                    right = Math.Max(_bounds.Right, e.Location.X);
                    bottom = Math.Max(_bounds.Bottom, e.Location.Y);
                }
                else
                {
                     left = Math.Min(_mouseDownPoint.X, e.Location.X);
                     top = Math.Min(_mouseDownPoint.Y,  e.Location.Y);
                     right = Math.Max(_mouseDownPoint.X,  e.Location.X);
                     bottom = Math.Max(_mouseDownPoint.Y, e.Location.Y);
                }

                _bounds = Rectangle.FromLTRB(left, top, right, bottom);
                _pathPoints.Add(e.Location);
            }
        }

        public override void OnPaint(Graphics g)
        {
            if (_pathPoints.Count <= 0)
            {
                return;
            }
            using (Pen p = GetBorderPen())
            {
                var state = g.Save();
                g.SmoothingMode = SmoothingMode.HighQuality;
                g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;

                switch (_pathPoints.Count)
                {
                    case 0:
                        break;
                    case 1:
                        var point = _pathPoints.First();
                        var radius = (float)(BorderSize / 2);
                        g.FillEllipse(p.Brush, RectangleF.FromLTRB(point.X - radius
                            , point.Y - radius
                            , point.X + radius
                            , point.Y + radius));
                        break;
                    default:
                        g.DrawCurve(p, _pathPoints.ToArray());
                        break;
                }
                g.Restore(state);
            }
        }

        private float GetDistance(PointF p1,PointF p2)
        {
            return (float)Math.Sqrt(Math.Pow(p2.X - p1.X, 2) + Math.Pow(p2.Y - p1.Y, 2));
        }
    }
}
