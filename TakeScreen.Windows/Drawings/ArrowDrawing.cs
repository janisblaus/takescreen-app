﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TakeScreen.Windows.Drawings
{
    public class ArrowDrawing : LineDrawing
    {

        public override void OnPaint(Graphics g)
        {
            if(_endPoint == null)
            {
                return;
            }
            using (Pen p = GetBorderPen())
            {
                var state = g.Save();
                g.SmoothingMode = SmoothingMode.HighQuality;
                g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                DrawArrow(g, p, _mouseDownPoint, _endPoint.Value);
                g.Restore(state);
            }
         
        }

        private void DrawArrow(Graphics g, Pen pen, Point p1, Point p2)
        {
            float theta = (float)(Math.Atan2((p2.Y - p1.Y), (p2.X - p1.X)) * 180 / Math.PI);

            var segmentLength = Math.Sqrt(Math.Pow(p2.X - p1.X, 2) + Math.Pow(p2.Y - p1.Y, 2));
            var triangleHeight = pen.Width * 5;
            var triangleBaseLength = pen.Width * 3;
            var lineLength = segmentLength - triangleHeight;


            using (Matrix m = new Matrix())
            {
                m.Translate(p1.X, p1.Y);
                m.Rotate(theta - 90);
                var state = g.Save();
                g.Transform = m;
                g.SetClip(RectangleF.FromLTRB(-triangleBaseLength / 2, (float)Math.Max(0, segmentLength), triangleBaseLength / 2, 0));
                g.DrawLine(pen, PointF.Empty, new PointF(0f, (float)lineLength + 1));

                using (GraphicsPath path = new GraphicsPath())
                {
                    path.AddLine(new PointF(-triangleBaseLength / 2, (float)lineLength), new PointF(0, (float)segmentLength));
                    path.AddLine(new PointF(0, (float)segmentLength), new PointF(triangleBaseLength / 2, (float)lineLength));
                    path.CloseFigure();
                    g.FillPath(pen.Brush, path);
                }

                g.Restore(state);
            }
        }

        public override Rectangle GetInvalidationBounds()
        {
            var b= base.GetInvalidationBounds();
            b.Inflate(BorderSize * 4, BorderSize * 4);
            return b;
        }
    }
}
