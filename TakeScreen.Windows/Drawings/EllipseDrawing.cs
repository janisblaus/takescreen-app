﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TakeScreen.Windows.Drawings
{
    public class EllipseDrawing : Drawing
    {

        public override void OnPaint(Graphics g)
        {
            using (Pen p = GetBorderPen())
            {
                 var state = g.Save();
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.DrawEllipse(p, Bounds);
                g.Restore(state);
           
            }
        }

        public override Rectangle GetInvalidationBounds()
        {
            var bounds =  base.GetInvalidationBounds();
            bounds.Inflate(BorderSize * 2, BorderSize * 2);
            bounds.Inflate(1, 1);
            return bounds;
        }


    }
}
