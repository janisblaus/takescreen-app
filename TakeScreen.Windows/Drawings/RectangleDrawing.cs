﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TakeScreen.Windows.Drawings
{
    public class RectangleDrawing:Drawing
    {


 
        public override void OnPaint(Graphics g)
        {
            using (Pen p = GetBorderPen())
            {
                var state = g.Save();
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighSpeed;
                g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighSpeed;
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighSpeed;
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.Low;
                g.DrawRectangle(p, Bounds.X,Bounds.Y,Bounds.Width,Bounds.Height);
                g.Restore(state);
            }
        }
    }
}
