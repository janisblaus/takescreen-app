﻿
using Resolve.HotKeys;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Net.Http;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using TakeScreen.Windows.Controllers;
using TakeScreen.Windows.Extensions;
using TakeScreen.Windows.Views;

namespace TakeScreen.Windows
{
    static class Program
    {

        private static HotKey hotKey;


        private static readonly Semaphore singleInstanceWatcher;
        private static readonly bool createdNew;

  private static ApplicationView _mainView;
        private static AppMessageFilter _messageFilter;
        private static Thread _mainThread;
        private static SynchronizationContext _mainContext;

        static Program()
        {
            // Ensure other instances of this application are not running.
            singleInstanceWatcher = new Semaphore(
                0, // Initial count.
                1, // Maximum count.
                Assembly.GetExecutingAssembly().GetName().Name,
                out createdNew);

            if (createdNew)
            {
                AppDomain currentDomain = AppDomain.CurrentDomain;
                if (currentDomain != null)
                {
                    currentDomain.UnhandledException += new UnhandledExceptionEventHandler(AppDomain_UnhandledException);
                }
            }
            else
            {
                // This thread opened an existing kernel object with the same
                // string name; another instance of this app must be running now.

                // Gets a new System.Diagnostics.Process component and the
                // associates it with currently active process.
                Process current = Process.GetCurrentProcess();

                // Enumerate through all the process resources on the share
                // local computer that the specified process name.
                foreach (Process process in
                     Process.GetProcessesByName(current.ProcessName))
                {
                    if (process.Id != current.Id)
                    {

                        break;
                    }
                }

                // Terminate this process and gives the underlying operating 
                // system the specified exit code.
                Environment.Exit(-2);
            }

        }


        public static Thread MainThread
        {
            get
            {
                return _mainThread;
            }
        }

        public static SynchronizationContext MainContext
        {
            get
            {
                return _mainContext;
            }
        }

       public static bool InvokeRequired
        {
            get
            {
                return MainThread.ManagedThreadId != Thread.CurrentThread.ManagedThreadId;
            }
        }

        public static TResult Invoke<TResult>(Func<TResult> method)
        {
            TResult value = default(TResult);
            ManualResetEvent e = new ManualResetEvent(false);
            _mainContext.Post(new SendOrPostCallback((o) =>
            {
                //e.Reset();

                value = method();
                e.Set();
            }),null);
        
                e.WaitOne();
            
            return value;
        }

        public static void BeginInvoke(Delegate method)
        {
            _mainContext.Post(new SendOrPostCallback((o) =>
            {
                method.DynamicInvoke(null);
            }), null);
        }
   

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(true);
            MainView.Show();

            _mainThread = Thread.CurrentThread;
            _mainContext = SynchronizationContext.Current;

            var coreAppController = ApplicationController.Current;
            coreAppController.UpdateCurrentCulture();


            _messageFilter = new AppMessageFilter();
            Application.AddMessageFilter(_messageFilter);



            //UploadView w = new UploadView();
            //w.Left = SystemInformation.WorkingArea.Right - w.Width;
            //w.Top = SystemInformation.WorkingArea.Bottom - w.Height;
            //w.Show();
            //w.Upload();
            //Application.Run();
            //return;

            try
            {
                hotKey = new HotKey(Keys.PrintScreen, ModifierKey.None);
                hotKey.Pressed += HotKey_Pressed;
                hotKey.Register();

            }
            catch (Exception ex)
            {
#if DEBUG
                Console.WriteLine(ex);
                MainView.DisplayNotification(TakeScreen.Core.Properties.Resources.RegisterFailCaption, ex.Message, System.Windows.Forms.ToolTipIcon.Error);
#else
                MainView.DisplayNotification(TakeScreen.Core.Properties.Resources.RegisterFailCaption, ex.Message, System.Windows.Forms.ToolTipIcon.Error);
#endif
            }



#if DEBUG
            //TODO: make it download from a local source
            AutoUpdateController.Current.UseSimulation = true;
            AutoUpdateController.Current.StartMonitoring();
#else
            AutoUpdateController.Current.StartMonitoring();
#endif





            var wUser = TakeScreen.Core.Properties.Settings.Default.CurrentWebUser;
            if (wUser != null)
            {
                Task.Run(async () =>
                {
                    await ApiController.Current.LoginAsync();
                });
            }

            Application.Run();
            if (hotKey != null)
            {
                hotKey.Pressed -= HotKey_Pressed;
                hotKey.Dispose();
            }
        }

        private static void HotKey_Pressed(object sender, EventArgs e)
        {
            CaptureController.Current.Start();
        }

        public static ApplicationView MainView
        {
            get
            {
                if (_mainView == null)
                {
                    _mainView = new ApplicationView();
                }
                return _mainView;
            }
        }



        /// <summary>
        /// The function gets the elevation information of the current process. It 
        /// dictates whether the process is elevated or not. Token elevation is only 
        /// available on Windows Vista and newer operating systems, thus 
        /// IsProcessElevated throws a C++ exception if it is called on systems prior 
        /// to Windows Vista. It is not appropriate to use this function to determine 
        /// whether a process is run as administartor.
        /// </summary>
        /// <returns>
        /// Returns true if the process is elevated. Returns false if it is not.
        /// </returns>
        /// <exception cref="System.ComponentModel.Win32Exception">
        /// When any native Windows API call fails, the function throws a Win32Exception 
        /// with the last error code.
        /// </exception>
        /// <remarks>
        /// TOKEN_INFORMATION_CLASS provides TokenElevationType to check the elevation 
        /// type (TokenElevationTypeDefault / TokenElevationTypeLimited / 
        /// TokenElevationTypeFull) of the process. It is different from TokenElevation 
        /// in that, when UAC is turned off, elevation type always returns 
        /// TokenElevationTypeDefault even though the process is elevated (Integrity 
        /// Level == High). In other words, it is not safe to say if the process is 
        /// elevated based on elevation type. Instead, we should use TokenElevation. 
        /// </remarks>
        internal static bool IsProcessElevated()
        {
            bool fIsElevated = false;
            SafeTokenHandle hToken = null;
            int cbTokenElevation = 0;
            IntPtr pTokenElevation = IntPtr.Zero;

            try
            {
                // Open the access token of the current process with TOKEN_QUERY.
                if (!NativeMethods.OpenProcessToken(Process.GetCurrentProcess().Handle,
                    NativeMethods.TOKEN_QUERY, out hToken))
                {
                    throw new Win32Exception();
                }

                // Allocate a buffer for the elevation information.
                cbTokenElevation = Marshal.SizeOf(typeof(TOKEN_ELEVATION));
                pTokenElevation = Marshal.AllocHGlobal(cbTokenElevation);
                if (pTokenElevation == IntPtr.Zero)
                {
                    throw new Win32Exception();
                }

                // Retrieve token elevation information.
                if (!NativeMethods.GetTokenInformation(hToken,
                    TOKEN_INFORMATION_CLASS.TokenElevation, pTokenElevation,
                    cbTokenElevation, out cbTokenElevation))
                {
                    // When the process is run on operating systems prior to Windows 
                    // Vista, GetTokenInformation returns false with the error code 
                    // ERROR_INVALID_PARAMETER because TokenElevation is not supported 
                    // on those operating systems.
                    throw new Win32Exception();
                }

                // Marshal the TOKEN_ELEVATION struct from native to .NET object.
                TOKEN_ELEVATION elevation = (TOKEN_ELEVATION)Marshal.PtrToStructure(
                    pTokenElevation, typeof(TOKEN_ELEVATION));

                // TOKEN_ELEVATION.TokenIsElevated is a non-zero value if the token 
                // has elevated privileges; otherwise, a zero value.
                fIsElevated = (elevation.TokenIsElevated != 0);
            }
            finally
            {
                // Centralized cleanup for all allocated resources. 
                if (hToken != null)
                {
                    hToken.Close();
                    hToken = null;
                }
                if (pTokenElevation != IntPtr.Zero)
                {
                    Marshal.FreeHGlobal(pTokenElevation);
                    pTokenElevation = IntPtr.Zero;
                    cbTokenElevation = 0;
                }
            }

            return fIsElevated;
        }

        /// <summary>
        /// The function checks whether the current process is run as administrator.
        /// In other words, it dictates whether the primary access token of the 
        /// process belongs to user account that is a member of the local 
        /// Administrators group and it is elevated.
        /// </summary>
        /// <returns>
        /// Returns true if the primary access token of the process belongs to user 
        /// account that is a member of the local Administrators group and it is 
        /// elevated. Returns false if the token does not.
        /// </returns>
        public static bool IsRunAsAdmin()
        {
            WindowsIdentity id = WindowsIdentity.GetCurrent();
            WindowsPrincipal principal = new WindowsPrincipal(id);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }

        private static void AppDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
#if DEBUG
            Console.WriteLine(e.ExceptionObject);
#else
            ApplicationController.Current.CaptureException(e.ExceptionObject as Exception);
#endif
        }
    }
}
