﻿using System.Management;

namespace TakeScreen.Windows.Controllers
{
    public class ApiController : TakeScreen.Core.Controllers.ApiController
    {
        private static ApiController _current;

        public static ApiController Current
        {
            get
            {
                if (_current == null)
                {
                    _current = new ApiController();
                }

                return _current;
            }
        }

        public override string GetProcessorID()
        {
            string cpuInfo = string.Empty;
            ManagementClass mc = new ManagementClass("win32_processor");
            ManagementObjectCollection moc = mc.GetInstances();

            foreach (ManagementObject mo in moc)
            {
                if (cpuInfo == "")
                {
                    //Get only the first CPU's ID
                    cpuInfo = mo.Properties["processorID"].Value.ToString();
                    break;
                }
            }
            return cpuInfo;
        }

        public override string GetVolumeSerial(string strDriveLetter)
        {
            if (strDriveLetter == "" || strDriveLetter == null) strDriveLetter = "C";
            ManagementObject disk =
                new ManagementObject("win32_logicaldisk.deviceid=\"" + strDriveLetter + ":\"");
            disk.Get();
            return disk["VolumeSerialNumber"].ToString();
        }
    }
}