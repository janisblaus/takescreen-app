﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using TakeScreen.Windows.Extensions;
using TakeScreen.Windows.Models;
using TakeScreen.Windows.Views;
using TakeScreen.Windows.Views.Upload;

namespace TakeScreen.Windows.Controllers
{
    public class CaptureController : Core.Controllers.CaptureController
    {
        private WeakReference<SaveFileDialog> _saveFileDialogRef;
        private WeakReference<SaveFileDialog> _videoSaveFileDialogRef;
        private VideoCaptureView _recordingView;
        private CaptureView _captureView;
        //private ToolboxWindow _toolboxWindow;

        Process _recordingProcess;
        StringBuilder _outputRecordingData;
        StringBuilder _errorRecordingData;


        public CaptureController() : base()
        {
            Model = new Capture();
        }

        public new Capture Model
        {
            get
            {
                return base.Model as Capture;
            }
            set
            {
                UnwireModelEvents();
                base.Model = value;
                WireModelEvents();
            }
        }

        private void WireModelEvents()
        {
            if(Model != null)
            {
                Model.ModeChanged += Model_ModeChanged;
                Model.ToolTypeChanged += Model_ToolTypeChanged;
            }
        }

        private void UnwireModelEvents()
        {
            if (Model != null)
            {
                Model.ModeChanged -= Model_ModeChanged;
                Model.ToolTypeChanged -= Model_ToolTypeChanged;
            }
        }

        private void Model_ToolTypeChanged(object sender, EventArgs e)
        {
            UpdateWiews();
        }

        private void UpdateWiews()
        {
            if(Model == null)
            {
                return;
            }

            

        }

        private void Model_ModeChanged(object sender, EventArgs e)
        {
            ////UpdateWiews();
            ////ToolboxWindow.Hide();
            //CaptureView.Hide();


            //var selRectangle = Model.Selection.Rectangle;
            //RecordingView.Width = selRectangle.Width;
            //RecordingView.Height = selRectangle.Height;
            //RecordingView.Left = selRectangle.Left;
            //RecordingView.Top = selRectangle.Top;
            ////CaptureWindow.Model = Model;
            ////CaptureWindow.Owner = MainWindow;
            //RecordingView.Show();


        }

        public SaveFileDialog SaveFileDialog
        {
            get
            {
                SaveFileDialog sfd = null;

                var setProperties = new Action(() =>
                {
                    //TODO: find a way to use the settings
                    sfd.Filter = "Png Image|*.png|JPEG Image|*.jpeg";
                    sfd.OverwritePrompt = true;
                    sfd.RestoreDirectory = false;
                });

                if (_saveFileDialogRef == null)
                {
                    sfd = new SaveFileDialog();
                    setProperties();
                    _saveFileDialogRef = new WeakReference<SaveFileDialog>(sfd);
                }
                else
                {
                    if (!_saveFileDialogRef.TryGetTarget(out sfd))
                    {
                        sfd = new SaveFileDialog();
                        setProperties();

                        _saveFileDialogRef.SetTarget(sfd);
                    }
                }
                sfd.FilterIndex = TakeScreen.Core.Properties.Settings.Default.CurrentImageFormat == "png" ? 1 : 2;
                return sfd;
            }
        }

        public SaveFileDialog VideoSaveFileDialog
        {
            get
            {
                SaveFileDialog sfd = null;

                var setProperties = new Action(() =>
                {
                    sfd.Filter = "MP4 Video|*.mp4";
                    sfd.OverwritePrompt = true;
                    sfd.RestoreDirectory = false;
                });

                if (_videoSaveFileDialogRef == null)
                {
                    sfd = new SaveFileDialog();
                    setProperties();
                    _videoSaveFileDialogRef = new WeakReference<SaveFileDialog>(sfd);
                }
                else
                {
                    if (!_videoSaveFileDialogRef.TryGetTarget(out sfd))
                    {
                        sfd = new SaveFileDialog();
                        setProperties();

                        _videoSaveFileDialogRef.SetTarget(sfd);
                    }
                }
                sfd.FilterIndex = 1;
                return sfd;
            }
        }

        private static CaptureController _current;
 
        public static CaptureController Current
        {
            get
            {
                if (_current == null)
                {
                    _current = new CaptureController();
                }

                return _current;
            }
        }

        public CaptureView CaptureView
        {
            get
            {
                if (_captureView == null)
                {
                    _captureView = new CaptureView();
                }
                return _captureView;
            }
        }

        public VideoCaptureView RecordingView
        {
            get
            {
                if (_recordingView == null)
                {
                    _recordingView = new VideoCaptureView();
                }
                return _recordingView;
            }
        }

        //public ToolboxWindow ToolboxWindow
        //{
        //    get
        //    {
        //        if (_toolboxWindow == null)
        //        {
        //            _toolboxWindow = new ToolboxWindow();
        //        }
        //        return _toolboxWindow;
        //    }
        //}

        //public MainWindow MainWindow
        //{
        //    get
        //    {
        //        if (_mainWindow == null)
        //        {
        //            _mainWindow = new MainWindow();
        //        }
        //        return _mainWindow;
        //    }
        //}

        public override void Start()
        {
            if (IsStarted)
            {
                return;
            }

            AutoUpdateController.Current.Locker.Reset();
            var screenshot = TakeScreenshot();
            Model = new Capture();
            Model.Screenshot = screenshot;


            var sf = CaptureView;
            sf.Model = Model;

            var vScreen = SystemInformation.VirtualScreen;
           sf. Bounds = vScreen;
            sf.Show();
            sf.Activate();

            base.Start();

        }

        public override void Close()
        {
            base.Close();

          if(_captureView != null)
            {
                _captureView.Close();
                _captureView.Dispose();
                _captureView = null;
            }
            if (_recordingView != null)
            {
                _recordingView.Close();
                _recordingView.Dispose();
                _recordingView = null;
            }
 
            Model = null;
         }

        public override void Hide()
        {
            base.Hide();
            if (_captureView != null)
            {
                _captureView.Hide();
            }
          if(_recordingView != null)
            {
                _recordingView.Hide();
            }
        }

        public Bitmap TakeScreenshot()
        {
            var vScreen = SystemInformation.VirtualScreen;
            var bmp = new Bitmap(vScreen.Width
                , vScreen.Height
                , System.Drawing.Imaging.PixelFormat.Format32bppRgb);
       
            //var dpi = MainWindow.Dpi();
            //    bmp.SetResolution((float)dpi.PixelsPerInchX, (float)dpi.PixelsPerInchY);

            //StringBuilder sb = new StringBuilder();
            //sb.AppendLine("Take Screenshot:");
            //sb.AppendFormat("DPI: {0}x{1}", dpi.PixelsPerInchX, dpi.PixelsPerInchY);
            //sb.AppendLine();
            //sb.AppendFormat("DPI Scale: {0}x{1}", dpi.DpiScaleX, dpi.DpiScaleY);
            //sb.AppendLine();
            //sb.AppendFormat("Virtual Screen: {0}x{1}", SystemParameters.VirtualScreenWidth, SystemParameters.VirtualScreenHeight);


            using (var g = System.Drawing.Graphics.FromImage(bmp))
            {
               
                g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;


                //System.Drawing.Point upperLeftDestination = new System.Drawing.Point();
                //int vScreenLeft = vScreen.Left;
                //int vScreenTop = vScreen.Top;
                //foreach (Screen screen in Screen.AllScreens)
                //{

                //    //sb.AppendLine();
                //    //sb.AppendFormat("Screen Bounds: {0}", screen.Bounds);

                //    upperLeftDestination.X = screen.Bounds.X - vScreenLeft;
                //    upperLeftDestination.Y = screen.Bounds.Y - vScreenTop;
                //    g.CopyFromScreen(screen.Bounds.Location
                //       , upperLeftDestination
                //       , screen.Bounds.Size
                //       , CopyPixelOperation.SourceCopy );



                //}


                g.CopyFromScreen(vScreen.Location, Point.Empty, vScreen.Size);
                if (TakeScreen.Core.Properties.Settings.Default.ImageCaptureCursor)
                {
                    var cCursor = System.Windows.Forms.Cursor.Current;
                    var mPosition = System.Windows.Forms.Control.MousePosition;
                    var cSize = cCursor.Size;
                    cCursor.Draw(g, new System.Drawing.Rectangle(mPosition, cSize));
                }

            }
            //bmp.Save(System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), "test.png"), System.Drawing.Imaging.ImageFormat.Png);
            //ApplicationController.Current.CaptureMessage(sb.ToString());
            return bmp;
        }

        //[DllImport("gdi32.dll")]
        //static extern int GetDeviceCaps(IntPtr hdc, int nIndex);
        //public enum DeviceCap
        //{
        //    VERTRES = 10,
        //    DESKTOPVERTRES = 117,

        //    // http://pinvoke.net/default.aspx/gdi32/GetDeviceCaps.html
        //}


        //private float getScalingFactor()
        //{
        //    Graphics g = Graphics.FromHwnd(IntPtr.Zero);
        //    IntPtr desktop = g.GetHdc();
        //    int LogicalScreenHeight = GetDeviceCaps(desktop, (int)DeviceCap.VERTRES);
        //    int PhysicalScreenHeight = GetDeviceCaps(desktop, (int)DeviceCap.DESKTOPVERTRES);

        //    float ScreenScalingFactor = (float)PhysicalScreenHeight / (float)LogicalScreenHeight;

        //    return ScreenScalingFactor; // 1.25 = 125%
        //}

        public void StartRecording()
        {
var model = Model;
            var rView = RecordingView;
            try
            {

                
              
                _outputRecordingData = new StringBuilder();
                _errorRecordingData = new StringBuilder();
                model.Mode = Core.Models.CaptureMode.Video;
                model.AdjustSelection();
                model.Selection.RenderOutsideBounds = true;
                model.Selection.GrabHandlesVisible = false;

                var vScreenBounds = SystemInformation.VirtualScreen;


            

                string fileName = Core.Properties.Settings.Default.VideoFilePath;
                if (System.IO.File.Exists(fileName))
                {
                    System.IO.File.Delete(fileName);
                }
                Rectangle vRect = Model.Selection.Rectangle;


                var pScreenBounds = Screen.PrimaryScreen.Bounds;
                vRect.X -= (pScreenBounds.X - vScreenBounds.X);
                vRect.Y -= (pScreenBounds.Y - vScreenBounds.Y);
                string dirPath = System.IO.Directory.GetParent(System.Reflection.Assembly.GetExecutingAssembly().Location).FullName;
                string path = System.IO.Path.Combine(dirPath, "ffmpeg.exe");

                StringBuilder args = new StringBuilder();
                //global options
                args.Append("-y");//Overwrite output files without asking. 
                                  //input
                args.Append(" ");
                args.Append("-rtbufsize 100M"); //max memory used for buffering real-time frames..
                //args.Append(" ");
                //args.Append("-hwaccel auto"); //Use hardware acceleration to decode the matching stream(s).
                args.Append(" ");
                args.Append("-f");//Force input or output file format
                args.Append(" ");
                args.Append("gdigrab");//capture the display
                //gdigrab options
                args.Append(" ");
                args.AppendFormat("-offset_x {0}", vRect.X);//left
                args.Append(" ");
                args.AppendFormat("-offset_y {0}", vRect.Y);//top
                args.Append(" ");
                args.AppendFormat("-video_size {0}x{1}", vRect.Width, vRect.Height);//size
                args.Append(" ");
                args.Append("-draw_mouse 1");
                args.Append(" ");
                args.Append("-framerate 30");//grabbing frame rate.
                args.Append(" ");
                args.Append("-i");//input 
                args.Append(" ");
                args.Append("desktop");//capture from the entire desktop 
                                       //output
                args.Append(" ");
                args.Append("-pix_fmt yuv420p");//pixel format
                args.Append(" ");
                args.Append("-c:v libx264");//x264 H.264/MPEG-4 AVC encoder wrapper
                args.Append(" ");
                args.Append("-preset ultrafast");
                args.Append(" ");
                args.Append("-tune zerolatency");
                args.Append(" ");
                args.Append("-crf 28");
                args.Append(" ");
                args.AppendFormat("\"{0}\"", fileName);//output file path

                _recordingProcess = new Process();
                ProcessStartInfo psi = new ProcessStartInfo(path);
                psi.UseShellExecute = false;
                psi.CreateNoWindow = true;
                psi.RedirectStandardInput = true;
                psi.RedirectStandardOutput = true;
                psi.RedirectStandardError = true;
                psi.Arguments = args.ToString();
                psi.WorkingDirectory = dirPath;
                psi.StandardOutputEncoding = Encoding.UTF8;
                psi.StandardErrorEncoding = Encoding.UTF8;

                _recordingProcess.EnableRaisingEvents = true;
                if (psi.RedirectStandardOutput) _recordingProcess.OutputDataReceived += _recordingProcess_OutputDataReceived;
                if (psi.RedirectStandardError) _recordingProcess.ErrorDataReceived += _recordingProcess_ErrorDataReceived;
                _recordingProcess.Exited += _recordingProcess_Exited;

                _recordingProcess.StartInfo = psi;


                CaptureView.Hide();

                _recordingProcess.Start();
                if (psi.RedirectStandardOutput) _recordingProcess.BeginOutputReadLine();
                if (psi.RedirectStandardError) _recordingProcess.BeginErrorReadLine();

                rView.Model = Model;
                rView.MenuBar.Visible = true;
                rView.Bounds = vScreenBounds;
                rView.SelectionRectangle = Model.Selection.Rectangle;
                rView.Show();
                rView.SetLayeredWindow();
                rView.Activate();

            }
            catch (Exception ex)
            {
                ApplicationController.Current.CaptureException(ex);
                Program.MainView.DisplayNotification(Core.Properties.Resources.RecordingFailedMessage, ex.Message, ToolTipIcon.Error);
                //restore windows
                model.Mode = Core.Models.CaptureMode.Image;
                model.AdjustSelection();
                model.Selection.RenderOutsideBounds = false;
                model.Selection.GrabHandlesVisible = true;

                rView.Close();
                _recordingView = null;

                CaptureView.Show();
                CaptureView.Activate();
            }
        }

        private void _recordingProcess_Exited(object sender, EventArgs e)
        {

            //ApplicationController.Current.CaptureMessage(_outputRecordingData.ToString());
            //ApplicationController.Current.CaptureMessage(_errorRecordingData.ToString());
            Program.BeginInvoke(new Action(() =>
                   {
                       try
                       {
                           UploadView w = new UploadView();
                           w.FormClosed += UploadView_FormClosed;
                           w.Left = SystemInformation.WorkingArea.Right - w.Width;
                           w.Top = SystemInformation.WorkingArea.Bottom - w.Height;
                           w.Show();
                           w.Upload();
                           Close();
                       }
                       catch (Exception ex)
                       {
                           ApplicationController.Current.CaptureException(ex);
                           Program.MainView.DisplayNotification("TakeScreen", ex.Message, ToolTipIcon.Error);
                       }
                   }));
        }

        private void UploadView_FormClosed(object sender, FormClosedEventArgs e)
        {
            UploadView w = sender as UploadView;
            if (w != null)
            {
                w.FormClosed -= UploadView_FormClosed;
            }
            AutoUpdateController.Current.Locker.Set();
        }

        public void StopRecording()
        {
            
            try
            {
                _recordingProcess.StandardInput.WriteLine("q");
            }
            catch (Exception ex)
            {
                ApplicationController.Current.CaptureException(ex);
                try
                {
                    _recordingProcess.Kill();
                }
                catch (Exception ex1)
                {
                    ApplicationController.Current.CaptureException(ex1);
                }
            }
        }

        private void _recordingProcess_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            Console.WriteLine(e.Data);
            _errorRecordingData.AppendLine(e.Data);
        }

        private void _recordingProcess_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            Console.WriteLine(e.Data);
            _outputRecordingData.AppendLine(e.Data);
        }


    }
}
