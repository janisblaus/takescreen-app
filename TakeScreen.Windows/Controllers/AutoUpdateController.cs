﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using TakeScreen.Windows.Views;

namespace TakeScreen.Windows.Controllers
{
   public class AutoUpdateController:Core.Controllers.AutoUpdateController
    {
        private static AutoUpdateController _current;
        private DownloadProgressView _downloadProgressView;

        public AutoUpdateController() : base()
        {
            Program.MainView.Model.UpdateMenuItem.Action = new Action(() =>
           {
               Console.WriteLine("Autoupdate waiting ...");
               Locker.WaitOne();
               Console.WriteLine("Autoupdate done waiting.");
               RunUpdateAction();

           });
        }

        public override void RunUpdateAction()
        {
            if (_downloadProgressView != null)
            {
                _downloadProgressView.Show();
                _downloadProgressView.Activate();
                return;
            }


            Task.Run(async () =>
            {
                try
                {
                    Locker.Reset();
                DoUpdate:
                    if (!IsDownloaded())
                    {
                        var response = GetDownloadResponse();
                        switch (response)
                        {
                            case Core.Models.AutoUpdateDownloadResponse.Download:
                                CancellationTokenSource cts = new CancellationTokenSource();
                                Model.DownloadedFile = new Core.Models.WebFile();

                                Action showViewAction = new Action(() =>
                                {
                                    DownloadProgressView.File = Model.DownloadedFile;
                                    DownloadProgressView.CancellationTokenSource = cts;
                                    DownloadProgressView.Show();
                                });

                                if (Program.InvokeRequired)
                                {
                                    Program.BeginInvoke(showViewAction);

                                }
                                else
                                {
                                    showViewAction();
                                }

                                try
                                {
                                    await DownloadAsync(cts.Token);
                                    goto DoUpdate;
                                }
                                catch (OperationCanceledException oce)
                                {
                                    Console.WriteLine(oce);
                                    if (System.IO.File.Exists(Model.InstallerFilePath))
                                    {
                                        System.IO.File.Delete(Model.InstallerFilePath);
                                    }


                                }
                                finally
                                {
                                    Action disposeAction = new Action(() =>
                                    {
                                        _downloadProgressView.Dispose();
                                        _downloadProgressView = null;
                                        Model.DownloadedFile = null;

                                    });

                                    if (Program.InvokeRequired)
                                    {
                                        Program.BeginInvoke(disposeAction);
                                    }
                                    else
                                    {
                                        disposeAction();
                                    }
                                }
                                break;
                            case Core.Models.AutoUpdateDownloadResponse.DecideLater:
                                Schedule();
                                break;
                            case Core.Models.AutoUpdateDownloadResponse.ShowLog:
                                ShowLog();
                                break;
                        }
                    }
                    else
                    {
                        var response = GetUpgradeResponse();
                        switch (response)
                        {
                            case Core.Models.AutoUpdateUpgradeResponse.Upgrade:
                                await StartAsync();
                                break;
                            case Core.Models.AutoUpdateUpgradeResponse.NotNow:
                                Schedule();
                                break;
                        }
                    }
                }
                catch (Exception exx)
                {
                    ApplicationController.Current.CaptureException(exx);
                    Action action = new Action(() =>
                    {
                        MessageBox.Show(exx.Message);
                    });
                    if (Program.InvokeRequired)
                    {
                        Program.BeginInvoke(action);
                    }
                    else
                    {
                        action();
                    }
                }
                finally
                {
                    Locker.Set();
                }
            });
        }

        public static AutoUpdateController Current
        {
            get
            {
                if (_current == null)
                {
                    _current = new AutoUpdateController();
                }
                return _current;
            }
        }

        public DownloadProgressView DownloadProgressView {
            get
            {
                if(_downloadProgressView == null)
                {
                    _downloadProgressView = new DownloadProgressView();
                }
                return _downloadProgressView;
            }
        }

        public override Core.Models.AutoUpdateUpgradeResponse GetUpgradeResponse()
        {
            var response = Core.Models.AutoUpdateUpgradeResponse.None;
            Func<Core.Models.AutoUpdateUpgradeResponse> method = new Func<Core.Models.AutoUpdateUpgradeResponse>(() =>
            {
                using (UpgradeView v = new UpgradeView())
                {
                   
                    var result = v.ShowDialog();
                    return v.Response;
                }
            });
            if (Program.InvokeRequired)
            {
                response = Program.Invoke<Core.Models.AutoUpdateUpgradeResponse>(method);
            }
            else
            {
                response = method();
            }

            return response;
        }


        public override Core.Models.AutoUpdateDownloadResponse GetDownloadResponse()
        {

            var response = Core.Models.AutoUpdateDownloadResponse.None;
            Func<Core.Models.AutoUpdateDownloadResponse> method = new Func<Core.Models.AutoUpdateDownloadResponse>(() =>
            {
                using (DownloadView v = new DownloadView())
                {
                    var result = v.ShowDialog();
                    return v.Response;
                }
            });
            if (Program.InvokeRequired)
            {
                response = Program.Invoke<Core.Models.AutoUpdateDownloadResponse>(method);
            }
            else
            {
                response = method();
            }

            return response;
        }



        public override void AddUpdateNowUI()
        {
            Delegate method = new Action(() =>
            {
                var mainModel = Program.MainView.Model;
                var updateMenuItem = mainModel.UpdateMenuItem;
                if (!mainModel.NotifyIcon.Menu.Items.Contains(updateMenuItem))
                {
                    mainModel.NotifyIcon.Menu.Items.Insert(2, updateMenuItem);
                }
            });

            if (Program.InvokeRequired)
            {
                Program.BeginInvoke(method);
            }
            else
            {
                method.DynamicInvoke(null);
            }

        }

        public override void DisplayMinorUpdateNotification()
        {
            Action action = new Action(() =>
            {
                var mv = Program.MainView;
                mv.NotifyIcon.BalloonTipClicked += NotifyIcon_BalloonTipClicked;
                mv.NotifyIcon.BalloonTipClosed += NotifyIcon_BalloonTipClosed;
                mv.DisplayNotification("TakeScreen", Core.Properties.Resources.MinorUpdateNotificationText, ToolTipIcon.Info);
            });
            if (Program.InvokeRequired)
            {
                Program.BeginInvoke(action);
            }
            else
            {
                action();
            }
        }

        private void NotifyIcon_BalloonTipClosed(object sender, EventArgs e)
        {
            var ni = sender as NotifyIcon;
            if (object.Equals(ni.BalloonTipText, Core.Properties.Resources.MinorUpdateNotificationText))
            {
                ni.BalloonTipClicked -= NotifyIcon_BalloonTipClicked;
                ni.BalloonTipClosed -= NotifyIcon_BalloonTipClosed;
            }
        }

        private void NotifyIcon_BalloonTipClicked(object sender, EventArgs e)
        {
            Program.MainView.Model.UpdateMenuItem.Action?.Invoke();
        }


        public override async Task StartAsync()
        {
            await Task.Run(() =>
             {
                 ProcessStartInfo sInfo = new ProcessStartInfo("msiexec", string.Format(@" /i {0} /qn /norestart /log c:\TakeScreenInstall.log", Model.InstallerFilePath));
                 sInfo.UseShellExecute = true;
                 if (!Program.IsRunAsAdmin())
                 {
                     sInfo.Verb = "runas";
                 }


                 UpgradeProgressView v = null;
                 if (Program.InvokeRequired)
                 {
                     Program.BeginInvoke(new Action(() =>
                     {
                         v = new UpgradeProgressView();
                         v.Show();
                     }));
                 }
                 else
                 {
                     v = new UpgradeProgressView();
                     v.Show();
                 }
                 try
                 {
                     using (var p = Process.Start(sInfo))
                     {

                         p.WaitForExit();
                         //TODO: check if it really installed
                         Console.WriteLine("Installer exit: {0}", p.ExitCode);
                         Environment.Exit(-10);
                     }
                 }
                 catch(Win32Exception ex)
                 {
                     switch (ex.ErrorCode)
                     {
                         default:
                             throw ex;
                         case NativeMethods.ERROR_CANCELLED:
                             ApplicationController.Current.CaptureMessage(ex.Message);
                             break;
                     }
                    
                 }
                 finally
                 {
                     Action closeViewAction = new Action(() =>
                       {
                           if (v != null)
                           {
                               v.Close();
                               v.Dispose();
                               v = null;
                           }
                       });
                     if (Program.InvokeRequired)
                     {
                         Program.BeginInvoke(closeViewAction);
                     }
                     else
                     {
                         closeViewAction();
                     }
                 }
             });
        }
    }
}
