﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TakeScreen.Core.Controllers;

namespace TakeScreen.Windows.Controllers
{
    public class ApplicationController : Core.Controllers.ApplicationController
    {
        private static ApplicationController _current;

        public static  ApplicationController Current
        {
            get
            {
                if (_current == null)
                {
                    _current = new ApplicationController();
                }
               
                return _current ;
            }
        }

    }
}
