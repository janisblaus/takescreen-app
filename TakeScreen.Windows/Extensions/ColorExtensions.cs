﻿using SDColor = System.Drawing.Color;
using SWMColor = System.Windows.Media.Color;

namespace TakeScreen.Windows.Extensions
{
    public static class ColorExtensions
    {

        public static SWMColor ToMediaColor(this SDColor color) => SWMColor.FromArgb(color.A, color.R, color.G, color.B);
        public static SDColor ToDrawingColor(this SWMColor color) => SDColor.FromArgb(color.A, color.R, color.G, color.B);

    }
}
