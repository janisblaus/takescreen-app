﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TakeScreen.Windows.Extensions
{
   public static partial class RectangleExtensions
    {

        public static Point BottomRight(this Rectangle rect)
        {
            return new Point(rect.Right, rect.Bottom);
        }

        public static Point BottomLeft(this Rectangle rect)
        {
            return new Point(rect.Left, rect.Bottom);
        }

        public static Point TopLeft(this Rectangle rect)
        {
            return new Point(rect.Left, rect.Top);
        }

        public static Point TopRight(this Rectangle rect)
        {
            return new Point(rect.Right, rect.Top);
        }

        public static System.Windows.Rect Rect(this Rectangle rectangle)
        {
            return new System.Windows.Rect(rectangle.X, rectangle.Y, rectangle.Width, rectangle.Height);
        }

    }
}
