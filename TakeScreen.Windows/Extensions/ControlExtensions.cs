﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TakeScreen.Windows.Extensions
{
   public static class ControlExtensions
    {
        /// <summary>
        /// Checks if the control can call <see cref="Control.BeginInvoke(Delegate)"/>, <see cref="Control.BeginInvoke(Delegate, object[])"/>, <see cref="Control.Invoke(Delegate)"/> or <see cref="Control.Invoke(Delegate, object[])"/>.
        /// Returns true if <see cref="Control.IsHandleCreated"/> is true and <see cref="Control.IsDisposed"/> is false.
        /// </summary>
        public static bool CanInvoke(this Control control)
        {
            return control.IsHandleCreated && !control.IsDisposed;
        }
    }
}
