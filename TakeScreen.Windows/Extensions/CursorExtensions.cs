﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace TakeScreen.Windows.Extensions
{
 public static  class CursorExtensions
    {

        private static class NativeMethods
        {
            public struct IconInfo
            {
                public bool fIcon;
                public int xHotspot;
                public int yHotspot;
                public IntPtr hbmMask;
                public IntPtr hbmColor;
            }

            [DllImport("user32.dll")]
            public static extern IntPtr CreateIconIndirect(ref IconInfo icon);

            [DllImport("user32.dll")]
            public static extern bool DestroyIcon(IntPtr hIcon);

            [DllImport("user32.dll")]
            [return: MarshalAs(UnmanagedType.Bool)]
            public static extern bool GetIconInfo(IntPtr hIcon, ref IconInfo pIconInfo);
        }


        public static Cursor GetArrowCursorWithElement(UIElement element)
        {
            var baseCursor = System.Windows.Forms.Cursors.Arrow;
            int width = Math.Max(baseCursor.Size.Width, (int)element.DesiredSize.Width);
            int height = baseCursor.Size.Height + (int)element.DesiredSize.Height;

            using (var bmp = new System.Drawing.Bitmap(width, height))
            {
                using (var g = System.Drawing.Graphics.FromImage(bmp))
                {
                    g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                    g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                    baseCursor.Draw(g, new System.Drawing.Rectangle(System.Drawing.Point.Empty, baseCursor.Size));
                }
                var bs = bmp.BitmapSource();
                Image img = new Image();
                img.Source = bs;
                img.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;

                var grid = new Grid();
                grid.RowDefinitions.Add(new RowDefinition());
                grid.RowDefinitions.Add(new RowDefinition());
                grid.Children.Add(img);
                grid.Children.Add(element);
                Grid.SetRow(element, 1);

                grid.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
                grid.Arrange(new Rect(new Point(), grid.DesiredSize));


                RenderTargetBitmap rtb = new RenderTargetBitmap((int)grid.DesiredSize.Width, (int)grid.DesiredSize.Height, 96, 96, PixelFormats.Pbgra32);

                rtb.Render(grid);


                var encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(rtb));

                using (var ms = new MemoryStream())
                {
                    encoder.Save(ms);
                    using (var bmp1 = new System.Drawing.Bitmap(ms))
                    {
                        return InternalCreateCursor(bmp1);

                        //return new Cursor(ms);
                    }
                }


            }
        }

        public static Cursor GetArrowCursorWithEllipse(System.Windows.Shapes.Ellipse ellipse)
        {
            var baseCursor = System.Windows.Forms.Cursors.Arrow;
            int eHalfWidth = (int)(ellipse.Width / 2.0d);
            int eHalfHeight = (int)(ellipse.Height / 2.0d);
            int width = baseCursor.Size.Width + eHalfWidth;
            int height = baseCursor.Size.Height + eHalfHeight;


            using (var bmp = new System.Drawing.Bitmap(width, height))
            {
                using (var g = System.Drawing.Graphics.FromImage(bmp))
                {
                    g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                    g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
                    baseCursor.Draw(g, new System.Drawing.Rectangle(System.Drawing.Point.Empty, baseCursor.Size));
                }
                var bs = bmp.BitmapSource();
                Image img = new Image();
                img.Source = bs;
                img.Margin = new Thickness(eHalfWidth, eHalfHeight, 0.0d, 0.0d);
                var grid = new Grid();

                grid.Children.Add(ellipse);
                grid.Children.Add(img);

                grid.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
                grid.Arrange(new Rect(new Point(), grid.DesiredSize));


                RenderTargetBitmap rtb = new RenderTargetBitmap((int)grid.DesiredSize.Width, (int)grid.DesiredSize.Height, 96, 96, PixelFormats.Pbgra32);

                rtb.Render(grid);


                var encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(rtb));

                using (var ms = new MemoryStream())
                {
                    encoder.Save(ms);
                    using (var bmp1 = new System.Drawing.Bitmap(ms))
                    {
                        return InternalCreateCursor(bmp1, eHalfWidth, eHalfHeight);
                    }
                }
            }
        }

        private static Cursor InternalCreateCursor(System.Drawing.Bitmap bmp)
        {
            return InternalCreateCursor(bmp, 0, 0);
        }

        private static Cursor InternalCreateCursor(System.Drawing.Bitmap bmp, int xHotspot, int yHotspot)
        {
            var iconInfo = new NativeMethods.IconInfo();
            NativeMethods.GetIconInfo(bmp.GetHicon(), ref iconInfo);

            iconInfo.xHotspot = xHotspot;
            iconInfo.yHotspot = yHotspot;
            iconInfo.fIcon = false;

            var cursorHandle = NativeMethods.CreateIconIndirect(ref iconInfo);
            return new Cursor(cursorHandle);
        }


    }
}
