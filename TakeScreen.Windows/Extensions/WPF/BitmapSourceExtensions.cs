﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace TakeScreen.Windows.Extensions
{
  public static  class BitmapSourceExtensions
    {
        public static byte[] JpegBytes(this BitmapSource bitmap)
        {
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.QualityLevel =TakeScreen.Core. Properties.Settings.Default.JpegQuality;
            byte[] imageBytes = null;

            using (var ms = new System.IO.MemoryStream())
            {
                encoder.Frames.Add(BitmapFrame.Create(bitmap));
                encoder.Save(ms);
                imageBytes = ms.ToArray();
            }
            return imageBytes;
        }

        public static byte[] PngBytes(this BitmapSource bitmap)
        {
            PngBitmapEncoder encoder = new PngBitmapEncoder();
            byte[] imageBytes = null;

            using (var ms = new System.IO.MemoryStream())
            {
                encoder.Frames.Add(BitmapFrame.Create(bitmap));
                encoder.Save(ms);
                imageBytes = ms.ToArray();
            }
            return imageBytes;
        }

        public static System.Drawing.Bitmap PngImage(this BitmapSource bitmap)
        {
            PngBitmapEncoder encoder = new PngBitmapEncoder();
            var ms = new System.IO.MemoryStream();
            var clone = bitmap.Clone();
            clone.Freeze();
            encoder.Frames.Add(BitmapFrame.Create(clone));
            encoder.Save(ms);
            return new System.Drawing.Bitmap(ms);
        }

        public static System.Drawing.Bitmap JpegBitmap(this BitmapSource bitmap)
        {
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.QualityLevel = TakeScreen.Core.Properties.Settings.Default.JpegQuality;
            var ms = new System.IO.MemoryStream();
            var clone = bitmap.Clone();
            clone.Freeze();
            encoder.Frames.Add(BitmapFrame.Create(clone));
            encoder.Save(ms);
            return new System.Drawing.Bitmap(ms);
        }
    }
}
