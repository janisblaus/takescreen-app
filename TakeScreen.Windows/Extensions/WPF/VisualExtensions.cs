﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using TakeScreen.Windows.Models;

namespace TakeScreen.Windows.Extensions
{
    public static class VisualExtensions
    {

        public static DpiInfo Dpi(this Visual visual)
        {

            var source = PresentationSource.FromVisual(visual);
            if (source == null)
            {
                return new DpiInfo(1, 1);
            }

            var transform = source.CompositionTarget.TransformToDevice;
            return new DpiInfo(transform.M11, transform.M22);
        }
    }
}
