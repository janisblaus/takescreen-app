using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Deployment.WindowsInstaller;
using System.Diagnostics;

namespace TakeScreen.Windows.Setup.InstallerActions
{
    public class CustomActions
    {
        [CustomAction]
        public static ActionResult KillTakeScreen(Session session)
        {
            session.Log("Begin KillTakeScreen");
            try
            {
                var processes = Process.GetProcessesByName("TakeScreen");
                session.Log(string.Format( "{0} found",processes.Length));
                foreach (var p in processes)
                {
                    //try
                    //{
                    //    session.Log("Closing TakeScreen ...");
                    //    p.Close();
                    //    session.Log("TakeScreen closed.");
                    //}
                    //catch(Exception ex1)
                    //{
                    //    session.Log(ex1.StackTrace);

                        try
                        {
                            session.Log("Killing TakeScreen ...");
                            p.Kill();
                            session.Log("TakeScreen killed.");
                        }
                        catch (Exception ex2)
                        {
                            session.Log(ex2.StackTrace);
                        }


                    //}

                  

                   
                    
                }
                return ActionResult.Success;
            }
            catch(Exception ex)
            {
                session.Log(ex.StackTrace);
            }
            finally
            {
                session.Log("End KillTakeScreen");
            }

            return ActionResult.NotExecuted;
           
        }
    }
}
