# TakeScreen #

##Windows##

###Project requirements###
1. Visual Studio Community 2017 (https://www.visualstudio.com/downloads/)
2. Wix Toolset V3.11 RC2 (http://wixtoolset.org/releases/)

### Building the installer ###
1. In Visual Studio, select "Release" configuration
2. In Solution Explorer, right click TakeScreen.Windows.Setup project and choose "Build"
3. The installer file should be created in "TakeScreen\TakeScreen.Windows.Setup\bin\Release" folder.

### Preparing a new version ###
1. In Visual Studio Solution Explorer, right click TakeScreen.Windows project and choose "Properties"
2. Select "Application" tab and click "Assembly Information" button
3. Increase the versions (file and assembly)
4. In TakeScreen.Windows.Setup project, open TakeScreenSetup.wxs file
5. In the Product tag, use the same version you set in step 3
6. Change the Id attribute. Generate a new GUID (Tools->Create GUID)
7. Select "Release" configuration
8. Right click TakeScreen.Windows.Setup project and choose "Build"
9. The installer file should be created in "TakeScreen\TakeScreen.Windows.Setup\bin\Release" folder. Make this file available for new users.
10. Upload the file to the ftp folder
11. Update the config.ini file to reflect the new version and the new file name.

##Mac##

###Project requirements###
1. Visual Studio for Mac (https://www.visualstudio.com/vs/visual-studio-mac/)