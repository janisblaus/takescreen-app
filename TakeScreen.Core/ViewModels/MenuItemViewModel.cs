﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TakeScreen.Core.ViewModels
{
    public class MenuItemViewModel
    {
        private bool _enabled;
        public event EventHandler EnabledChanged;

        public MenuItemViewModel()
        {
            _enabled = true;
        }

        public bool Enabled
        {
            get
            {
                return _enabled;
            }
            set
            {
                if (_enabled != value)
                {
                    _enabled = value;
                    EnabledChanged?.Invoke(this, EventArgs.Empty);
                }
            }
        }

        public string Key { get; set; }
    }
}
