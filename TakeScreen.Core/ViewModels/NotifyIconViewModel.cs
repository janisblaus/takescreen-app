﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TakeScreen.Core.ViewModels
{
    public class NotifyIconViewModel
    {
        public Icon Icon { get; set; }

        public MenuViewModel Menu { get; set; }

        public Action Action { get; set; }
    }
}
