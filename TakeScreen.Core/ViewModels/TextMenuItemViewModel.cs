﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TakeScreen.Core.ViewModels
{
    public class TextMenuItemViewModel : MenuItemViewModel
    {
        private string _text;
        public event EventHandler TextChanged;
        public string Text
        {
            get
            {
                return _text;
            }
            set
            {
                if(_text != value)
                {
                    _text = value;
                    TextChanged?.Invoke(this, EventArgs.Empty);
                }
            }
        }
        public Action Action { get; set; }
    }
}
