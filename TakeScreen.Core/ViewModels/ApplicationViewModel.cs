﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TakeScreen.Core.Controllers;
using TakeScreen.Core.Models;

namespace TakeScreen.Core.ViewModels
{
    public abstract class ApplicationViewModel<API,APP>
        where API : ApiController
        where APP:ApplicationController
    {
        private NotifyIconViewModel _notifyIcon;
        private TextMenuItemViewModel _takeScreenMenuItem;
        private TextMenuItemViewModel _optionsMenuItem;
        private TextMenuItemViewModel _signinMenuItem;
        private TextMenuItemViewModel _logoutMenuItem;
        private TextMenuItemViewModel _webAccountMenuItem;
        private TextMenuItemViewModel _exitMenuItem;
        private TextMenuItemViewModel _updateMenuItem;

        public ApplicationViewModel()
        {
            _notifyIcon = new NotifyIconViewModel();
            _notifyIcon.Icon = Properties.Resources.Icon;
            _notifyIcon.Action = new Action(StartCapture);

            var menu = new MenuViewModel();
            _takeScreenMenuItem = new TextMenuItemViewModel()
            { Text = Properties.Resources.TakeScreen,
                Action = new Action(StartCapture),
                Key="TakeScreen"
            };
            menu.Items.Add(_takeScreenMenuItem);

            menu.Items.Add(new SeparatorMenuItemViewModel());

            _signinMenuItem = new TextMenuItemViewModel() { Text = Properties.Resources.Signin };
            _signinMenuItem.Action = new Action(() =>
            {
                Task.Run(async () =>
                {
                    await this.ApiController.SigninAsync();
                });
            });
            _signinMenuItem.Key = "Signin";
            menu.Items.Add(_signinMenuItem);


            _webAccountMenuItem = new TextMenuItemViewModel() { Text = Properties.Resources.MyAccount };
            _webAccountMenuItem.Action = new Action(() =>
            {
                Process.Start(Properties.Settings.Default.HomeURL);
            });
            _webAccountMenuItem.Key = "MyAccount";

            _logoutMenuItem = new TextMenuItemViewModel() { Text = Properties.Resources.Logout };
            _logoutMenuItem.Action = new Action(() =>
            {
                Task.Run(async () =>
                {
                    await ApiController.LogoutAsync();
                });
            });
            _logoutMenuItem.Key = "Logout";

            menu.Items.Add(new SeparatorMenuItemViewModel());

            _optionsMenuItem = new TextMenuItemViewModel() { Text = Properties.Resources.Options };
            _optionsMenuItem.Action = new Action(DisplayOptions);
            _optionsMenuItem.Key = "Options";
            menu.Items.Add(_optionsMenuItem);

            menu.Items.Add(new SeparatorMenuItemViewModel());

            _exitMenuItem = new TextMenuItemViewModel() { Text = Properties.Resources.Exit };
                        _exitMenuItem.Action = new Action(() =>
            {
                Environment.Exit(0);
            });
            _exitMenuItem.Key = "Exit";
            menu.Items.Add(_exitMenuItem);

            _updateMenuItem = new TextMenuItemViewModel() { Text = Properties.Resources.UpdateNow };
        

            _notifyIcon.Menu = menu;

            this.ApiController.AccountStatusChanged += ApiController_AccountStatusChanged;
            this.ApplicationController.CurrentCultureUpdated += ApplicationController_CurrentCultureUpdated;
        }

        private void ApplicationController_CurrentCultureUpdated(object sender, EventArgs e)
        {

            TakeScreenMenuItem.Text = TakeScreen.Core.Properties.Resources.TakeScreen;
            SigninMenuItem.Text = Properties.Resources.Signin;
            var user = Properties.Settings.Default.CurrentWebUser;
            if (user == null)
            {
                WebAccountMenuItem.Text = Properties.Resources.MyAccount;
            }
            else
            {
                WebAccountMenuItem.Text = string.Format("{0} ({1} {2})", Properties.Resources.MyAccount, user.FirstName, user.LastName);
            }
            LogoutMenuItem.Text = Properties.Resources.Logout;
            OptionsMenuItem.Text = Properties.Resources.Options;

            ExitMenuItem.Text = Properties.Resources.Exit;

        }

        private void ApiController_AccountStatusChanged(object sender, EventArgs e)
        {
            ApiController c = sender as ApiController;
            if (c == null)
            {
                return;
            }

            var status = c.AccountStatus;

            var menuItems = this.NotifyIcon.Menu.Items;
            int index = 0;
            switch (status)
            {
                case AccountStatus.None:
                case AccountStatus.Loggedout:

                    if (!menuItems.Contains(SigninMenuItem))
                    {
                        index = menuItems.IndexOf(WebAccountMenuItem);
                        menuItems.Insert(index,SigninMenuItem);
                    }
                    if (menuItems.Contains(WebAccountMenuItem))
                    {
                        menuItems.Remove(WebAccountMenuItem);
                    }
                    if (menuItems.Contains(LogoutMenuItem))
                    {
                        menuItems.Remove(LogoutMenuItem);
                    }
                    SigninMenuItem.Enabled = true;

                    break;
                case AccountStatus.Signingin:
                    SigninMenuItem.Enabled = false;

                    break;
                case AccountStatus.Logingin:
                    SigninMenuItem.Enabled = false;
                    break;
                case AccountStatus.Loggedin:
                     index = menuItems.IndexOf(SigninMenuItem);
                    menuItems.RemoveAt(index);
                    if (!menuItems.Contains(WebAccountMenuItem))
                    {
                        menuItems.Insert(index, WebAccountMenuItem);
                    }

                    var user = Properties.Settings.Default.CurrentWebUser;
                    if (user == null)
                    {
                        WebAccountMenuItem.Text = Properties.Resources.MyAccount;
                    }
                    else
                    {
                        WebAccountMenuItem.Text = string.Format("{0} ({1} {2})", Properties.Resources.MyAccount, user.FirstName, user.LastName);
                    }

                    index = menuItems.IndexOf(WebAccountMenuItem) + 1;
                    if (!menuItems.Contains(LogoutMenuItem))
                    {
                        menuItems.Insert(index, LogoutMenuItem);
                    }

                    break;

            }
        }

        public NotifyIconViewModel NotifyIcon
        {
            get
            {
                return _notifyIcon;
            }
        }

        public TextMenuItemViewModel TakeScreenMenuItem
        {
            get
            {
                return _takeScreenMenuItem;
            }
        }

        public TextMenuItemViewModel OptionsMenuItem
        {
            get
            {
                return _optionsMenuItem;
            }
        }

        public TextMenuItemViewModel SigninMenuItem
        {
            get
            {
                return _signinMenuItem;
            }
        }

        public TextMenuItemViewModel LogoutMenuItem
        {
            get
            {
                return _logoutMenuItem;
            }
        }

        public TextMenuItemViewModel WebAccountMenuItem
        {
            get
            {
                return _webAccountMenuItem;
            }
        }

        public TextMenuItemViewModel ExitMenuItem
        {
            get
            {
                return _exitMenuItem;
            }
        }

        public TextMenuItemViewModel UpdateMenuItem
        {
            get
            {
                return _updateMenuItem;
            }
        }

        public abstract void StartCapture();
        
        public abstract void DisplayOptions();

        public abstract API ApiController {get; }

        public abstract APP ApplicationController { get; }
    }
}