﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TakeScreen.Core.ViewModels
{
    public class MenuViewModel
    {
        private ObservableCollection<MenuItemViewModel> _items;

        public ObservableCollection<MenuItemViewModel> Items
        {
            get
            {
                if(_items == null)
                {
                    _items = new ObservableCollection<MenuItemViewModel>();
                
                }
                return _items;
            }
        }
    }
}
