﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Threading;
using TakeScreen.Core.Controllers;

namespace TakeScreen.Core.Properties {


    // This class allows you to handle specific events on the settings class:
    //  The SettingChanging event is raised before a setting's value is changed.
    //  The PropertyChanged event is raised after a setting's value is changed.
    //  The SettingsLoaded event is raised after the setting values are loaded.
    //  The SettingsSaving event is raised before the setting values are saved.
    public sealed partial class Settings
    {

        public Settings()
        {
            AvailableCultures = new ArrayList();
            CultureInfo en = CultureInfo.GetCultureInfo("en");
            AvailableCultures.Add(en);
            CultureInfo lv = CultureInfo.GetCultureInfo("lv");
            AvailableCultures.Add(lv);
            CultureInfo ro = CultureInfo.GetCultureInfo("ro");
            AvailableCultures.Add(ro);
            this.SettingsLoaded += Settings_SettingsLoaded;

        }

        private void Settings_SettingsLoaded(object sender, System.Configuration.SettingsLoadedEventArgs e)
        {
            if (!IsUpdated)
            {
                Upgrade();
                IsUpdated = true;
                Save();
            }
        }


        public string UploadURLString
        {
            get
            {
                return "http://node-eu.takescreen.io/";
            }
        }

        public string DocumentsPath
        {
            get
            {
                var dPath= Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "TakeScreen");
                if (!Directory.Exists(dPath))
                {
                    Directory.CreateDirectory(dPath);
                }
                return dPath;
            }

        }

        public string VideoFilePath
        {
            get
            {
                return Path.Combine(DocumentsPath, "video.mp4");
            }

        }

        public string AppDataPath
        {
            get
            {
                return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "TakeScreen");
            }
        }

        public string InstallerFolderPath
        {
            get
            {
                return Path.Combine(AppDataPath, "Setup");
            }
        }

        public string InstallerFileName
        {
            get
            {
                return "Setup";
            }
        }
    }
}
