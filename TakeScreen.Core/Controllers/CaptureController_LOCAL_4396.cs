﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TakeScreen.Core.Models;

namespace TakeScreen.Core.Controllers
{
    public class CaptureController
    {
        private bool _isStarted;

        public CaptureController()
        {
            Model = new Capture();
        }
        public Capture Model
        {
            get; set;
        }

        public bool IsStarted
        {
            get { return _isStarted; }
        }

        public string GetNextFileName(string folderPath, string baseName, string extension)
        {
            string filePath = System.IO.Path.ChangeExtension(System.IO.Path.Combine(folderPath, string.Format("{0}", baseName)), extension);
            if (System.IO.File.Exists(filePath))
            {
                int index = 1;
                filePath = System.IO.Path.ChangeExtension(System.IO.Path.Combine(folderPath, string.Format("{0}_{1}", baseName, index)), extension);
                while (System.IO.File.Exists(filePath))
                {
                    index++;
                    filePath = System.IO.Path.ChangeExtension(System.IO.Path.Combine(folderPath, string.Format("{0}_{1}", baseName, index)), extension);
                }
            }

            return System.IO.Path.GetFileNameWithoutExtension(filePath);
        }

        public virtual void Start()
        {
            _isStarted = true;
        }

        public virtual void Close()
        {
            _isStarted = false;

        }
        public virtual void Hide() { }
    }
}
