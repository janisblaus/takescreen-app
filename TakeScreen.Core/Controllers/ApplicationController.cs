﻿using SharpRaven;
using SharpRaven.Data;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TakeScreen.Core.Controllers
{
    public class ApplicationController
    {
        private RavenClient _ravenClient; 
        public event EventHandler CurrentCultureUpdated;


        public ApplicationController()
        {
            _ravenClient = new RavenClient("https://45ad3a64274f4136b1b8a37bd3b811c6:31e726cab03648c8afd1a9697b04803d@sentry.netcore.lv/5");
        }

        public RavenClient RavenClient
        {
            get
            {
                return _ravenClient;
            }
        }

        public void UpdateCurrentCulture()
        {

            //CultureInfo ci = CultureInfo.GetCultureInfo(Properties.Settings.Default.CurrentCultureName);
            CultureInfo ci = CultureInfo.GetCultureInfo("en");//just use English for now
            CultureInfo.DefaultThreadCurrentCulture = ci;
            CultureInfo.DefaultThreadCurrentUICulture = ci;
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;

            if(CurrentCultureUpdated != null)
            {
                CurrentCultureUpdated(this, EventArgs.Empty);
            }
        }

        public void CaptureException(Exception ex)
        {
            Console.WriteLine(ex);
            if(ex == null)
            {
                return;
            }
            SentryEvent se = new SentryEvent(ex);
            RavenClient.CaptureAsync(se);
        }

        public void CaptureMessage(String message)
        {
            if (string.IsNullOrWhiteSpace(message))
            {
                return;
            }
            Console.WriteLine(message);
            SentryEvent se = new SentryEvent(message);
            se.Level = ErrorLevel.Info;
             RavenClient.CaptureAsync(se);
        }


    }
}
