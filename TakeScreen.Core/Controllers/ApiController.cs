﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TakeScreen.Core.Models;
using System.IO;

namespace TakeScreen.Core.Controllers
{
    public class ApiController
    {

        public ApiController()
        {
            ServicePointManager.Expect100Continue = false;
            ServicePointManager.UseNagleAlgorithm = false;
            ServicePointManager.CheckCertificateRevocationList = false;
        }
        #region Login/Auth
        private CancellationToken _loginTasksCancellationToken;
        private CancellationTokenSource _loginTasksCancellationTokenSource;
        private DateTime _loginStartTime;
        private AccountStatus _accountStatus;

        public event EventHandler AccountStatusChanged;

        private CancellationToken LoginTasksCancellationToken
        {
            get
            {
                if (_loginTasksCancellationToken == null)
                {
                    _loginTasksCancellationToken = this.LoginTasksCancellationTokenSource.Token;
                }
                return _loginTasksCancellationToken;
            }
        }

        private CancellationTokenSource LoginTasksCancellationTokenSource
        {
            get
            {
                if (_loginTasksCancellationTokenSource == null)
                {
                    _loginTasksCancellationTokenSource = new CancellationTokenSource();
                }
                return _loginTasksCancellationTokenSource;
            }
        }

        public AccountStatus AccountStatus
        {
            get { return this._accountStatus; }
            set
            {
                if(this._accountStatus != value)
                {
                    this._accountStatus = value;
                    if (this.AccountStatusChanged != null)
                    {
                        this.AccountStatusChanged(this, EventArgs.Empty);
                    }
                }
            }
        }

        public async Task SigninAsync()
        {
            var prevStatus = this.AccountStatus;
            this.AccountStatus = AccountStatus.Signingin;

            string token = this.GetUniqueToken();

            string url = string.Format(Properties.Settings.Default.AuthorizeURLFormat, WebUtility.UrlEncode(token));

            System.Diagnostics.Process.Start(url);

            await this.LoginAsync(TimeSpan.FromMinutes(1));
            if(AccountStatus == AccountStatus.Signingin)
            {
                AccountStatus = prevStatus;
            }
        }

        public async Task LoginAsync()
        {
            await LoginAsync(TimeSpan.Zero);
        }

        public async Task LoginAsync(TimeSpan timeout)
        {
            switch (this.AccountStatus)
            {
                case AccountStatus.Loggedin:
                    return;
            }

            _loginStartTime = DateTime.Now;
            var prevStatus = this.AccountStatus;
            this.AccountStatus = AccountStatus.Logingin;
            do
            {
                this.LoginTasksCancellationTokenSource.Cancel();
                try
                {
                    var json = await this.LoginInternalAsync();
                    if (json == null)
                    {
                        continue;
                    }
                    var state = json.Value<string>("state");
                    switch (state)
                    {
                        default:
                            break;
                        case "error":
                            break;
                        case "success":
                            Properties.Settings.Default.CurrentWebUser = WebUser.FromJson(json);
                            Properties.Settings.Default.Save();
                            this.AccountStatus = AccountStatus.Loggedin;
                            return;
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }

                Thread.Sleep(2000);
            } while (DateTime.Now - _loginStartTime < timeout);

            if (this.AccountStatus == AccountStatus.Logingin)
            {
                this.AccountStatus = prevStatus;
            }
        }

        internal virtual async Task<JToken> LoginInternalAsync()
        {
            if (this.LoginTasksCancellationToken.IsCancellationRequested)
            {
                return null;
            }

            string token = this.GetUniqueToken();

            using (var client = new System.Net.Http.HttpClient())
            {
                if (this.LoginTasksCancellationToken.IsCancellationRequested)
                {
                    return null;
                }
               
                using (var response = await client.GetAsync(string.Format(TakeScreen.Core.Properties.Settings.Default.AuthorizeCheckURLFormat, System.Net.WebUtility.UrlEncode(token))))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        if (this.LoginTasksCancellationToken.IsCancellationRequested)
                        {
                            return null;
                        }
                        var data = await response.Content.ReadAsStringAsync();
                        Console.WriteLine(data);
                        var json = JValue.Parse(data);
                        return json;
                    }
                }

            }

            return null;
        }

        public async Task LogoutAsync()
        {
            await Task.Run(() =>
            {
                switch (AccountStatus)
                {
                    case AccountStatus.Signingin:
                    case AccountStatus.Logingin:
                        this.LoginTasksCancellationTokenSource.Cancel();
                        this.LoginTasksCancellationToken.WaitHandle.WaitOne();
                        break;
                }
                
                Properties.Settings.Default.CurrentWebUser = null;
                Properties.Settings.Default.Save();
                this.AccountStatus = AccountStatus.Loggedout;
            });
        }
        #endregion

        #region Machine Data

        private JObject _machineData;
        public JObject MachineData
        {
            get
            {
                if (_machineData == null)
                {
                    _machineData = new JObject();
                    _machineData["host"] = System.Environment.MachineName;
                    try
                    {
                        string mac = this.GetMACAddress();
                        if (!string.IsNullOrWhiteSpace(mac))
                        {
                            _machineData["mac"] = mac;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                        ApplicationController c = new ApplicationController();
                        c.CaptureException(ex);
                    }
                    _machineData["language"] = this.GetCurrentLanguageName();
                    _machineData["timeZone"] = this.GetCurrentTimeZoneName();
                    _machineData["systemDriveID"] = this.GetSystemVolumeSerial();
                    _machineData["processorID"] = this.GetProcessorID();
                    _machineData["token"] = this.GetUniqueToken();
                    var wUser = Properties.Settings.Default.CurrentWebUser;

                    if (wUser != null)
                    {
                        _machineData["user_id"] = wUser.ID;
                    }
                }
                return _machineData;
            }
        }

        public string GetMACAddress()
        {
            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                // Only consider Ethernet network interfaces
                if (nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet &&
                    nic.OperationalStatus == OperationalStatus.Up)
                {
                    return nic.GetPhysicalAddress().ToString();
                }
            }
            return null;
        }

        public string GetCurrentLanguageName()
        {
            return CultureInfo.InstalledUICulture.DisplayName;
        }

        public string GetCurrentTimeZoneName()
        {
            return TimeZoneInfo.Local.DisplayName;
        }

        string _uniqueToken;
        public string GetUniqueToken()
        {
            if (_uniqueToken == null)
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.AppendLine(System.Security.Principal.WindowsIdentity.GetCurrent().Name);
                sb.AppendLine(this.GetProcessorID());
                sb.Append(this.GetSystemVolumeSerial());
                var bytes = System.Text.Encoding.Unicode.GetBytes(sb.ToString());
                _uniqueToken = Convert.ToBase64String(bytes);
            }
            return _uniqueToken;
        }

        public virtual string GetProcessorID()
        {

            throw new NotImplementedException();
   
        }

        public string GetSystemVolumeSerial()
        {
            string systemDrivePath = System.IO.Path.GetPathRoot(Environment.SystemDirectory);
            string systemDriveLetter = systemDrivePath.TrimEnd(@":\".ToCharArray());
            return GetVolumeSerial(systemDriveLetter);
        }
        public virtual string GetVolumeSerial(string strDriveLetter)
        {


            throw new NotImplementedException();

        }


        #endregion

        #region Upload

        public async Task<string> UploadImageAsync(byte[] data, Action<int> progressHandler)
        {
            string imageFormat = Properties.Settings.Default.CurrentImageFormat;
            return await this.UploadAsync(data, "image", string.Format("image.{0}", imageFormat), progressHandler);
        }


        public async Task<string> UploadVideoAsync(string sourceFilePath, Action<int> progressHandler)
        {
            Console.WriteLine("{0}: Reading video file ...",DateTime.Now);
            byte[] data = System.IO.File.ReadAllBytes(sourceFilePath);
            Console.WriteLine("{0}: Video file read.",DateTime.Now);
            return await this.UploadAsync(data, "video", "video.mp4", progressHandler);

            //string boundary = "----------------------------" + DateTime.Now.Ticks.ToString("x");


            //HttpRequestMessage m = new HttpRequestMessage(HttpMethod.Post, Properties.Settings.Default.UploadURLString);
           
            //using (var requestContent = new MultipartFormDataContent(boundary))
            //{
            //    //    here you can specify boundary if you need---^
            //    var imageContent = new ByteArrayContent(data);
            //    imageContent.Headers.ContentType =
            //        System.Net.Http.Headers.MediaTypeHeaderValue.Parse("application/octet-stream");

            //    requestContent.Add(imageContent, "video", "video.mp4");

            //    byte[] machineData = System.Text.Encoding.UTF8.GetBytes(this.MachineData.ToString());
            //    var mContent = new ByteArrayContent(machineData);
            //    imageContent.Headers.ContentType =
            //        System.Net.Http.Headers.MediaTypeHeaderValue.Parse("application/json");

            //    requestContent.Add(imageContent, "machinedata");
            //    var ph = new System.Net.Http.Handlers.ProgressMessageHandler();
            //    ph.HttpSendProgress += (sender, args) =>
            //    {
            //        Console.WriteLine("{0}: {1} ; {2} ; {3}", DateTime.Now, args.BytesTransferred, args.TotalBytes, args.ProgressPercentage);
            //    };


            //    /*var client = HttpClientFactory.Create(ph)*/;

            //    using (var client = HttpClientFactory.Create(ph))
            //    {
            //        Console.WriteLine("{0}: Sending ...", DateTime.Now);
            //        var response = await client.PostAsync(Properties.Settings.Default.UploadURLString, requestContent);
                  
            //        Console.WriteLine("{0}: Reading response ...", DateTime.Now);
            //        var s = await  response.Content.ReadAsStringAsync();
            //        Console.WriteLine("{0}: Response read.",DateTime.Now);
            //        return s;
            //    }

            //}
             


        }

        public class XXX: MultipartFormDataContent
        {
            Timer timer;
            Stream s;
            public XXX(string boundary) : base(boundary)
            {
                timer = new Timer(new TimerCallback(Blah));
            }

            private void Blah(object state)
            {
                if (s == null)
                {
                    return;
                }
                var ass = System.Reflection.Assembly.GetAssembly(typeof(System.Net.AuthenticationManager));

                var t = ass.GetType("System.Net.ConnectStream");
                System.Reflection.PropertyInfo prop =
    t.GetProperty("BytesLeftToWrite", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);

                System.Reflection.MethodInfo getter = prop.GetGetMethod(nonPublic: true);
                object bar = getter.Invoke(s, null);


                Console.WriteLine(bar);
            }

            protected override async Task SerializeToStreamAsync(Stream stream, TransportContext context)
            {
                s = stream;
                timer.Change(TimeSpan.Zero, TimeSpan.FromSeconds(1));

                await base.SerializeToStreamAsync(stream, context);
                
            }

            protected override async Task<Stream> CreateContentReadStreamAsync()
            {
                var dd=  await base.CreateContentReadStreamAsync();
                return dd;
            }
        }

        /// <summary>
        /// Uploads a video or image file to the server.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="name"></param>
        /// <param name="fileName"></param>
        /// <param name="progressHandler"></param>
        /// <returns>Retruns the URL of the uploaded file.</returns>
        public async Task<string> UploadAsync(byte[] data, string name, string fileName, Action<int> progressHandler)
        {

            progressHandler?.Invoke(0);

            byte[] machineData = System.Text.Encoding.UTF8.GetBytes(this.MachineData.ToString());

            double totalBytes = data.Length;
            totalBytes += machineData.Length;

            double bytesWritten = 0;
            string boundary = "----------------------------" + DateTime.Now.Ticks.ToString("x");
            // The first boundary
            byte[] boundaryBytes = System.Text.Encoding.UTF8.GetBytes("\r\n--" + boundary + "\r\n");
            totalBytes += boundaryBytes.Length;
            // The last boundary
            byte[] trailer = System.Text.Encoding.UTF8.GetBytes("\r\n--" + boundary + "--\r\n");
            totalBytes += trailer.Length;

            // Create the request and set parameters
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(TakeScreen.Core.Properties.Settings.Default.UploadURLString);
            request.ContentType = "multipart/form-data; boundary=" + boundary;
            //request.SendChunked = true;
           
            request.KeepAlive = true;
            request.Proxy = null;
            request.PreAuthenticate = false;
            request.Method = "POST";
 
            //request.Credentials = System.Net.CredentialCache.DefaultCredentials;

            // Get request stream
            using (System.IO.Stream requestStream = await request.GetRequestStreamAsync())
            {

           


                byte[] formImageBytes = System.Text.Encoding.UTF8.GetBytes(string.Format("Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: application/octet-stream\r\n\r\n", name, fileName));
                totalBytes += formImageBytes.Length;

                byte[] formDataBytes = System.Text.Encoding.UTF8.GetBytes(string.Format("Content-Disposition: form-data; name=\"{0}\"\r\nContent-Type: application/json\r\n\r\n", "machinedata"));
                totalBytes += formDataBytes.Length;

              await  requestStream.WriteAsync(boundaryBytes, 0, boundaryBytes.Length);
                await requestStream.FlushAsync();

                bytesWritten += boundaryBytes.Length;
                progressHandler?.Invoke((int)((bytesWritten / totalBytes) * 100d));

               await requestStream.WriteAsync(formImageBytes, 0, formImageBytes.Length);
                await requestStream.FlushAsync();
                bytesWritten += formImageBytes.Length;
                progressHandler?.Invoke((int)((bytesWritten / totalBytes) * 100d));


                //int bufferLength = (int)(totalBytes / 100);
                int bufferLength = 8 ;
                if (bufferLength <= 0)
                {
                    bufferLength = 32;
                }
                int bytesLeft = data.Length;
                int offset = 0;
                while (bytesLeft > 0)
                {
                    int bytesToWrite = Math.Min(bufferLength, bytesLeft);
                   await requestStream.WriteAsync(data, offset, bytesToWrite);
                    await requestStream.FlushAsync();
                    bytesWritten += bytesToWrite;
                    progressHandler?.Invoke((int)((bytesWritten / totalBytes) * 100d));

                    offset += bytesToWrite;
                    bytesLeft -= bytesToWrite;
                }

                //add the machine info


              await  requestStream.WriteAsync(formDataBytes, 0, formDataBytes.Length);
                await requestStream.FlushAsync();
                bytesWritten += formDataBytes.Length;
                progressHandler?.Invoke((int)((bytesWritten / totalBytes) * 100d));

              await  requestStream.WriteAsync(machineData, 0, machineData.Length);
                await requestStream.FlushAsync();
                bytesWritten += machineData.Length;
                progressHandler?.Invoke((int)((bytesWritten / totalBytes) * 100d));


                // Write trailer and close stream
             await   requestStream.WriteAsync(trailer, 0, trailer.Length);
                await requestStream.FlushAsync();
                bytesWritten += trailer.Length;
                progressHandler?.Invoke((int)((bytesWritten / totalBytes) * 100d));
                Console.WriteLine("{0} Last write operation", DateTime.Now);


            }



            progressHandler?.Invoke(100);
            Console.WriteLine("{0} Getting server response ...", DateTime.Now);

           
            using (var response = await request.GetResponseAsync())
            {
                Console.WriteLine("{0} Received response.", DateTime.Now);
                HttpWebResponse wResponse = response as HttpWebResponse;
                if (wResponse == null)
                {
                    throw new InvalidCastException("Unexpected server response, please retry.");
                }
                switch (wResponse.StatusCode)
                {
                    default:
                        throw new InvalidOperationException("Unexpected server response, please retry.");
                    case HttpStatusCode.OK:
                    case HttpStatusCode.NotFound:
                    case HttpStatusCode.ServiceUnavailable:
                        if (wResponse.ContentType != "application/json")
                        {
                            throw new InvalidCastException("Unexpected server response, please retry.");
                        }
                        using (var rStream = wResponse.GetResponseStream())
                        {
                            using (System.IO.StreamReader reader = new System.IO.StreamReader(rStream))
                            {
                                var jSonString = await reader.ReadToEndAsync();
                                if (string.IsNullOrWhiteSpace(jSonString))
                                {
                                    throw new InvalidCastException("Unexpected server response, please retry.");
                                }

                                var json = JValue.Parse(jSonString);
                                if (json == null)
                                {
                                    throw new InvalidCastException("Unexpected server response, please retry.");
                                }
                                else
                                {
                                    var url = json.Value<string>("url");
                                    //this.SetURLSafe(url);
                                    return url;
                                }
                            }
                        }
                }
            }
        

        }

        #endregion

        public async Task<JObject> GetServerVersionInfoAsync()
        {
            using (HttpClient client = new HttpClient())
            {

                var response = await client.GetAsync(TakeScreen.Core.Properties.Settings.Default.VersionURL);

                return await response.Content.ReadAsAsync<JObject>();
            }
        }
    }
}
