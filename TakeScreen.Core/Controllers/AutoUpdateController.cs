﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TakeScreen.Core.Models;

namespace TakeScreen.Core.Controllers
{

    public class AutoUpdateController
    {
        private AutoUpdate _model;
        private Timer _timer;
        private Timer _scheduleTimer;
        private ManualResetEvent _pauseDownloadEvent = new ManualResetEvent(false);
        private ManualResetEvent _locker = new ManualResetEvent(true);


        /// <summary>
        /// When set to true, it checks and downloads files from the local system.
        /// </summary>
        public bool UseSimulation { get; set; }

        private Timer Timer
        {
            get
            {
                return _timer;
            }
        }

        public ManualResetEvent Locker
        {
            get
            {
                return _locker;
            }
        }

        public AutoUpdate Model
        {
            get
            {
                if (_model == null)
                {
                    _model = new AutoUpdate();
                    if (UseSimulation)
                    {
                        _model.Interval = TimeSpan.FromSeconds(30);
                        _model.ReminderInterval = TimeSpan.FromMinutes(2);
                    }
                    _model.PropertyChanged += Model_PropertyChanged;
                }
                return _model;
            }
        }

        private void Model_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var model = sender as AutoUpdate;
            switch (e.PropertyName)
            {
                case "Interval":
                    if (_timer != null)
                    {
                        _timer.Change(model.Interval, model.Interval);
                    }
                    break;
            }
        }

        /// <summary>
        /// Adds a user interface element (menu item) that allows the user to perform the update.
        /// </summary>
        public virtual void AddUpdateNowUI()
        {

        }

        /// <summary>
        /// Queries the server.
        /// Sets <see cref="Model.NewVersion"/> if there is one.
        /// </summary>
        public async Task CheckAsync()
        {
            Console.WriteLine("Autoupdate waiting ...");
            Locker.WaitOne();
            Console.WriteLine("Autoupdate done waiting.");
            var model = Model;
            var currentVersion = model.CurrentVersion;
            //query the API
            var serverVersionInfo = await GetServerVersionInfoAsync();
            string serverVersionStr = serverVersionInfo.Value<string>("version");
            Version serverVersion = new Version(serverVersionStr);
  
            int fileSize = serverVersionInfo.Value<int>("size");

            if (serverVersion > currentVersion)
            {
                AddUpdateNowUI();
            }
            //set new version if found

            if (serverVersion > model.NewVersion)
            {
                model.NewVersion = serverVersion;
                model.FileSize = fileSize;

                if (model.IsScheduled)
                {
                    return;
                }
                model.DownloadURL = serverVersionInfo.Value<string>("download");
                if (model.NewVersion.Major > model.CurrentVersion.Major)//is major version
                {
                    await DownloadAsync();
                    if (IsDownloaded())
                    {
                        Console.WriteLine("Autoupdate waiting ...");
                        _locker.WaitOne();
                        Console.WriteLine("Autoupdate done waiting.");
                        _locker.Reset();
                        var response = GetUpgradeResponse();
                        switch (response)
                        {
                            case AutoUpdateUpgradeResponse.Upgrade:
                                await StartAsync();
                                break;
                            case AutoUpdateUpgradeResponse.NotNow:
                                Schedule();
                                break;

                        }
                        _locker.Set();
                    }
                }
                else
                {
                    Console.WriteLine("Autoupdate waiting ...");
                    _locker.WaitOne();
                    Console.WriteLine("Autoupdate done waiting.");
                    DisplayMinorUpdateNotification();
                }
            }
        }

        public virtual bool IsDownloaded()
        {
            return File.Exists(Model.InstallerFilePath);
        }

        public void Schedule()
        {
            Model.ReminderDate = DateTime.Now + Model.ReminderInterval;
            Properties.Settings.Default.AutoUpdateReminderDate = Model.ReminderDate;
            Properties.Settings.Default.Save();
            if (_scheduleTimer == null)
            {
                _scheduleTimer = new Timer(new TimerCallback((o) =>
                {
                 
                    Console.WriteLine("Autoupdate waiting ...");
                    _locker.WaitOne();
                    Console.WriteLine("Autoupdate done waiting.");
                    RunUpdateAction();
                    //if (IsDownloaded())
                    //{
                       

                    //}
                    //else
                    //{
                     
                        

                    //}
                    //Console.WriteLine("Autoupdate waiting ...");
                    //_locker.WaitOne();
                    //Console.WriteLine("Autoupdate done waiting.");

                    //var response = GetUpgradeResponse();
                    //switch (response)
                    //{
                    //    case AutoUpdateUpgradeResponse.Upgrade:
                    //        Task.Run(async () =>
                    //        {
                    //            await StartAsync();
                    //        });

                    //        break;
                    //    case AutoUpdateUpgradeResponse.NotNow:
                    //        Schedule();
                    //        break;
                    //}
                })
                , null
                , Model.ReminderDate - DateTime.Now
                , TimeSpan.FromMilliseconds(-1));
            }
            else
            {
                _scheduleTimer.Change(Model.ReminderDate - DateTime.Now, TimeSpan.FromMilliseconds(-1));
            }
        }

        public void ShowLog()
        {
            Process.Start("http://takescreen.io/changelog");
        }


        public virtual void RunUpdateAction()
        {

        }
        /// <summary>
        /// Presents a user interface element that allows the user to choose a <see cref="AutoUpdateDownloadResponse"/>.
        /// </summary>
        /// <returns>
        /// The default implementation returns <see cref="AutoUpdateDownloadResponse.Download"/>.
        /// </returns>
        public virtual AutoUpdateDownloadResponse GetDownloadResponse()
        {
            return AutoUpdateDownloadResponse.Download;
        }

        /// <summary>
        /// Displays a notification informing the user that a minor update is available for download.
        /// </summary>
        public virtual void DisplayMinorUpdateNotification()
        {

        }

        /// <summary>
        /// Presents a user interface element that allows the user to choose a <see cref="AutoUpdateUpgradeResponse"/>.
        /// </summary>
        /// <returns>
        /// The default implementation returns <see cref="AutoUpdateUpgradeResponse.Upgrade"/>.
        /// </returns>
        public virtual AutoUpdateUpgradeResponse GetUpgradeResponse()
        {
            return AutoUpdateUpgradeResponse.Upgrade;
        }

        /// <summary>
        /// Runs the installer and closes the current process.
        /// </summary>
        public virtual async Task StartAsync()
        {
            await Task.Run(() => { });
        }

        public async Task DownloadAsync()
        {
            await DownloadAsync(CancellationToken.None);
        }

        public async Task DownloadAsync(CancellationToken ct)
        {
            var model = Model;
            if (model.DownloadedFile == null)
            {
                model.DownloadedFile = new WebFile();
            }
            model.DownloadedFile.StartDate = DateTime.Now;


            if (UseSimulation)
            {
                var installerPath = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "TakeScreenSetup.msi");
                using (var stream =  File.OpenRead(installerPath))
                {
                    await ProcessDownloadStream(model, stream, ct);
                }
            }
            else
            {
                using (HttpClient client = new HttpClient())
                {
                    using (var response = await client.GetAsync(Model.DownloadURL, HttpCompletionOption.ResponseHeadersRead, ct))
                    {
                        using (var stream = await response.Content.ReadAsStreamAsync())
                        {
                            await ProcessDownloadStream(model, stream, ct);
                        }
                    }
                }
            }
         
        }

        private async Task ProcessDownloadStream(AutoUpdate model, Stream stream, CancellationToken ct)
        {
            string installerFolder = Properties.Settings.Default.InstallerFolderPath;
            if (!Directory.Exists(installerFolder))
            {
                Directory.CreateDirectory(installerFolder);
            }
            else
            {
                //delete all files
                try
                {
                    var files = Directory.GetFiles(installerFolder);
                    foreach (var file in files)
                    {
                        try
                        {
                            File.Delete(file);
                        }
                        catch (Exception ex1)
                        {
                            Console.WriteLine(ex1);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
            string installerFilePath = Model.InstallerFilePath;


            model.DownloadedFile.Downloaded = 0;
            model.DownloadedFile.Total = model.FileSize;
            var totalRead = 0;
            var buffer = new byte[8192];

            var read = 0;
            using (var fs = File.Create(installerFilePath))
            {

                do
                {
                    if (model.DownloadedFile.Paused)
                    {
                        PauseDownloadEvent.WaitOne();
                    }
                    read = await stream.ReadAsync(buffer, 0, buffer.Length, ct);

                    if (ct.IsCancellationRequested)
                    {
                        throw new OperationCanceledException(ct);
                    }

                    await fs.WriteAsync(buffer, 0, read, ct);
                    totalRead += read;
                    model.DownloadedFile.Downloaded = totalRead;
                    //Thread.Sleep(10);
                }
                while (read > 0);
            }
        }

        public ManualResetEvent PauseDownloadEvent
        {
            get
            {
                if (_pauseDownloadEvent == null)
                {
                    _pauseDownloadEvent = new ManualResetEvent(false);
                }
                return _pauseDownloadEvent;
            }
        }

        /// <summary>
        /// Calls <see cref="Check"/> method using <see cref="Model.Interval"/>
        /// </summary>
        public void StartMonitoring()
        {
            if (_timer == null)
            {
                _timer = new Timer(new TimerCallback(async (object sender) =>
                {
                    try
                    {
                        await CheckAsync();
                    }
                    catch (Exception ex)
                    {
                        var ac = new ApplicationController();
                        ac.CaptureException(ex);
                    }
                })
          , Model
          , TimeSpan.Zero
          , Model.Interval);
            }
        }

        public async Task<JObject> GetServerVersionInfoAsync()
        {
            if (UseSimulation)
            {
               
                var jSonStr = File.ReadAllText(Path.Combine(Path.GetDirectoryName( Assembly.GetEntryAssembly().Location), "TestVersion.json"));
                using (StringReader sReader = new StringReader(jSonStr))
                {
                    using (JsonTextReader jReader = new JsonTextReader(sReader))
                    {
                        return await JToken.ReadFromAsync(jReader) as JObject;
                    }
                }
            }

            using (HttpClient client = new HttpClient())
            {
                var response = await client.GetAsync(TakeScreen.Core.Properties.Settings.Default.VersionURL);
                return await response.Content.ReadAsAsync<JObject>();
            }
        }
    }
}

