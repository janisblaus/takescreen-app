﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TakeScreen.Core.Models
{
    public enum ToolType
    {
        None,
        Pen,
        Rectangle,
        Ellipse,
        Arrow,
        Line,
        Text,
        Marker
    }
}
