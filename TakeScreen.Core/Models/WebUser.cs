﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TakeScreen.Core.Models
{
    [SettingsSerializeAs(SettingsSerializeAs.Xml)]
    public class WebUser
    {

        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public static WebUser FromJson(JToken json)
        {
            WebUser user = new WebUser();
            user.ID = json.Value<int>("user_id");
            user.FirstName = json.Value<string>("first_name");
            user.LastName = json.Value<string>("last_name");

            return user;
        }
    }
}
