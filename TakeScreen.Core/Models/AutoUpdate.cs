﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TakeScreen.Core.Models
{
    public class AutoUpdate : Model
    {
        private Version _newVersion;
        private TimeSpan _interval;
        private string _downloadURL;
        private TimeSpan _reminderInterval;
        private DateTime _reminderDate;

        public AutoUpdate() : base()
        {
            _interval = Properties.Settings.Default.AutoUpdateInterval;
            _reminderInterval = Properties.Settings.Default.AutoUpdateReminderInterval;
            _reminderDate = Properties.Settings.Default.AutoUpdateReminderDate;
        }

        public TimeSpan Interval
        {
            get
            {
                return _interval;
            }
            set
            {
                if (_interval != value)
                {
                    _interval = value;
                    RaisePropertyChanged("Interval");
                }
            }
        }

        /// <summary>
        /// The current version of the TakeScreen executable.
        /// </summary>
        public Version CurrentVersion
        {
            get
            {
                return Assembly.GetEntryAssembly().GetName().Version;
            }
        }


        /// <summary>
        /// If a <see cref="Controllers.AutoUpdateController"/> finds a new version on the server it sets this property.
        /// Otherwise it returns <see cref="CurrentVersion"/>.
        /// </summary>
        public Version NewVersion
        {
            get
            {
                if (_newVersion == null)
                {
                    return CurrentVersion;
                }
                return _newVersion;
            }
            set
            {
                if (_newVersion != value)
                {
                    _newVersion = value;
                    RaisePropertyChanged("NewVersion");
                }
            }
        }

        /// <summary>
        /// Returns true if a new version is available.
        /// </summary>
        public bool Available
        {
            get
            {
                return NewVersion > CurrentVersion;
            }
        }

        public string DownloadURL
        {
            get
            {
                return _downloadURL;
            }
            set
            {
                _downloadURL = value;
            }
        }

        public string InstallerFilePath
        {
            get
            {
                string installerFolder = Properties.Settings.Default.InstallerFolderPath;

                return Path.Combine(installerFolder,
                   string.Format("{0}{1}.msi"
                    , Properties.Settings.Default.InstallerFileName
                    , NewVersion.ToString(3)));
            }
        }


        /// <summary>
        /// The interval used for calculating <see cref="ReminderDate"/>.
        /// </summary>
        public TimeSpan ReminderInterval
        {
            get
            {
                return _reminderInterval;
            }
            set
            {
                if (_reminderInterval != value)
                {
                    _reminderInterval = value;
                    RaisePropertyChanged("ReminderInterval");
                }
            }
        }
        /// <summary>
        /// The date set when the user chooses to install the update later.
        /// </summary>
        public DateTime ReminderDate
        {
            get
            {
                return _reminderDate;
            }
            set
            {
                if (_reminderDate != value)
                {
                    _reminderDate = value;
                    RaisePropertyChanged("ReminderDate");
                }
            }
        }

        public bool IsScheduled
        {
            get
            {
                return ReminderDate> DateTime.Now;
            }
        }

        public WebFile DownloadedFile { get; set; }
        public int FileSize { get; internal set; }
    }

    public enum AutoUpdateDownloadResponse
    {
        None,
        Download,
        DecideLater,
        ShowLog
    }

    public enum AutoUpdateUpgradeResponse
    {
        None,
        Upgrade,
        NotNow
    }
}
