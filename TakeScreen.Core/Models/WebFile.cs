﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TakeScreen.Core.Models
{
    /// <summary>
    /// Stores information about downloaded file.
    /// </summary>
    public class WebFile : Model
    {
        private DateTime? _startDate;
        private int _downloaded;
        private int _total;
        private bool _paused;
        private DateTime? _pausedDate;
        private TimeSpan? _ellapsed;
        private DateTime? _resumeDate;

        public DateTime? StartDate
        {
            get
            {
                return _startDate;
            }
            set
            {
                if (_startDate != value)
                {
                    _startDate = value;
                    RaisePropertyChanged("StartDate");
                }
            }
        }

        /// <summary>
        /// Downloaded bytes.
        /// </summary>
        public int Downloaded
        {
            get
            {
                return _downloaded;
            }
            set
            {
                if (_downloaded != value)
                {
                    _downloaded = value;
                    RaisePropertyChanged("Downloaded");
                }
            }
        }

        /// <summary>
        /// Total bytes.
        /// </summary>
        public int Total
        {
            get
            {
                return _total;
            }
            set
            {
                if (_total != value)
                {
                    _total = value;
                    RaisePropertyChanged("Total");
                }
            }
        }


        public bool Paused
        {
            get
            {
                return _paused;
            }
            set
            {
                if (_paused != value)
                {
                    _paused = value;
                    if (_paused)
                    {
                        PausedDate = DateTime.Now;
                    }
                    else
                    {
                        _resumeDate = DateTime.Now;
                    }
                    RaisePropertyChanged("Paused");
                }
            }
        }

        public DateTime? PausedDate
        {
            get
            {
                return _pausedDate;
            }
            private set
            {
                if (_pausedDate != value)
                {
                    var oldValue = _pausedDate;
                    _pausedDate = value;
                    if (_ellapsed.HasValue)
                    {
                        if (_pausedDate.HasValue && oldValue.HasValue)
                        {
                            _ellapsed += (_pausedDate.Value - oldValue.Value);
                        }
                    }
                    else
                    {
                        if (_pausedDate == null)
                        {
                            _ellapsed = null;
                        }
                        else
                        {
                            if (_startDate.HasValue)
                            {
                                _ellapsed = _pausedDate.Value - _startDate.Value;
                            }
                            else
                            {
                                _ellapsed = null;
                            }

                        }
                    }
                }
            }
        }

        public TimeSpan? Ellapsed
        {
            get
            {
                if(Paused){
                    return _ellapsed;
                }
                if(_ellapsed == null)
                {
                    return DateTime.Now - _startDate.Value;
                }
                if (_resumeDate.HasValue)
                {
                    return _ellapsed.Value + (DateTime.Now - _resumeDate.Value);
                }
                return null;
            }
        }

        /// <summary>
        /// Current download speed in bytes/second
        /// </summary>
        public double? Speed
        {
            get
            {
                if (_startDate == null)
                {
                    return null;
                }
                var ellapsed = Ellapsed;
                if (ellapsed == null)
                {
                    return null;
                }
                if (ellapsed.Value.TotalSeconds == 0)
                {
                    return 0;
                }
                return _downloaded / ellapsed.Value.TotalSeconds;
            }
        }

        public TimeSpan? TimeRemaining
        {
            get
            {
                var speed = Speed;
                if (speed == null)
                {
                    return null;
                }
                var bytesRemaining = _total - _downloaded;

                if (speed.Value == 0)
                {
                    return TimeSpan.Zero;
                }
                var seconds = bytesRemaining / speed.Value;
                return TimeSpan.FromSeconds(seconds);
            }
        }

        public double DownloadedPercent
        {
            get
            {
                if (_total == 0)
                {
                    return 0;
                }
                return (double)_downloaded /(double) _total;
            }
        }
    }
}
