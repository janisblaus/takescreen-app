﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TakeScreen.Core.Models
{
    public class Selection : Model
    {

        private Rectangle _rectangle;
        private SelectionStatus _selectionStatus;
        private bool _grabHandlesVisible;
        private bool _renderOutsideBounds;
        private bool _visible;

        public Rectangle Rectangle
        {
            get
            {
                return _rectangle;
            }
            set
            {
                if (_rectangle != value)
                {
                    _rectangle = value;
                    RaisePropertyChanged("Rectangle");
                }
            }
        }

        public SelectionStatus Status
        {
            get
            {
                return _selectionStatus;
            }
            set
            {
                if (_selectionStatus != value)
                {
                    _selectionStatus = value;
                    RaisePropertyChanged("Status");
                    switch (_selectionStatus)
                    {
                        case SelectionStatus.Selecting:
                        case SelectionStatus.Selected:
                            Visible = true;
                            GrabHandlesVisible = true;
                            break;
                        default:
                            Visible = false;
                            GrabHandlesVisible = false;
                            break;
                    }
                }
            }
        }

        public bool GrabHandlesVisible
        {
            get
            {
                return _grabHandlesVisible;
            }
            set
            {
                if (_grabHandlesVisible != value)
                {
                    _grabHandlesVisible = value;
                    RaisePropertyChanged("GrabHandlesVisible");
                }
            }
        }

        public bool RenderOutsideBounds
        {
            get
            {
                return _renderOutsideBounds;
            }
            set
            {
                if (_renderOutsideBounds != value)
                {
                    _renderOutsideBounds = value;
                    RaisePropertyChanged("RenderOutsideBounds");
                }
            }
        }

        public bool Visible
        {
            get
            {
                return _visible;
            }
            set
            {
                if (_visible != value)
                {
                    _visible = value;
                    RaisePropertyChanged("Visible");
                }
            }
        }

    }
}
