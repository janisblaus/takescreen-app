﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TakeScreen.Core.Models
{
    public class Capture : Model
    {
        private Selection _selection;
        private CaptureMode _mode;
        private ToolType _toolType;
        private bool _toolboxIsOpen;
        private bool _menuboxIsOpen;
        private bool _screenshotControlVisible;
        private bool _videoControlVisible;
        private bool _canUndo;
        private bool _canMaximize;
        private bool _canCopy;
        private bool _canPrint;
        private bool _canUpload;
        private bool _canSave;

        public event EventHandler ModeChanged;
        public event EventHandler ToolTypeChanged;

       public Capture() : base()
        {
            ScreenshotControlVisible = true;
            CanMaximize = true;
            
        }

        public Selection Selection
        {
            get
            {
                if (_selection == null)
                {
                    _selection = new Selection();
                    RaisePropertyChanged("Selection");
                    UpdateCanCopy();
                    UpdateCanPrint();
                    UpdateCanUpload();
                    UpdateCanSave();
                    WireSelectionEvents();
                }
                return _selection;
            }
            set
            {
                if(_selection != value)
                {
                    UnwireSelectionEvents();
                    _selection = value;
                    RaisePropertyChanged("Selection");
                    UpdateCanCopy();
                    UpdateCanPrint();
                    UpdateCanUpload();
                    UpdateCanSave();
                    WireSelectionEvents();
                }
            }
        }

        private void WireSelectionEvents()
        {
            if (_selection != null)
            {
                _selection.PropertyChanged += Selection_PropertyChanged;
            }
        }

        private void Selection_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "Status":
                    UpdateCanCopy();
                    UpdateCanPrint();
                    UpdateCanUpload();
                    UpdateCanSave();
                    break;
            }
        }

        private void UnwireSelectionEvents()
        {
            if (_selection != null)
            {
                _selection.PropertyChanged -= Selection_PropertyChanged;
            }
        }

        internal virtual void UpdateCanCopy()
        {
            if(Mode == CaptureMode.Video)
            {
                CanCopy = false;
                return;
            }
            CanCopy = Selection.Status == SelectionStatus.Selected;
        }

        internal virtual void UpdateCanPrint()
        {
            if (Mode == CaptureMode.Video)
            {
                CanPrint = false;
                return;
            }
            CanPrint = Selection.Status == SelectionStatus.Selected;
        }
        internal virtual void UpdateCanUpload()
        {
            if (Mode == CaptureMode.Video)
            {
                CanUpload = false;
                return;
            }
            CanUpload = Selection.Status == SelectionStatus.Selected;
        }

        internal virtual void UpdateCanSave()
        {
            if (Mode == CaptureMode.Video)
            {
                CanSave = false;
                return;
            }
            CanSave = Selection.Status == SelectionStatus.Selected;
        }
        public virtual void AdjustSelection()
        {
            var selection = Selection;
            if (Mode ==  CaptureMode.Video)
            {
                var sRect = selection.Rectangle;
                if (sRect.Width % 2 != 0)
                {
                    sRect.Width -= 1;
                }
                if (sRect.Height % 2 != 0)
                {
                    sRect.Height -= 1;
                }
                selection.Rectangle = sRect;
            }
        }

        public CaptureMode Mode
        {
            get { return _mode; }
            set
            {
                if(_mode != value)
                {
                    _mode = value;
                    RaisePropertyChanged("Mode");
                    ModeChanged?.Invoke(this, EventArgs.Empty);
                    ScreenshotControlVisible = _mode == CaptureMode.Image;
                    VideoControlVisible = _mode == CaptureMode.Video;
                    UpdateMenuboxProperties();
                    UpdateToolboxProperties();
                    CanMaximize = _mode == CaptureMode.Image;
                    UpdateCanCopy();
                    UpdateCanPrint();
                    UpdateCanUpload();
                    UpdateCanSave();
                }
            }
        }

        public virtual ToolType ToolType
        {
            get
            {
                return _toolType;
            }
            set
            {
                if(_toolType != value)
                {
                    _toolType = value;
                    RaisePropertyChanged("ToolType");
                    ToolTypeChanged?.Invoke(this, EventArgs.Empty);
                }
            }
        }

        public bool ToolboxIsOpen
        {
            get
            {
                return _toolboxIsOpen;
            }
            set
            {
                if (_toolboxIsOpen != value)
                {
                    _toolboxIsOpen = value;
                    RaisePropertyChanged("ToolboxIsOpen");
                }
            }
        }

        public bool MenuboxIsOpen
        {
            get
            {
                return _menuboxIsOpen;
            }
            set
            {
                if(_menuboxIsOpen != value)
                {
                    _menuboxIsOpen = value;
                    RaisePropertyChanged("MenuboxIsOpen");
                }
            }
        }

        public bool ScreenshotControlVisible
        {
            get
            {
                return _screenshotControlVisible;
            }
           private set
            {
                if(_screenshotControlVisible != value)
                {
                    _screenshotControlVisible = value;
                    RaisePropertyChanged("ScreenshotControlVisible");
                }
            }
        }

        public bool VideoControlVisible
        {
            get
            {
                return _videoControlVisible;
            }
            private set
            {
                if(_videoControlVisible != value)
                {
                    _videoControlVisible = value;
                    RaisePropertyChanged("VideoControlVisible");
                }
            }
        }

        public  void UpdateToolboxProperties()
        {
            switch (Mode)
            {
                case CaptureMode.Video:
                    ToolboxIsOpen = false;
                    return;
            }


            bool isOpen = false;
            switch (this.Selection.Status)
            {
                case SelectionStatus.Selected:
                    isOpen = true;
                    break;
            }


            this.ToolboxIsOpen = isOpen;
        }

        public void UpdateMenuboxProperties()
        {
            switch (this.Mode)
            {
                case CaptureMode.Video:
                    ToolboxIsOpen = true;
                    return;
            }


            bool isOpen = false;
            switch (this.Selection.Status)
            {
                case SelectionStatus.Selected:
                    isOpen = true;
                    break;
            }


            this.MenuboxIsOpen = isOpen;
        }


        public bool CanUndo
        {
            get { return _canUndo; }
            set
            {
                if(_canUndo != value)
                {
                    _canUndo = value;
                    RaisePropertyChanged("CanUndo");
                }
            }
        }

        public bool CanMaximize
        {
            get { return _canMaximize; }
            set
            {
                if (_canMaximize != value)
                {
                    _canMaximize = value;
                    RaisePropertyChanged("CanMaximize");
                }
            }
        }
        public bool CanCopy
        {
            get { return _canCopy; }
            set
            {
                if (_canCopy != value)
                {
                    _canCopy = value;
                    RaisePropertyChanged("CanCopy");
                }
            }
        }
        public bool CanPrint
        {
            get { return _canPrint; }
            set
            {
                if (_canPrint != value)
                {
                    _canPrint = value;
                    RaisePropertyChanged("CanPrint");
                }
            }
        }
        public bool CanUpload
        {
            get { return _canUpload; }
            set
            {
                if (_canUpload != value)
                {
                    _canUpload = value;
                    RaisePropertyChanged("CanUpload");
                }
            }
        }
        public bool CanSave
        {
            get { return _canSave; }
            set
            {
                if (_canSave != value)
                {
                    _canSave = value;
                    RaisePropertyChanged("CanSave");
                }
            }
        }
    }
}
