﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TakeScreen.Core.Models
{
    public enum SelectionStatus
    {
        None,
        //Inactive,
        Selecting,
        Selected
    }
}
