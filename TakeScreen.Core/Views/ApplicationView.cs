﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TakeScreen.Core.Controllers;
using TakeScreen.Core.Models;
using TakeScreen.Core.ViewModels;

namespace TakeScreen.Core.Views
{
    public abstract class ApplicationView<M,API,APP> 
        where APP:ApplicationController
        where API:ApiController
        where M:ApplicationViewModel<API,APP>,new()
    {

   
 
        private M _model;
        public M Model
        {
            get
            {
                if (_model == null)
                {
                    _model = new M();
                }
                return _model;
            }
        }

        public abstract void Show();


        public abstract void Hide();

    }
}
