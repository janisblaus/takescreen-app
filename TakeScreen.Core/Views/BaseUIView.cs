﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TakeScreen.Core.Views
{
    public class BaseUIView
    {
        private Rectangle _bounds;
        public event EventHandler BoundsChanging;
        public event EventHandler BoundsChanged;
        public event EventHandler SizeChanging;
        public event EventHandler SizeChanged;
        public event EventHandler LocationChanging;
        public event EventHandler LocationChanged;
        public event EventHandler LeftChanging;
        public event EventHandler LeftChanged;
        public event EventHandler TopChanging;
        public event EventHandler TopChanged;
        public BaseUIView()
        {
            Enabled = true;
            Visible = true;
        }
        public virtual Rectangle Bounds { get
            {
                return _bounds;
            }
            set
            {
                if(_bounds != value)
                {
                    BoundsChanging?.Invoke(this, EventArgs.Empty);

                    if (_bounds.Left != value.Left)
                    {
                        LeftChanging?.Invoke(this, EventArgs.Empty);
                    }
                    if (_bounds.Top != value.Top)
                    {
                        TopChanging?.Invoke(this, EventArgs.Empty);
                    }
                    if (_bounds.Location!= value.Location)
                    {
                        LocationChanging?.Invoke(this, EventArgs.Empty);
                    }
                    if(_bounds.Size != value.Size)
                    {
                        SizeChanging?.Invoke(this, EventArgs.Empty);
                    }

                    var oldValue = _bounds;
                    _bounds = value;
                    BoundsChanged?.Invoke(this, EventArgs.Empty);
                    if (oldValue.Location != value.Location)
                    {
                        LocationChanged?.Invoke(this, EventArgs.Empty);
                    }
                    if (oldValue.Left != value.Left)
                    {
                        LeftChanged?.Invoke(this, EventArgs.Empty);
                    }
                    if (oldValue.Top != value.Top)
                    {
                        TopChanged?.Invoke(this, EventArgs.Empty);
                    }
                    if (oldValue.Size != value.Size)
                    {
                        SizeChanged?.Invoke(this, EventArgs.Empty);
                    }
                }
            }
        }

        public Size Size
        {
            get
            {
                return _bounds.Size;
            }
            set
            {
                var bounds = _bounds;
                bounds.Size = value;
                Bounds = bounds;
            }
        }

        public Point Location
        {
            get
            {
                return _bounds.Location;
            }
            set
            {
                var bounds = _bounds;
                bounds.Location = value;
                Bounds = bounds;
            }
        }
        public int Left
        {
            get
            {
                return _bounds.Left;
            }
            set
            {
                var bounds = _bounds;
                bounds.X = value;
                Bounds = bounds;
            }
        }
        public int Top
        {
            get
            {
                return _bounds.Top;
            }
            set
            {
                var bounds = _bounds;
                bounds.Y = value;
                Bounds = bounds;
            }
        }

        public virtual Rectangle RenderingBounds {
            get
            {
                return Bounds;
            }
        }
        public virtual bool Enabled { get; set; }
        public virtual bool Visible { get; set; }
        public string Name { get; set; }
        public object Tag { get; set; }
    }
}
